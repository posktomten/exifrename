/*
    EXIF ReName
    Copyright (C) 2007 - 2017  Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details..
*/

#include "createuser.h"
//#include<QStringBuilder>

const QString CONF_DIR = QDir::toNativeSeparators(QDir::homePath()+"/.exifrename");
const QString CONF_FILE = CONF_DIR + QDir::toNativeSeparators("/exif_rename.conf");
const QString CONF_FILE2 = CONF_DIR + QDir::toNativeSeparators("/exif_rename3.conf");


bool Createuser::mkDir()
{
    QDir dir;
    if ( dir.mkdir(CONF_DIR) )
        return true;

        return false;


}


bool Createuser::removeDir()
{

   QFile fil(CONF_FILE2);
   if (fil.exists() == true)
   {

    QDir dir;
    if (dir.remove(CONF_FILE) && dir.remove(CONF_FILE2))
    {
        if (dir.rmdir(CONF_DIR))
            return true;
    }
        return false;

    }


   if (fil.exists() == false)
   {

    QDir dir;
    if (dir.remove(CONF_FILE))
    {
        if (dir.rmdir(CONF_DIR))
            return true;
    }
        return false;

    }
return false;

}


