<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>Messagebox</name>
    <message>
        <location filename="messagebox.cpp" line="32"/>
        <source>Do you want to remove your personal settings?
This is an appropriate action if you like to uninstall this application.
Or if you want to reset all settings to default.</source>
        <translation>Möchten Sie Ihre persönlichen Einstellungen entfernen?
Dies ist eine geeignete Aktion, wenn Sie diese Anwendung deinstallieren möchten.
Oder wenn Sie alle Einstellungen auf den Standard zurücksetzen möchten.</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="33"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="34"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="42"/>
        <source>Your personal settings are deleted.</source>
        <translation>Ihre persönlichen Einstellungen werden gelöscht.</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="48"/>
        <location filename="messagebox.cpp" line="61"/>
        <source>Your personal settings cannot be found.
New settings will be saved then you restart the program.</source>
        <translation>Ihre persönlichen Einstellungen können nicht gefunden werden.
Neue Einstellungen werden gespeichert und anschließend starten Sie das Programm neu.</translation>
    </message>
</context>
</TS>
