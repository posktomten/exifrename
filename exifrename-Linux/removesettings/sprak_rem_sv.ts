<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>Messagebox</name>
    <message>
        <location filename="messagebox.cpp" line="32"/>
        <source>Do you want to remove your personal settings?
This is an appropriate action if you like to uninstall this application.
Or if you want to reset all settings to default.</source>
        <translation>Vill du ta bort alla personliga inställningar?
Det är lämpligt om du ska avinstallera programmet
eller vill återställa alla inställningar till standardvärden.</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="33"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="34"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="42"/>
        <source>Your personal settings are deleted.</source>
        <translation>Dina personliga inställningar är borttagna.</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="48"/>
        <location filename="messagebox.cpp" line="61"/>
        <source>Your personal settings cannot be found.
New settings will be saved then you restart the program.</source>
        <translation>Dina personliga inställningar kunde inte hittas.
Nya inställningar kommer att sparas när du startar om programmet.</translation>
    </message>
</context>
</TS>
