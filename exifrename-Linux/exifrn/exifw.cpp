/*
    EXIF ReName
    Copyright (C) 2007-2019 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/




#include <fstream>
#include "exifw.h"
const int LAS_LANGD = 5120;
// 3072
using namespace std;

Exifw::Exifw() {}

bool Exifw::setExif(char *filnamn, char *hittat, char *info)
{
    char in;
    char hela[LAS_LANGD] = {};
    ifstream lasfil;
    /* Öppna filen. */
    lasfil.open(filnamn, ios::binary);

    if(! lasfil.good()) {
        strcpy(filnamn, "");
        strcpy(info, "Unable to open ");
        return false;
    }

    for(int i = 0; i < LAS_LANGD; i++) {
        lasfil.read((char*) &in, 1);
        hela[i] = in;
    }

//strcpy(hittat,"");
    int raknare = 0;

    for(int ganger = 0; ganger < 3; ganger++) {
        bool f = finn(hela, ganger);

        if(f == false) {
            raknare++;
            f = true;
        }

        if(raknare > 2) {
            strcpy(filnamn, "");
            strcpy(info, "No EXIF data found in ");
            return false;
        }

        if(manipulera(filnamn, hittat, info) == false)
            strcpy(info, "Unable to write EXIF data ");

        strcpy(info, "");
    }

    return true;
}

bool Exifw::finn(char *h, int ganger)
{
    raknare = 0;
    int k = 0;

    for(int i = 0; i <= LAS_LANGD; i++) {
        raknare++;

        if(s(h[i]) && s(h[i + 1]) && s(h[i + 2]) && s(h[i + 3]) && s(h[i + 5]) && sa(h[i + 4]) && s(h[i + 5]) && s(h[i + 6]) && sa(h[i + 7]) && s(h[i + 8]) && s(h[i + 9]) && sb(h[i + 10]) && s(h[i + 11]) && s(h[i + 12]) && sa(h[i + 13]) && s(h[i + 14]) && s(h[i + 15]) && sa(h[i + 16]) && s(h[i + 17]) && s(h[i + 18])) {
            k++;

            if(k > ganger) {
                return true;
            }
        }
    }

    return false;
}

bool Exifw::s(char c)
{
    if(c >= '0' && c <= '9')
        return true;

    return false;
}
bool Exifw::sa(char c)
{
    if(c == ':')
        return true;

    return false;
}
bool Exifw::sb(char c)
{
    if(c == ' ')
        return true;

    return false;
}

bool Exifw::manipulera(char *filnamn, char *bytut, char *info)
{
    raknare--;
    fstream skrivfil;
    skrivfil.open(filnamn, ios::binary | ios::in | ios::out);

    if(! skrivfil.good()) {
        strcpy(filnamn, "");
        strcpy(info, "Unable to write to file... ");
        return false;
    }

//Year
    skrivfil.seekp(raknare);
    skrivfil.put(bytut[0]);
    skrivfil.seekp(raknare + 1);
    skrivfil.put(bytut[1]);
    skrivfil.seekp(raknare + 2);
    skrivfil.put(bytut[2]);
    skrivfil.seekp(raknare + 3);
    skrivfil.put(bytut[3]);
//Months
    skrivfil.seekp(raknare + 5);
    skrivfil.put(bytut[4]);
    skrivfil.seekp(raknare + 6);
    skrivfil.put(bytut[5]);
//Day
    skrivfil.seekp(raknare + 8);
    skrivfil.put(bytut[6]);
    skrivfil.seekp(raknare + 9);
    skrivfil.put(bytut[7]);
//Hour
    skrivfil.seekp(raknare + 11);
    skrivfil.put(bytut[8]);
    skrivfil.seekp(raknare + 12);
    skrivfil.put(bytut[9]);
//Minute
    skrivfil.seekp(raknare + 14);
    skrivfil.put(bytut[10]);
    skrivfil.seekp(raknare + 15);
    skrivfil.put(bytut[11]);
//Second
    skrivfil.seekp(raknare + 17);
    skrivfil.put(bytut[12]);
    skrivfil.seekp(raknare + 18);
    skrivfil.put(bytut[13]);
    skrivfil.close();
    return true;
}
