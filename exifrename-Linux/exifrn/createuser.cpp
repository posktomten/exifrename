/*
    EXIF ReName
    Copyright (C) 2007-2019 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#include "createuser.h"


const QString CONF_DIR = QDir::toNativeSeparators(QDir::homePath() + "/.exifrename");
const QString CONF_FILE = CONF_DIR + QDir::toNativeSeparators("/exif_rename.conf");
const QString CONF_FILE3 = CONF_DIR + QDir::toNativeSeparators("/exif_rename3.conf");
const QString CONF_FILE_BUGG = CONF_DIR + QDir::toNativeSeparators("/exif_rename2.conf");


bool Createuser::mkDir()
{
    QDir dir;

    if(dir.mkdir(CONF_DIR))
        return true;

    return false;
}




bool Createuser::removeDir()
{
    QDir dir;

    if(dir.remove(CONF_FILE) && dir.remove(CONF_FILE3)) {
        if(dir.rmdir(CONF_DIR))
            return true;
    }

    if(dir.remove(CONF_FILE)) {
        if(dir.rmdir(CONF_DIR))
            return true;
    }

    if(dir.rmdir(CONF_DIR))
        return true;

    return false;
}

bool Createuser::remove_buggConfig() //Trista config buggen!
{
    QDir dir;

    if(! QFile::exists(CONF_FILE_BUGG))
        return true;

    if(dir.remove(CONF_FILE_BUGG))
        return true;

    return false;
}
