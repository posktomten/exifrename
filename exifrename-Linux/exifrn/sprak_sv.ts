<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="sv">
<context>
    <name>Exif_w</name>
    <message>
        <location filename="exif_w.ui" line="35"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="72"/>
        <source>Date and time</source>
        <translation>Datum och tid</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="85"/>
        <source>Change the time stamp on</source>
        <translation>Ändra tidsstämpeln på</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="98"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="111"/>
        <source>Year-Month-Day Hour-Minute-Second</source>
        <translation>År-Månad-Dag Timme-Minut-Sekund</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="124"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="137"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="150"/>
        <source>Current Time</source>
        <translation>Nuvarande tid</translation>
    </message>
    <message>
        <location filename="exif_w.cpp" line="72"/>
        <source>Incorrect date/time format, unable to write to file.
You must write in this way: </source>
        <translation>Felaktigt datum/tid format, kan inte skriva till filen. Du måste skriva på det här sättet: </translation>
    </message>
    <message>
        <location filename="exif_w.cpp" line="101"/>
        <source>Unable to save EXIF data</source>
        <translation>Kan inte spara EXIF data</translation>
    </message>
</context>
<context>
    <name>FilePattern</name>
    <message>
        <location filename="filepattern.ui" line="59"/>
        <source>file name</source>
        <translation>filnamn</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="80"/>
        <location filename="filepattern.ui" line="117"/>
        <location filename="filepattern.ui" line="266"/>
        <source>none</source>
        <translation>none</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="85"/>
        <location filename="filepattern.ui" line="122"/>
        <location filename="filepattern.ui" line="271"/>
        <source>space</source>
        <translation>space</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="90"/>
        <location filename="filepattern.ui" line="127"/>
        <location filename="filepattern.ui" line="276"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="95"/>
        <location filename="filepattern.ui" line="132"/>
        <location filename="filepattern.ui" line="281"/>
        <source>_</source>
        <translation>_</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="149"/>
        <source>File name figures, separated by</source>
        <translation>Siffrorna i filnamnet avdelas av</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="232"/>
        <source>Save</source>
        <translation>Spara</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="295"/>
        <source>Folder name figures, separated by</source>
        <translation>Siffrorna i mappnamnet avdelas av</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="373"/>
        <source>Only time in file name</source>
        <translation>Endast tiden i filnamnet</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="834"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Extensions of files you want to be able to open, separated by cemikolon. S&lt;span style=&quot; color:#000000;&quot;&gt;uch as jpg and tif.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000&quot;&gt;Example:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;jpg;tif;cr2&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Extensions of files you want to be able to open, separated by cemikolon. S&lt;span style=&quot; color:#000000; background-color:#e6ecf9;&quot;&gt;uch as jpg and tif.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000&quot;&gt;Example:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;jpg;tif;cr2&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Filändelser på filer du vill kunna öppna, avdelade av cemikolon.&lt;span style=&quot; color:#000000; background-color:#e6ecf9;&quot;ådana som jpg och tif.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000; &quot;&gt;Exempel:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000&quot;&gt;jpg;tif;cr2&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="867"/>
        <source>File extension:</source>
        <oldsource>File extension: *.</oldsource>
        <translation>Filändelse:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="886"/>
        <source>*.</source>
        <translation>*.</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="168"/>
        <source>Date and time, separated by</source>
        <translation>Datum och tid, avdelade av</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="72"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The numbers in the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Siffrorna i filnamnet är avdelade av ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="109"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Date and time component part of the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Datum och tidsdelen av filnamnet är avdelade av ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="187"/>
        <source>File extension (.jpg or .JPG)</source>
        <translation>Filnamnstillägg (.jpg eller .JPG)</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="200"/>
        <source>CAPITAL letter</source>
        <translation>VERSALER</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="216"/>
        <source>lowercase letter</source>
        <translation>gemener</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="245"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="258"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The figures in the folder name are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <oldsource>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The figures in the folder name is separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</oldsource>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Siffrorna i mappnamnet är avdelade av ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="314"/>
        <source>File name:</source>
        <translation>Filnamn:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="333"/>
        <source>Folder name:</source>
        <translation>Mappnamn:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="352"/>
        <source>folder name</source>
        <translation>mappnamn</translation>
    </message>
    <message>
        <location filename="filepattern.cpp" line="97"/>
        <location filename="filepattern.cpp" line="321"/>
        <source> or .</source>
        <translation> eller .</translation>
    </message>
    <message>
        <location filename="filepattern.cpp" line="97"/>
        <location filename="filepattern.cpp" line="321"/>
        <source>Extension: .</source>
        <translation>Filändelse: .</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="help.cpp" line="62"/>
        <source>Close</source>
        <translation>Stäng</translation>
    </message>
    <message>
        <location filename="help.cpp" line="155"/>
        <source>Error reading
</source>
        <translation>Fel vid läsningen
</translation>
    </message>
    <message>
        <location filename="help.cpp" line="155"/>
        <source>
info file missing!</source>
        <translation>
info filen saknas!</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="127"/>
        <source>Copy (do not rename)</source>
        <translation>Kopiera (ej nytt namn)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="129"/>
        <source>Copy to...</source>
        <translation>Kopiera till...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="155"/>
        <source>Preserve original file name</source>
        <translation>Behåll orginalnamnet</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="157"/>
        <location filename="mainwindow.cpp" line="190"/>
        <location filename="mainwindow.cpp" line="400"/>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Before date/time</source>
        <translation>Före datum/tid</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="158"/>
        <location filename="mainwindow.cpp" line="191"/>
        <location filename="mainwindow.cpp" line="406"/>
        <location filename="mainwindow.cpp" line="419"/>
        <source>After date/time</source>
        <translation>Efter datum/tid</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="188"/>
        <source>Insert new file name</source>
        <translation>Infoga nytt filnamn</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="212"/>
        <source>Go!</source>
        <translation>Kör!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="231"/>
        <source>Clear log</source>
        <translation>Rensa logg</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="233"/>
        <source>Save log</source>
        <translation>Spara logg</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="351"/>
        <source>&amp;Open...</source>
        <translation>&amp;Öppna...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="352"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="353"/>
        <source>Open jpeg image(s)</source>
        <translation>Öppna jpeg bild(er)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="357"/>
        <source>&amp;Rename</source>
        <translation>&amp;Nytt namn</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="358"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="359"/>
        <source>Rename original file(s)</source>
        <translation>Nytt namn på originalfilen/originalfilerna</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="364"/>
        <source>&amp;Copy to...</source>
        <translation>&amp;Kopiera till...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="365"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="366"/>
        <source>Copy original file(s) with new name to folder...</source>
        <translation>Kopiera originalfilen/originalfilerna med nytt namn till folder...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="373"/>
        <source>E&amp;xit</source>
        <translation>&amp;Avsluta</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="374"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="394"/>
        <source>File name is only date/time</source>
        <translation>Filnamnet är endast datum/tid</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="402"/>
        <source>Preserve original file name before date/time</source>
        <translation>Behåll orginalnamn före datum/tid</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="408"/>
        <source>Preserve original file name after date/time</source>
        <translation>Behåll originalnam efter datum/tid</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="415"/>
        <source>Insert new file name before date/time</source>
        <translation>Infoga nytt filnamn före datum/tid</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="421"/>
        <source>Insert new file name after date/time</source>
        <translation>Infoga nytt filnamn efter datum/tid</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="426"/>
        <location filename="mainwindow.cpp" line="427"/>
        <source>Edit date and time...</source>
        <translation>Redigera datum och tid...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="434"/>
        <source>Help for application</source>
        <translation>Hjälp om programmet</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="439"/>
        <source>About application</source>
        <translation>Om programmet</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="461"/>
        <source>Italian</source>
        <translation>Italienska</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="467"/>
        <location filename="mainwindow.cpp" line="2336"/>
        <source>German</source>
        <translation>Tyska</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="468"/>
        <source>German language</source>
        <translation>Tyska språket</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2332"/>
        <source>Italiano</source>
        <translation>Italienska</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="462"/>
        <source>Italian language</source>
        <translation>Italienskt språk</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="473"/>
        <location filename="mainwindow.cpp" line="2333"/>
        <source>Russian</source>
        <translation>Ryska</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="474"/>
        <source>Russian language</source>
        <translation>Ryska språket</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="486"/>
        <source>Spanish language</source>
        <translation>Spanska språket</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="497"/>
        <location filename="mainwindow.cpp" line="498"/>
        <source>Specify file name pattern...</source>
        <translation>Filnamnsmönster...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="537"/>
        <source>&amp;File</source>
        <translation>&amp;Fil</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="545"/>
        <source>&amp;Edit</source>
        <translation>&amp;Redigera</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="551"/>
        <source>&amp;Preserve original file name</source>
        <translation>&amp;Behåll filnamnet</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="555"/>
        <source>&amp;Insert new file name</source>
        <translation>&amp;Infoga nytt filnamn</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="590"/>
        <source>&amp;Help</source>
        <translation>&amp;Hjälp</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="653"/>
        <source>File</source>
        <translation>Fil</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="673"/>
        <source>Create folders (yyyy</source>
        <translation>Skapa mappar (åååå</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="673"/>
        <source>mm</source>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="673"/>
        <source>dd)</source>
        <translation>dd)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="692"/>
        <location filename="mainwindow.cpp" line="2410"/>
        <source>Open File</source>
        <translation>Öppna fil</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="735"/>
        <location filename="mainwindow.cpp" line="2437"/>
        <source>Image files</source>
        <translation>Bildfiler</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1200"/>
        <location filename="mainwindow.cpp" line="1314"/>
        <location filename="mainwindow.cpp" line="1428"/>
        <source>Unable to delete:
</source>
        <translation>Kan inte ta bort:
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1627"/>
        <location filename="mainwindow.cpp" line="2100"/>
        <source>Licensierat under GNU General Public License This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version </source>
        <translation>Licensierat under GNU General Public License Detta program är fri programvara; du kan distribuera det och / eller modifiera det under villkoren i GNU General Public License, publicerad av Free Software Foundation, antingen version </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1627"/>
        <location filename="mainwindow.cpp" line="2100"/>
        <source> of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</source>
        <translation> av licensen, eller (om du så vill) någon senare version Detta program distribueras i hopp om att det ska vara användbart, men UTAN NÅGON GARANTI, även utan underförstådd garanti om SÄLJBARHET eller LÄMPLIGHET FÖR ETT VISST SYFTE Se GNU General Public License för fler detaljer. Du bör ha fått en kopia av GNU General Public License tillsammans med detta program, om inte, skriv till Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1630"/>
        <location filename="mainwindow.cpp" line="2099"/>
        <source>&lt;br&gt;&lt;font coor=&quot;green&quot; size=&quot;3&quot;&gt;&lt;b&gt;Copyright &amp;copy; 2007 - </source>
        <translation>&lt;br&gt;&lt;font coor=&quot;green&quot; size=&quot;3&quot;&gt;&lt;b&gt;Copyright &amp;copy; 2007 - </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>Thank you Ivan Dolgov for the translation into Russian.</source>
        <translation>Stort tack till Ivan Dolgov för den ryska översättningen.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>Thank you Roberto Nerozzi for the translation into Italien.</source>
        <translation>Stort tack till Roberto Nerozzi för den italienska översättningen.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>Thank you Cristian Sosa Noe for the translation into Spanish.</source>
        <translation>Stort tack till Cristian Sosa Noe för den spanska översättningen.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>Thank you Ben Weis for the translation into German.</source>
        <translation>Stort tack till Ben Weis för den tyska översättningen.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1663"/>
        <location filename="mainwindow.cpp" line="1704"/>
        <location filename="mainwindow.cpp" line="1743"/>
        <location filename="mainwindow.cpp" line="1785"/>
        <location filename="mainwindow.cpp" line="1827"/>
        <location filename="mainwindow.cpp" line="1868"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="479"/>
        <location filename="mainwindow.cpp" line="2334"/>
        <source>Swedish</source>
        <translatorcomment>Svenska</translatorcomment>
        <translation>Svenska</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="485"/>
        <location filename="mainwindow.cpp" line="2335"/>
        <source>Spanish</source>
        <translation>Spanska</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2351"/>
        <source>You must restart the application to the Russian language settings to take effect.</source>
        <translation>Du måste starta om programmet för att de ryska språkinställningarna ska börja gälla.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2374"/>
        <location filename="mainwindow.cpp" line="2381"/>
        <source>You must restart the application to the Spanish language settings to take effect.</source>
        <translation>Du måste starta om programmet för att de spanska språkinställningarna ska börja gälla.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2531"/>
        <source>MS-DOS-based version of Windows.</source>
        <translation>MS-DOS-baserad version av programmet.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2534"/>
        <source>NT-based version of Windows.</source>
        <translation>NT-basserad version av programmet.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2537"/>
        <source>CE-based version of Windows.</source>
        <translation>CE-basserad version av programmet.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2540"/>
        <source>Windows 3.1 with Win 32s operating system.</source>
        <translation>Windows 3.1 med Win 32s operativsystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2543"/>
        <source>Windows 95 operating system.</source>
        <translation>Windows 95 operativsystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2546"/>
        <source>Windows 98 operating system.</source>
        <translation>Windows 98 operativsystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2549"/>
        <source>Windows Me operating system.</source>
        <translation>Windows ME operativsystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2552"/>
        <source>Windows operating system version 4.0, corresponds to Windows NT.</source>
        <translation>Windows operativsystem version 4.0, korresponderande till Windows NT.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2555"/>
        <source>Windows operating system version 5.0, corresponds to Windows 2000.</source>
        <translation>Windows operativsystem version 5.0, korresponderande till Windows 2000.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2558"/>
        <source>Windows operating system version 5.1, corresponds to Windows XP.</source>
        <translation>Windows operativsystem version 5.1, korresponderande till Windows XP.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2561"/>
        <source>Windows operating system version 5.2, corresponds to Windows Server 2003, Windows Server 2003 R2, Windows Home Server, and Windows XP Professional x64 Edition.</source>
        <translation>Windows operativsystem version 5.2, korresponderande till Windows Server 2003, Windows Server 2003 R2, Windows Home Server, och Windows XP Professional x64 Edition.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2564"/>
        <source>Windows operating system version 6.0, corresponds to Windows Vista and Windows Server 2008.</source>
        <translation>Windows operativsystem version 6.0, korresponderande till Windows Vista och Windows Server 2008.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2567"/>
        <source>Windows operating system version 6.1, corresponds to Windows 7 and Windows Server 2008 R2.</source>
        <translation>Windows operativsystem version 6.1, korresponderande till Windows 7 och Windows Server 2008 R2.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2570"/>
        <source>Windows operating system version 6.2, corresponds to Windows 8</source>
        <oldsource>Windows operating system version 6.2, corresponds to Windows 8.</oldsource>
        <translation>Windows operativsystem version 6.2, korresponderande till Windows 8</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2573"/>
        <source>Operating system version 6.3, corresponds to Windows 8.1</source>
        <translation>Windows operativsystem version 6.3, korresponderande till Windows 8.1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2576"/>
        <source>Operating system version 10.0, corresponds to Windows 10</source>
        <translation>Windows operativsystem version 10.0, korresponderande till Windows 10</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2579"/>
        <source>Windows CE operating system.</source>
        <translation>Windows CE operativsystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2582"/>
        <source>Windows CE .NET operating system.</source>
        <translation>Windows CE .NET operativsystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2585"/>
        <source>Windows CE 5.x operating system.</source>
        <translation>Windows CE 5.x operativsystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2588"/>
        <source>Windows CE 6.x operating system.</source>
        <translation>Windows CE 6.x operativsystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2591"/>
        <source>Unknown Windows operating system</source>
        <translation>Okännt Windows operativsystem</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2600"/>
        <source>Architecture instruction set: </source>
        <translation>Arkitekturens instruktionsuppsättning: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2611"/>
        <source> was created </source>
        <translation> skapades </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2621"/>
        <location filename="mainwindow.cpp" line="2623"/>
        <location filename="mainwindow.cpp" line="2658"/>
        <source>Compiled by</source>
        <translation>Kompilerad av</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2654"/>
        <source>Unknown version</source>
        <translation>Okänd version</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2663"/>
        <source>Unknown compiler.</source>
        <translation>Okänd kompilator.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2367"/>
        <source>You must restart the application to the Swedish language settings to take effect.</source>
        <translation>Du måste starta om programmet för att de svenska språkinställningarna ska börja gälla.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2441"/>
        <source>All files</source>
        <translation>Alla filer</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="762"/>
        <location filename="mainwindow.cpp" line="2465"/>
        <source>You do not have enough permissions to read
</source>
        <translation>Du har inte rättighet att läsa
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="795"/>
        <location filename="mainwindow.cpp" line="1953"/>
        <location filename="mainwindow.cpp" line="1998"/>
        <source>The original file will be renamed</source>
        <translation>Originalfilen kommer att få nytt namn</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="941"/>
        <source>Copy pictures to folder</source>
        <translation>Kopiera bilder till folder</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="963"/>
        <source>You have not enough permissions
to write to </source>
        <translation>Du har inte rättighet
att skriva till </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1056"/>
        <source>rename</source>
        <translation>nytt namn</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1058"/>
        <source>copy</source>
        <translation>kopiera</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1090"/>
        <source>Error in destination path
Please select another output directory!</source>
        <translation>Fel i sökvägen till distinationen
Välj en annan folder att kopiera till!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1525"/>
        <source>/exif_rename_log.txt</source>
        <oldsource>/exif_rename_logg.txt</oldsource>
        <translation>/exif_rename_logg.txt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1536"/>
        <source>Text files (*.txt)</source>
        <translation>Textfiler (*.txt)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1536"/>
        <source>Any files (*)</source>
        <translation>Alla filer (*)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2359"/>
        <source>You must restart the application to the Italien language settings to take effect.</source>
        <translation>Du måste starta om programmet för att de italienska språkändringen ska börja gälla.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2477"/>
        <source>Changes the EXIF information for these files</source>
        <translation>Ändra EXIF data för dessa filer</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2486"/>
        <source> ...selected</source>
        <translation> ...valda</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="969"/>
        <location filename="mainwindow.cpp" line="1094"/>
        <location filename="mainwindow.cpp" line="1965"/>
        <location filename="mainwindow.cpp" line="1967"/>
        <location filename="mainwindow.cpp" line="1988"/>
        <location filename="mainwindow.cpp" line="1990"/>
        <source>Copy to: </source>
        <translation>Kopiera till: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1376"/>
        <source>Indicate a new file name, please!</source>
        <translation>Skriv in ett nytt filnamn!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1528"/>
        <source>Save log as...</source>
        <translation>Spara logg som...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1555"/>
        <source>Error writing to
</source>
        <translation>Fel vid skrivning till
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1555"/>
        <source>
check your permissions</source>
        <translation>Kontrolera dina rättigheter</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1632"/>
        <source>About </source>
        <translation>Om </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1934"/>
        <source>Ready for action!</source>
        <translation>Klart att köra!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2185"/>
        <location filename="mainwindow.cpp" line="2189"/>
        <source>&lt;/b&gt; and renamed: </source>
        <translation>&lt;/b&gt; och namnändrad:  </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2200"/>
        <source>&lt;br /&gt;Selected file(s) in: &lt;b&gt;</source>
        <oldsource>&lt;br /&gt;Selected files in: &lt;b&gt;</oldsource>
        <translation>&lt;br /&gt;Valda file(r) i: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="455"/>
        <location filename="mainwindow.cpp" line="2331"/>
        <source>English</source>
        <translation>English</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="456"/>
        <source>English language</source>
        <translation>Engelskt språk</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="480"/>
        <source>Swedish language</source>
        <translation>Svenskt språk</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="574"/>
        <source>&amp;Language</source>
        <translation>&amp;Språk</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>No name exept date/time</source>
        <translation>Inget namn utom datum/tid</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1661"/>
        <location filename="mainwindow.cpp" line="1702"/>
        <location filename="mainwindow.cpp" line="1741"/>
        <location filename="mainwindow.cpp" line="1783"/>
        <location filename="mainwindow.cpp" line="1825"/>
        <location filename="mainwindow.cpp" line="1866"/>
        <source>The application needs to restart in order
to initialise the now language settings.

Do you want to close
the application now?</source>
        <translation>Programmet behöver startas om för att
de nya språkinställningarna ska börja gälla.

Vill du stänga 
programmet nu?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="515"/>
        <source>Display text alongside icons in the toolbar</source>
        <translation>Visa text bredvid ikonerna i verktygsfältet</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="520"/>
        <source>Display text under icons in the toolbar</source>
        <translation>Visa text under ikonerna i verktygsfältet</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="523"/>
        <source>No toolbar</source>
        <translation>Inget verktygsfält</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="524"/>
        <source>No toolbar will be displayed</source>
        <translation>Inget verktygsfält visas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="564"/>
        <location filename="mainwindow.cpp" line="655"/>
        <source>Toolbar</source>
        <translation>Verktygsfält</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="607"/>
        <location filename="mainwindow.cpp" line="1663"/>
        <location filename="mainwindow.cpp" line="1704"/>
        <location filename="mainwindow.cpp" line="1743"/>
        <location filename="mainwindow.cpp" line="1785"/>
        <location filename="mainwindow.cpp" line="1827"/>
        <location filename="mainwindow.cpp" line="1868"/>
        <location filename="mainwindow.cpp" line="2105"/>
        <source>No</source>
        <translation>Nej</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="606"/>
        <location filename="mainwindow.cpp" line="1662"/>
        <location filename="mainwindow.cpp" line="1703"/>
        <location filename="mainwindow.cpp" line="1742"/>
        <location filename="mainwindow.cpp" line="1784"/>
        <location filename="mainwindow.cpp" line="1826"/>
        <location filename="mainwindow.cpp" line="1867"/>
        <location filename="mainwindow.cpp" line="2104"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1632"/>
        <location filename="mainwindow.cpp" line="2103"/>
        <source>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</source>
        <translation>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="493"/>
        <source>Remove your personal settings</source>
        <translation>Ta bort dina personliga inställningar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="585"/>
        <source>&amp;Tools</source>
        <translation>&amp;Verktyg</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="605"/>
        <source>Do you want to remove your personal settings?
This is an appropriate action if you like to uninstall this application.
</source>
        <translation>Vill du tabort dina personliga inställningar?
Det är lämpligt om du ska avinstallera detta program.
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="605"/>
        <source> will exit after your personal data is deleted.</source>
        <translation> kommer att avslutas när dina personliga data har tagits bort.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="615"/>
        <source>Your personal settings are deleted and
 </source>
        <translation>Dina personliga inställningar har tagits bort och
 </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="615"/>
        <source> will exit</source>
        <translation> kommer att avslutas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="375"/>
        <source>Exit application</source>
        <translation>Avsluta programmet</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="506"/>
        <source>Icons only</source>
        <translation>Endast ikoner</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="507"/>
        <source>Display only icons in the toolbar</source>
        <translation>Visa bara ikoner i verktygsfältet</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="510"/>
        <source>Text only</source>
        <translation>Endast text</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="511"/>
        <source>Display only text in the toolbar</source>
        <translation>Visa endast text i verktygsfältet</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="514"/>
        <source>Text alongside icons</source>
        <translation>Text bredvid ikoner</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="519"/>
        <source>Text under icons</source>
        <translation>Text under ikoner</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="621"/>
        <source>Your personal settings cannot be found.
New settings will be saved then you restart the program.</source>
        <translation>Dina personliga inställningar kan inte hittas.
Nya inställningar kommer att sparas när du startar om programmet.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1064"/>
        <source>You have not selected any file(s).
Please select file(s) to </source>
        <translation>Du har inte valt någon fil/några filer.
Välj fil/filer att ge </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1568"/>
        <source>Copyright Ingemar Ceicer 2007 - </source>
        <translation>Copyright Ingemar Ceicer 2007 - </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1630"/>
        <location filename="mainwindow.cpp" line="2099"/>
        <source>homepage</source>
        <translation>hemsida</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>A small application for renaming pictures. EXIF ReName uses exif data from the picture file.&lt;br&gt;&lt;br&gt;</source>
        <translation>Ett litet program för att namnändra bilder. EXIF ReName använder exifdata från bildfilen.&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2103"/>
        <source>&lt;br&gt;A small application for renaming pictures. EXIF ReName uses exif data from the picture file.&lt;br&gt;&lt;br&gt;</source>
        <translation>&lt;br&gt;Ett litet program för att namnändra bilder. EXIF ReName använder exifdata från bildfilen..&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2103"/>
        <source>&lt;br&gt;&lt;br&gt;&lt;font color=&quot;#FF0000&quot; size=&quot;4&quot;&gt;Do you accept the license?&lt;/font&gt;</source>
        <translation>&lt;br&gt;&lt;br&gt;&lt;font color=&quot;#FF0000&quot; size=&quot;4&quot;&gt;Accepterar du licensvilkoren?&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="785"/>
        <location filename="mainwindow.cpp" line="786"/>
        <source> File(s) selected</source>
        <translation> Fil(er) valda</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1491"/>
        <source>Done! </source>
        <translation>Klart! </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1491"/>
        <source> of </source>
        <translation> av </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1491"/>
        <source>file(s) )</source>
        <translation>fil(er) )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1560"/>
        <source>*** Log saved: </source>
        <translation>*** Logg sparad: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1566"/>
        <source>Log created by </source>
        <translation>Logg skapad av </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2141"/>
        <location filename="mainwindow.cpp" line="2149"/>
        <source> is updatet to </source>
        <translation> är uppdaterad till </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="492"/>
        <source>Delete personal settings</source>
        <translation>Radera personliga inställningar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="385"/>
        <source>Create folders (yyyy_mm_dd)</source>
        <translation>Skapa kataloger (åååå_mm_dd)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="387"/>
        <source>Create folders according to file creation date and copy the files into these folders</source>
        <translation>Skapa kataloger som har namn efter år, månad och dag som filen skapades och kopiera filen dit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1178"/>
        <location filename="mainwindow.cpp" line="1241"/>
        <location filename="mainwindow.cpp" line="1292"/>
        <location filename="mainwindow.cpp" line="1348"/>
        <location filename="mainwindow.cpp" line="1407"/>
        <location filename="mainwindow.cpp" line="1466"/>
        <source>Unable to create directory 
</source>
        <translation>Kan inte skapa en folder 
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1178"/>
        <location filename="mainwindow.cpp" line="1241"/>
        <location filename="mainwindow.cpp" line="1292"/>
        <location filename="mainwindow.cpp" line="1348"/>
        <location filename="mainwindow.cpp" line="1407"/>
        <location filename="mainwindow.cpp" line="1466"/>
        <source>
check your permisions</source>
        <translation>kontrollera dina rättigheter</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="444"/>
        <source>About Qt C++ Application Development Framework</source>
        <translation>Om Qt, ramverk för att utveckla program i C++</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2185"/>
        <source>Copied to new folder in: &lt;b&gt;</source>
        <translation>Kopierad till ny mapp i: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2189"/>
        <source>Copied to: &lt;b&gt;</source>
        <translation>Kopierad till: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2337"/>
        <source>Choose the language that will be used by EXIF Rename</source>
        <translation>Välj språk för EXIF Rename</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="562"/>
        <source>&amp;View</source>
        <translation>&amp;Visa</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="433"/>
        <source>Help...</source>
        <translation>Hjälp...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="438"/>
        <source>About...</source>
        <translation>Om...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="443"/>
        <source>About Qt...</source>
        <translation>Om Qt...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="448"/>
        <source>Check for updates...</source>
        <translation>Sök efter uppdateringar...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="449"/>
        <source>Check for new versions of EXIF ReName</source>
        <translation>Sök efter nya versioner av EXIF ReName</translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <location filename="checkupdate.cpp" line="46"/>
        <source>Cancel</source>
        <translation>Avbryt</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="134"/>
        <source>Error message: Host found but
the requested version information was not found.
Please try again later</source>
        <translation>Felmeddelande: Värd hittad men
den sökta versionsinformationen hittas inte
Var vänlig försök igen senare</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="135"/>
        <location filename="checkupdate.cpp" line="149"/>
        <location filename="checkupdate.cpp" line="190"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="148"/>
        <source>Error message: </source>
        <translation>Felmeddelane: </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source>New version available: </source>
        <translation>Ny version är tillgänglig: </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source>Download it from</source>
        <translation>Ladda ner den från</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source> homepage</source>
        <translation> hemsida</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="181"/>
        <source>Your current version is the latest available: </source>
        <translation>Du använder den senaste versionen: </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="181"/>
        <source>Thank you for using</source>
        <translation>Tack för att du använder</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>homepage</source>
        <translation>hemsida</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="50"/>
        <source>Check for updates on startup</source>
        <translation>Sök efter uppdateringar vid start</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="148"/>
        <source>
Please try again later</source>
        <translation>
Vänligen försök igen senare</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>New version available!</source>
        <translation>Det finns en ny version!</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>Version </source>
        <translation>Version </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>Please visit</source>
        <translation>Vänligen besök</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="62"/>
        <source>Contacting server...</source>
        <translation>Kontaktar servern...</translation>
    </message>
</context>
</TS>
