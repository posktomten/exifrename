/*
    EXIF ReName
    Copyright (C) 2007-2019 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/


#ifndef FILEPATTERN_H
#define FILEPATTERN_H

#include <QtGui/QDialog>
#include <QCloseEvent>
#include <QDateTime>
#include "config.h"

namespace Ui
{
class FilePattern;
}

class FilePattern : public QDialog
{
    Q_OBJECT

public:
    FilePattern(QWidget *parent = 0);
    ~FilePattern();

protected:
    void closeEvent(QCloseEvent *event);

private:
    Ui::FilePattern *m_ui;
    QString format_date_time();
    QString format_folder_date_time();
    void setConfig();
    Config *k2;

private slots:
    void display_value(int i);
    void display_value(bool b);
    void display_value(QString s);
    void display_folder_value(int i);
    void cursorChanged(int i, int j);
    void spara();

};

#endif // FILEPATTERN_H

