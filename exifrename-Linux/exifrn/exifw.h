/*
    EXIF ReName
    Copyright (C) 2007-2019 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#ifndef EXIFW_H
#define EXIFW_H

#include "mainwindow.h"
#include "config.h"
using namespace std;

class Exifw
{



private:

    bool finn(char *hela, int ganger);
    bool s(char c);
    bool sa(char c);
    bool sb(char c);
    int raknare;
    bool manipulera(char *filnamn, char *bytut, char *info);
    char sep;
    char dt_sep;
    char *funnit;

public:
    Exifw();
    bool setExif(char *filnamn, char *hittat, char *info);




};

#endif // EXIFW_H
