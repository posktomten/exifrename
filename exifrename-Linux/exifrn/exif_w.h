
/*
    EXIF ReName
    Copyright (C) 2007-2019 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#ifndef EXIF_W_H
#define EXIF_W_H

#include <QtGui/QDialog>
#include <QDir>
#include <QMessageBox>


namespace Ui
{
class Exif_w;
}

class Exif_w : public QDialog
{
    Q_OBJECT

public:
    Exif_w(QWidget *parent = 0);
    ~Exif_w();

    void startExif(QStringList *filnamn2);


protected:
    void closeEvent(QCloseEvent *event);

private:
    Ui::Exif_w *m_ui;
    char *fnamn;
    void manipulateExif(QString filnamnet);
    QStringList fil_namn;



private slots:
    void spara();
    void current_time();

};

#endif // EXIF_W_H
