
/*
    EXIF ReName
    Copyright (C) 2007-2019 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/


#ifndef EXIFH
#define EXIFH
#include "mainwindow.h"
#include "config.h"
using namespace std;

class Exif
{
private:

    void format_good(char *s);
    void finn(char *hela, char *hittat);
    bool s(char c);
    bool sa(char c);
    bool sb(char c);

    char sep;
    char dt_sep;

public:
    Exif();
    bool getExif(char *filnamn, char *hittat, bool formatera, char *info);






};
#endif
