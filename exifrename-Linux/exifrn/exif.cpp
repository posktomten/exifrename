/*
    EXIF ReName
    Copyright (C) 2007-2019 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/




#include <fstream>
#include "exif.h"
const int LAS_LANGD = 5120;
// 3072
using namespace std;

Exif::Exif()
{
    QString conf_file_name = QDir::toNativeSeparators("/exif_rename3.conf");
    Config *k4 = new Config(conf_file_name, 2);
    string separator_s = k4->getConf("separator");

    if(separator_s == "none")
        sep = 'x';

    if(separator_s == "space")
        sep = ' ';

    if(separator_s == "-")
        sep = '-';

    if(separator_s == "_")
        sep = '_';

    string date_time_separator_s = k4->getConf("date_time_separator");

    if(date_time_separator_s == "none")
        dt_sep = 'x';

    if(date_time_separator_s == "space")
        dt_sep = ' ';

    if(date_time_separator_s == "-")
        dt_sep = '-';

    if(date_time_separator_s == "_")
        dt_sep = '_';

    delete k4;
}

bool Exif::getExif(char *filnamn, char *hittat, bool formatera, char *info)
{
    char in;
    char hela[LAS_LANGD] = {};
    ifstream lasfil;
    /* Öppna filen. */
    lasfil.open(filnamn, ios::binary);

    if(! lasfil.good()) {
        strcpy(filnamn, "");
        strcpy(info, "Unable to open ");
        return false;
    }

    for(int i = 0; i < LAS_LANGD; i++) {
        lasfil.read((char*) &in, 1);
        hela[i] = in;
    }

    strcpy(hittat, "");
    finn(hela, hittat);

    if(strcmp(hittat, "") == 0) {
        strcpy(filnamn, "");
        strcpy(info, "No EXIF data found in ");
        return false;
    }

    if(formatera)
        format_good(hittat);

    return true;
}
void Exif::finn(char *h, char *hittat)
{
    int k = 0;

    for(int i = 0; i <= LAS_LANGD; i++) {
        if(s(h[i]) && s(h[i + 1]) && s(h[i + 2]) && s(h[i + 3]) && s(h[i + 5]) && sa(h[i + 4]) && s(h[i + 5]) && s(h[i + 6]) && sa(h[i + 7]) && s(h[i + 8]) && s(h[i + 9]) && sb(h[i + 10]) && s(h[i + 11]) && s(h[i + 12]) && sa(h[i + 13]) && s(h[i + 14]) && s(h[i + 15]) && sa(h[i + 16]) && s(h[i + 17]) && s(h[i + 18])) {
            k++;

            if(k == 1) {
                for(int k = 0; k < 19; k++, i++) {
                    hittat[k] = h[i];
                }
            }

            if(k > 1) {
                for(int k = 0; k < 19; k++, i++) {
                    hittat[k] = h[i];
                }

                return;
            }
        }
    }
}

bool Exif::s(char c)
{
    if(c >= '0' && c <= '9')
        return true;

    return false;
}
bool Exif::sa(char c)
{
    if(c == ':')
        return true;

    return false;
}
bool Exif::sb(char c)
{
    if(c == ' ')
        return true;

    return false;
}

void Exif::format_good(char *s)
{
    s[4] = sep;
    s[7] = sep;
    s[10] = dt_sep;
    s[13] = sep;
    s[16] = sep;
}



