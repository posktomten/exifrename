/*
    EXIF ReName
    Copyright (C) 2007-2019 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "exif_w.h"
#include "exifw.h"
#include "exif.h"
#include "ui_exif_w.h"





Exif_w::Exif_w(QWidget *parent) :
    QDialog(parent),
    m_ui(new Ui::Exif_w)
{
    m_ui->setupUi(this);
    this->setWindowTitle(TITLE_STRING);
    connect(m_ui->pbCancel, SIGNAL(clicked()), this, SLOT(close()));
    connect(m_ui->pbOk, SIGNAL(clicked()), this, SLOT(spara()));
    connect(m_ui->pbCurrentTime, SIGNAL(clicked()), this, SLOT(current_time()));
}



Exif_w::~Exif_w()
{
    delete m_ui;
}

void Exif_w::closeEvent(QCloseEvent *event)
{
    event->accept();
}
void Exif_w::startExif(QStringList *filnamn_in)
{
    fil_namn = *filnamn_in;
    manipulateExif(fil_namn[0]);
}

void Exif_w::spara()
{
    QString from_le = m_ui->leDateTime->text();
    from_le.remove(QChar('-'), Qt::CaseInsensitive);
    from_le.remove(QChar(' '), Qt::CaseInsensitive);

    if(from_le.size() != 14) {
        QDateTime dateTime = QDateTime::currentDateTime();
        QString ct = dateTime.toString("yyyy-MM-dd hh-mm-ss");
        QMessageBox::critical(this, TITLE_STRING, tr("Incorrect date/time format, unable to write to file.\nYou must write in this way: ") + ct);
    } else {
        char *hittat = new char[20];
        char *info = new char[50];
        std::string from_le_s = from_le.toStdString();
        strcpy(hittat, from_le_s.c_str());
        Exifw *exw = new Exifw;
        bool b = exw->setExif(fnamn, hittat, info);
        QString s = QString::fromLatin1(info);
        delete exw;
        delete[] hittat;
        delete[] info;

        if(fil_namn.isEmpty() == false)
            fil_namn.pop_front();

        if(b == false) {
            QMessageBox::critical(this, TITLE_STRING, tr("Unable to save EXIF data"));
        }

        if(fil_namn.isEmpty() == false)
            manipulateExif(fil_namn[0]);

        if(fil_namn.isEmpty() == true)
            delete this;
    }
}



void Exif_w::manipulateExif(QString filnamn_qs)
{
    QString filnamn_qs2 = filnamn_qs;
    filnamn_qs2 = QDir::toNativeSeparators(filnamn_qs2);
    int pos = filnamn_qs2.lastIndexOf(QDir::toNativeSeparators("/"));
    QString filnamnet = filnamn_qs.mid(pos + 1, filnamn_qs2.size());
    m_ui->lblTidsstampel->setText(filnamnet);
    this->show();
    std::string filnamn_s = filnamn_qs.toStdString();
    char *hittat = new char[20];
    char *filnamn_char = new char[300];
    char *info = new char[50];
    strcpy(filnamn_char, filnamn_s.c_str());
    Exif *ex = new Exif;
    bool hittade = ex->getExif(filnamn_char, hittat, false, info);

    if(hittade == false) {
        QMessageBox::critical(this, TITLE_STRING, info + filnamnet);

        if(fil_namn.isEmpty() == false)
            fil_namn.pop_front();

        if(fil_namn.isEmpty() == false)
            manipulateExif(fil_namn[0]);

        if(fil_namn.isEmpty() == true)
            delete this;
    }

    if(hittade == true) {
        QString timestamp = QString::fromLatin1(hittat);
        m_ui->leDateTime->setTextMargins(10, 5, 10, 5);
        m_ui->leDateTime->setInputMask("0000-00-00 00-00-00");
        m_ui->leDateTime->setText(timestamp);
        fnamn = new char[300];
        strcpy(fnamn, filnamn_char);
    }

    delete[] info;
    delete[] filnamn_char;
    delete[] hittat;
    delete ex;
}

// private slots:
void Exif_w::current_time()
{
    QDateTime dateTime = QDateTime::currentDateTime();
    QString ct = dateTime.toString("yyyy-MM-dd hh-mm-ss");
    m_ui->leDateTime->setText(ct);
}

