/*
    EXIF ReName
    Copyright (C) 2007-2019 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/



#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include "config.h"
#include "exif.h"
#include "help.h"
#include "filepattern.h"
#include <QMenuBar>
#include <QTextEdit>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QButtonGroup>
#include <QRadioButton>
#include <QFileDialog>
#include <QToolBar>
#include <QStatusBar>
#include <QLabel>
#include <QGridLayout>
#include <QMainWindow>
#include <QCloseEvent>
#include <QAction>
#include <QProgressBar>
#include <QRegExp>
#include <QProcess>

#include <QSysInfo>
#include <QLibraryInfo>
#include <QApplication>


#define TITLE_STRING "EXIF ReName 0.1.15"
#define VERSION "0.1.15"
#define EXECUTABLE_NAME "exifrn"
#define CURRENT_YEAR "2019"
#define LICENCE_VERSION "3"
#define STANDARD_EXTENSION "jpg"

#define APPLICATION_HOMEPAGE "\"http://ceicer.org/exifrename/\""
#define APPLICATION_HOMEPAGE_ENG "\"http://ceicer.org/exifrename/index_eng.php\""

#define BUILD_DATE_TIME __DATE__ " " __TIME__

class MainWindow : public QMainWindow
{


    Q_OBJECT
    friend class FilePattern;
public:

    MainWindow();
    ~MainWindow();


    Config *k;

protected:
    void closeEvent(QCloseEvent *event);
    //void mousePressEvent(QMouseEvent *event);



private slots:
    void edit_menu_aboutToShow();
    void open();
    void rename();
    void copy_to();
    void dontAddFileName();
    void createFolder();
    void preserveText();
    //void preserve_beforeText();
    //void preserve_afterText();
    void m_preserve_beforeText();
    void m_preserve_afterText();
    void insertText();
    //void insert_beforeText();
    //void insert_afterText();
    void m_insert_beforeText();
    void m_insert_afterText();
    void copy();
    void li_insert_change(const QString&);
    void go();
    void clear_log(); //DISPLAY
    void save_log();  //DISPLAY
    void help(); //HELP
    void about(); //HELP

    void aboutQt(); //About Qt

    void engelska(); //LANGUAGE
    void svenska(); //LANGUAGE
    void ryska(); //LANGUAGE
    void italienska(); //LANGUAGE
    void spanska(); //LANGUAGE
    void tyska(); //LANGUAGE

    void icons_only(); //TOOLBAR
    void text_only(); //TOOLBAR
    void icons_text_alongside(); //TOOLBAR
    void icons_text_under(); //TOOLBAR
    void no_toolbar(); //TOOLBAR

    void delete_personal_data(); //TOOLBAR
    void fileNamePattern(); //TOOLBAR


    void check_update(); //Help menu

    //void exif_method();
    //void exif_method2();
//Edit EXIF
    void edit_exif();

private:
    QWidget *w;


    QString displayFolderSeparators();





    QVBoxLayout *topTopLayout;
    QGridLayout *topLayout;


    QVBoxLayout *layout;

    //QVBoxLayout *copyAllLayout;
    QGridLayout *copyLayout;


    QGridLayout *preserveLayout;
    QVBoxLayout *preserve_raLayout;

    QVBoxLayout *insert_allLayout;
    QGridLayout *insertLayout;
    QVBoxLayout *insert_raLayout;
    QVBoxLayout *ok_layout;


    QVBoxLayout *displayLayout;
    QHBoxLayout *log_buttomLayout;

    void createActions();
    void createMenus();
    void createToolBars();
    void createStatusBar();



    QAction *openAct;
    QAction *renameAct;
    QAction *copy_toAct;
    QAction *exitAct;
    QAction *dontAddFileNameAct;
    QAction *createFolderAct;
    // QAction *preserveAct;
    QAction *preserve_beforeAct;
    QAction *preserve_afterAct;
    // QAction *insertAct;
    QAction *insert_beforeAct;
    QAction *insert_afterAct;

//Ändra EXIF data
    QAction *edit_exifAct;



    QAction *helpAct;
    QAction *aboutAct;
    QAction *aboutQtAct; // About Qt


    QAction *engAct;
    QAction *svAct;
    QAction *spAct;
    QAction *ruAct;
    QAction *itAct;
    QAction *deAct;

    QAction *toolbarAct;
    QAction *iconsOnlyAct;
    QAction *textOnlyAct;
    QAction *textAlongsideIconsAct;
    QAction *textUnderIconsAct;
    QAction *noToolbarAct;
    QAction *fileNamePatternAct;


    QAction *delete_personal_dataAct;

    QAction *check_updateAct;


    QToolBar *fileToolBar;
    QMenu *fileMenu;
    QMenu *editMenu;
    QMenu *preserveMenu;
    QMenu *insertMenu;

    QMenu *helpMenu;

    QMenu *sprakMenu;

    QMenu *toolsMenu;


    QMenu * wievMenu;
    QMenu * toggleToolbarMenu;

    QLineEdit *li_insert;
    QCheckBox *ch_preserve;
    QButtonGroup *preserve;
    QRadioButton *ra_before_preserve;
    QRadioButton *ra_after_preserve;
    QCheckBox *ch_insert;
    QButtonGroup *insert;
    QRadioButton *ra_before_insert;
    QRadioButton *ra_after_insert;
    QCheckBox *ch_copy;
    QPushButton *pu_copy;
    QLabel *la_copy_path;
    QFileDialog *inputFileDialog;
    QFileDialog *outputFileDialog;
    QPushButton *pu_go;
    // QCheckBox *ch_exif;

    //DISPLAY
    QTextEdit *te_disp;
    QPushButton *pu_clear_log;
    QPushButton *pu_save_log;



    QStringList fileNames;



    bool test_copy_path(std::string);
    bool check_first_time();
    void check_copy();
    void setConfig();

    void synkronisera(bool bool_insert, bool bool_preserve);

    //DISPLAY
    void disp_log(QStringList::const_iterator, QString);
    void disp_log(QStringList::const_iterator, QString, QString);
    void selectedFiles(QString *directoryName);
    QString stripNames(QStringList::const_iterator ps);
    QString stripNames(QString s);


    QProgressBar *progress;

    void new_version_config();
    void sett_language();
    void startup_check();

    QString getSystem();

};

#endif
