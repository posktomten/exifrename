<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>Exif_w</name>
    <message>
        <location filename="exif_w.ui" line="35"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="72"/>
        <source>Date and time</source>
        <translation>Datum und Uhrzeit</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="85"/>
        <source>Change the time stamp on</source>
        <translation>Zeitstempel ändern auf</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="98"/>
        <source>TextLabel</source>
        <translation>TextBeschriftung</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="111"/>
        <source>Year-Month-Day Hour-Minute-Second</source>
        <translation>Jahr-Monat-Tag Stunde-Minute-Sekunde</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="124"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="137"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="150"/>
        <source>Current Time</source>
        <translation>Aktuelle Uhrzeit</translation>
    </message>
    <message>
        <location filename="exif_w.cpp" line="72"/>
        <source>Incorrect date/time format, unable to write to file.
You must write in this way: </source>
        <translation>Falsches Datum/Zeit-Format, kann nicht in Datei schreiben.
Sie müssen so schreiben: </translation>
    </message>
    <message>
        <location filename="exif_w.cpp" line="101"/>
        <source>Unable to save EXIF data</source>
        <translation>EXIF-Daten können nicht gespeichert werden</translation>
    </message>
</context>
<context>
    <name>FilePattern</name>
    <message>
        <location filename="filepattern.ui" line="59"/>
        <source>file name</source>
        <translation>Dateiname</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="72"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The numbers in the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Die Zahlen im Dateinamen sind getrennt durch ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="80"/>
        <location filename="filepattern.ui" line="117"/>
        <location filename="filepattern.ui" line="266"/>
        <source>none</source>
        <translation>kein</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="85"/>
        <location filename="filepattern.ui" line="122"/>
        <location filename="filepattern.ui" line="271"/>
        <source>space</source>
        <translation>Leerzeichen</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="90"/>
        <location filename="filepattern.ui" line="127"/>
        <location filename="filepattern.ui" line="276"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="95"/>
        <location filename="filepattern.ui" line="132"/>
        <location filename="filepattern.ui" line="281"/>
        <source>_</source>
        <translation>_</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="109"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Date and time component part of the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Datum- und Zeitbestandteil des Dateinamens sind durch getrennt durch ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="149"/>
        <source>File name figures, separated by</source>
        <translation>Dateinamenzahlen, getrennt durch</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="168"/>
        <source>Date and time, separated by</source>
        <translation>Datum und Zeit, getrennt durch</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="187"/>
        <source>File extension (.jpg or .JPG)</source>
        <translation>Dateierweiterung (.jpg oder .JPG)</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="200"/>
        <source>CAPITAL letter</source>
        <translation>GROSSbuchstabe</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="216"/>
        <source>lowercase letter</source>
        <translation>kleinbuchstabe</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="232"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="245"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="258"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The figures in the folder name are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Die Zahlen im Ordnernamen sind getrennt durch ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="295"/>
        <source>Folder name figures, separated by</source>
        <translation>Ordnernamenzahlen, getrennt durch</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="314"/>
        <source>File name:</source>
        <translation>Dateiname:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="333"/>
        <source>Folder name:</source>
        <translation>Ordnername:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="352"/>
        <source>folder name</source>
        <translation>Ordnername</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="373"/>
        <source>Only time in file name</source>
        <translation>Nur Zeit in Dateiname</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="834"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Extensions of files you want to be able to open, separated by cemikolon. S&lt;span style=&quot; color:#000000;&quot;&gt;uch as jpg and tif.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000&quot;&gt;Example:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;jpg;tif;cr2&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Dateierweiterungen, die Sie öffnen können, getrennt durch Strichpunkt. &lt;span style=&quot; color:#000000;&quot;&gt;Wie jpeg und tiff.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000&quot;&gt;Beispiel:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;jpg;tif;cr2&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="867"/>
        <source>File extension:</source>
        <translation>Dateierweiterung:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="886"/>
        <source>*.</source>
        <translation>*.</translation>
    </message>
    <message>
        <location filename="filepattern.cpp" line="97"/>
        <location filename="filepattern.cpp" line="321"/>
        <source>Extension: .</source>
        <translation>Erweiterung: .</translation>
    </message>
    <message>
        <location filename="filepattern.cpp" line="97"/>
        <location filename="filepattern.cpp" line="321"/>
        <source> or .</source>
        <translation> oder .</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="help.cpp" line="62"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="help.cpp" line="155"/>
        <source>Error reading
</source>
        <translation>Fehler beim Lesen
</translation>
    </message>
    <message>
        <location filename="help.cpp" line="155"/>
        <source>
info file missing!</source>
        <translation>
Info-Datei fehlt!</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="127"/>
        <source>Copy (do not rename)</source>
        <translation>Kopieren (nicht umbenennen)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="129"/>
        <source>Copy to...</source>
        <translation>Kopieren nach...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="155"/>
        <source>Preserve original file name</source>
        <translation>Originaldateiname beibehalten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="157"/>
        <location filename="mainwindow.cpp" line="190"/>
        <location filename="mainwindow.cpp" line="400"/>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Before date/time</source>
        <translation>Vor Datum/Zeit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="158"/>
        <location filename="mainwindow.cpp" line="191"/>
        <location filename="mainwindow.cpp" line="406"/>
        <location filename="mainwindow.cpp" line="419"/>
        <source>After date/time</source>
        <translation>Nach Datum/Zeit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="188"/>
        <source>Insert new file name</source>
        <translation>Neuen Dateinamen einfügen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="212"/>
        <source>Go!</source>
        <translation>Los!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="231"/>
        <source>Clear log</source>
        <translation>Protokoll löschen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="233"/>
        <source>Save log</source>
        <translation>Protokoll speichern</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="351"/>
        <source>&amp;Open...</source>
        <translation>&amp;Öffnen...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="352"/>
        <source>Ctrl+O</source>
        <translation>Strg+O</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="353"/>
        <source>Open jpeg image(s)</source>
        <translation>jpeg-Bild(er) öffnen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="357"/>
        <source>&amp;Rename</source>
        <translation>&amp;Umbenennen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="358"/>
        <source>Ctrl+R</source>
        <translation>Strg+R</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="359"/>
        <source>Rename original file(s)</source>
        <translation>Originaldatei(en) umbenennen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="364"/>
        <source>&amp;Copy to...</source>
        <translation>&amp;Kopieren nach...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="365"/>
        <source>Ctrl+C</source>
        <translation>Strg+C</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="366"/>
        <source>Copy original file(s) with new name to folder...</source>
        <translation>Originaldatei(en) mit neuem Namen in Ordner kopieren...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="373"/>
        <source>E&amp;xit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="374"/>
        <source>Ctrl+Q</source>
        <translation>Strg+Q</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="375"/>
        <source>Exit application</source>
        <translation>Anwendung beenden</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="385"/>
        <source>Create folders (yyyy_mm_dd)</source>
        <translation>Ordner erstellen (jjjj_mm_tt)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="387"/>
        <source>Create folders according to file creation date and copy the files into these folders</source>
        <translation>Ordner nach Dateierstellungsdatum erstellen und Dateien in diese Ordner kopieren</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>No name exept date/time</source>
        <translatorcomment>Typo in &apos;exept&apos;. It must be &apos;except&apos;.</translatorcomment>
        <translation>Kein Name außer Datum/Zeit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="394"/>
        <source>File name is only date/time</source>
        <translation>Dateiname ist nur Datum/Zeit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="402"/>
        <source>Preserve original file name before date/time</source>
        <translation>Originaldateinamen vor Datum/Zeit beibehalten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="408"/>
        <source>Preserve original file name after date/time</source>
        <translation>Originaldateinamen nach Datum/Zeit beibehalten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="415"/>
        <source>Insert new file name before date/time</source>
        <translation>Neuen Dateinamen vor Datum/Zeit einfügen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="421"/>
        <source>Insert new file name after date/time</source>
        <translation>Neuen Dateinamen nach Datum/Zeit einfügen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="426"/>
        <location filename="mainwindow.cpp" line="427"/>
        <source>Edit date and time...</source>
        <translation>Datum und Zeit bearbeiten...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="433"/>
        <source>Help...</source>
        <translation>Hilfe...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="434"/>
        <source>Help for application</source>
        <translation>Hilfe zur Anwendung</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="438"/>
        <source>About...</source>
        <translation>Über...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="439"/>
        <source>About application</source>
        <translation>Über die Anwendung</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="443"/>
        <source>About Qt...</source>
        <translation>Über Qt...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="444"/>
        <source>About Qt C++ Application Development Framework</source>
        <translation>Über Qt C++ Rahmenwerk zur Anwendungsentwicklung</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="448"/>
        <source>Check for updates...</source>
        <translation>Nach Aktualisierungen suchen...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="449"/>
        <source>Check for new versions of EXIF ReName</source>
        <translation>Nach neuen Versionen von EXIF ReName suchen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="455"/>
        <location filename="mainwindow.cpp" line="2331"/>
        <source>English</source>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="456"/>
        <source>English language</source>
        <translation>Englische Sprache</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="461"/>
        <source>Italian</source>
        <translation>Italienisch</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="462"/>
        <source>Italian language</source>
        <translation>Italienische Sprache</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="467"/>
        <location filename="mainwindow.cpp" line="2336"/>
        <source>German</source>
        <translation>Deutsche</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="468"/>
        <source>German language</source>
        <translation>idioma alemandeutsche Sprache</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="473"/>
        <location filename="mainwindow.cpp" line="2333"/>
        <source>Russian</source>
        <translation>Russisch</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="474"/>
        <source>Russian language</source>
        <translation>Russische Sprache</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="479"/>
        <location filename="mainwindow.cpp" line="2334"/>
        <source>Swedish</source>
        <translation>Schwedisch</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="480"/>
        <source>Swedish language</source>
        <translation>Schwedische Sprache</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="485"/>
        <location filename="mainwindow.cpp" line="2335"/>
        <source>Spanish</source>
        <translation>Spanisch</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="486"/>
        <source>Spanish language</source>
        <translation>Spanische Sprache</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="492"/>
        <source>Delete personal settings</source>
        <translation>Persönliche Einstellungen löschen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="493"/>
        <source>Remove your personal settings</source>
        <translation>Entfernen Sie Ihre persönlichen Einstellungen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="497"/>
        <location filename="mainwindow.cpp" line="498"/>
        <source>Specify file name pattern...</source>
        <translation>Dateinamensmuster angeben...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="506"/>
        <source>Icons only</source>
        <translation>Nur Symbole</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="507"/>
        <source>Display only icons in the toolbar</source>
        <translation>Nur Symbole in der Werkzeugleiste anzeigen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="510"/>
        <source>Text only</source>
        <translation>Nur Text</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="511"/>
        <source>Display only text in the toolbar</source>
        <translation>Nur Text in der Werkzeugleiste anzeigen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="514"/>
        <source>Text alongside icons</source>
        <translation>Text neben Symbolen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="515"/>
        <source>Display text alongside icons in the toolbar</source>
        <translation>Text neben Symbolen in der Werkzeugleiste anzeigen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="519"/>
        <source>Text under icons</source>
        <translation>Text unter Symbolen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="520"/>
        <source>Display text under icons in the toolbar</source>
        <translation>Text unter Symbolen in der Werkzeugleiste anzeigen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="523"/>
        <source>No toolbar</source>
        <translation>Keine Werkzeugleiste</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="524"/>
        <source>No toolbar will be displayed</source>
        <translation>Es wird keine Werkzeugleiste angezeigt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="537"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="545"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="551"/>
        <source>&amp;Preserve original file name</source>
        <translation>Originaldateiname &amp;beibehalten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="555"/>
        <source>&amp;Insert new file name</source>
        <translation>Neuen Dateinamen e&amp;ingeben</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="562"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="564"/>
        <location filename="mainwindow.cpp" line="655"/>
        <source>Toolbar</source>
        <translation>Werkzeugleiste</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="574"/>
        <source>&amp;Language</source>
        <translation>&amp;Sprache</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="585"/>
        <source>&amp;Tools</source>
        <translation>&amp;Werkzeuge</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="590"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="605"/>
        <source>Do you want to remove your personal settings?
This is an appropriate action if you like to uninstall this application.
</source>
        <translation>Möchten Sie Ihre persönlichen Einstellungen entfernen?
Dies ist eine geeignete Aktion, wenn Sie diese Anwendung deinstallieren möchten.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="605"/>
        <source> will exit after your personal data is deleted.</source>
        <translation> wird beendet, nachdem Ihre persönlichen Daten gelöscht wurden.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="606"/>
        <location filename="mainwindow.cpp" line="1662"/>
        <location filename="mainwindow.cpp" line="1703"/>
        <location filename="mainwindow.cpp" line="1742"/>
        <location filename="mainwindow.cpp" line="1784"/>
        <location filename="mainwindow.cpp" line="1826"/>
        <location filename="mainwindow.cpp" line="1867"/>
        <location filename="mainwindow.cpp" line="2104"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="607"/>
        <location filename="mainwindow.cpp" line="1663"/>
        <location filename="mainwindow.cpp" line="1704"/>
        <location filename="mainwindow.cpp" line="1743"/>
        <location filename="mainwindow.cpp" line="1785"/>
        <location filename="mainwindow.cpp" line="1827"/>
        <location filename="mainwindow.cpp" line="1868"/>
        <location filename="mainwindow.cpp" line="2105"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="615"/>
        <source>Your personal settings are deleted and
 </source>
        <translation>Ihre persönlichen Einstellungen werden gelöscht und
 </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="615"/>
        <source> will exit</source>
        <translation> wird beendet</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="621"/>
        <source>Your personal settings cannot be found.
New settings will be saved then you restart the program.</source>
        <translation>Ihre persönlichen Einstellungen können nicht gefunden werden.
Neue Einstellungen werden gespeichert und dann starten Sie das Programm neu.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="653"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="673"/>
        <source>Create folders (yyyy</source>
        <translation>Ordner erstellen (jjjj</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="673"/>
        <source>mm</source>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="673"/>
        <source>dd)</source>
        <translation>tt)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="692"/>
        <location filename="mainwindow.cpp" line="2410"/>
        <source>Open File</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="735"/>
        <location filename="mainwindow.cpp" line="2437"/>
        <source>Image files</source>
        <translation>Bilddateien</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="762"/>
        <location filename="mainwindow.cpp" line="2465"/>
        <source>You do not have enough permissions to read
</source>
        <translation>Sie haben nicht genügend Berechtigungen zum Lesen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="785"/>
        <location filename="mainwindow.cpp" line="786"/>
        <source> File(s) selected</source>
        <translation>Datei(en) ausgewählt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="795"/>
        <location filename="mainwindow.cpp" line="1953"/>
        <location filename="mainwindow.cpp" line="1998"/>
        <source>The original file will be renamed</source>
        <translation>Die Originaldatei wird umbenannt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="941"/>
        <source>Copy pictures to folder</source>
        <translation>Bilder in Ordner kopieren</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="963"/>
        <source>You have not enough permissions
to write to </source>
        <translation>Sie haben nicht genügend Berechtigungen
zum Schreiben in </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="969"/>
        <location filename="mainwindow.cpp" line="1094"/>
        <location filename="mainwindow.cpp" line="1965"/>
        <location filename="mainwindow.cpp" line="1967"/>
        <location filename="mainwindow.cpp" line="1988"/>
        <location filename="mainwindow.cpp" line="1990"/>
        <source>Copy to: </source>
        <translation>Kopieren nach: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1056"/>
        <source>rename</source>
        <translation>umbenennen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1058"/>
        <source>copy</source>
        <translation>kopieren</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1064"/>
        <source>You have not selected any file(s).
Please select file(s) to </source>
        <translation>Sie haben keine Datei(en) ausgewählt.
Bitte Datei(en) auswählen zum </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1090"/>
        <source>Error in destination path
Please select another output directory!</source>
        <translation>Fehler im Zielpfad
Bitte wählen Sie ein anderes Ausgabeverzeichnis aus!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1178"/>
        <location filename="mainwindow.cpp" line="1241"/>
        <location filename="mainwindow.cpp" line="1292"/>
        <location filename="mainwindow.cpp" line="1348"/>
        <location filename="mainwindow.cpp" line="1407"/>
        <location filename="mainwindow.cpp" line="1466"/>
        <source>Unable to create directory 
</source>
        <translation>Verzeichnis konnte nicht erstellt werden 
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1178"/>
        <location filename="mainwindow.cpp" line="1241"/>
        <location filename="mainwindow.cpp" line="1292"/>
        <location filename="mainwindow.cpp" line="1348"/>
        <location filename="mainwindow.cpp" line="1407"/>
        <location filename="mainwindow.cpp" line="1466"/>
        <source>
check your permisions</source>
        <translation>
überprüfen Sie Ihre Berechtigungen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1200"/>
        <location filename="mainwindow.cpp" line="1314"/>
        <location filename="mainwindow.cpp" line="1428"/>
        <source>Unable to delete:
</source>
        <translation>Nicht löschbar:
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1376"/>
        <source>Indicate a new file name, please!</source>
        <translation>Bitte geben Sie einen neuen Dateinamen an!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1491"/>
        <source>Done! </source>
        <translation>Fertig! </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1491"/>
        <source> of </source>
        <translation> von </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1491"/>
        <source>file(s) )</source>
        <translation>Datei(en) )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1525"/>
        <source>/exif_rename_log.txt</source>
        <translation>/exif_rename_log.txt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1528"/>
        <source>Save log as...</source>
        <translation>Protokoll speichern als...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1536"/>
        <source>Text files (*.txt)</source>
        <translation>Textdateien (*.txt)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1536"/>
        <source>Any files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1555"/>
        <source>Error writing to
</source>
        <translation>Fehler beim Schreiben in
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1555"/>
        <source>
check your permissions</source>
        <translation>
überprüfen Sie Ihre Berechtigungen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1560"/>
        <source>*** Log saved: </source>
        <translation>*** Protokoll gespeichert: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1566"/>
        <source>Log created by </source>
        <translation>Protokoll erstellt von </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1568"/>
        <source>Copyright Ingemar Ceicer 2007 - </source>
        <translation>Urheberrecht Ingemar Ceicer 2007 - </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1627"/>
        <location filename="mainwindow.cpp" line="2100"/>
        <source>Licensierat under GNU General Public License This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version </source>
        <translatorcomment>&apos;Licensierat&apos; source string is no English word.</translatorcomment>
        <translation>Lizensiert unter der GNU General Public License. Dieses Programm ist freie Software; Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation veröffentlicht, weiterverbreiten und/oder modifizieren; entweder Version </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1627"/>
        <location filename="mainwindow.cpp" line="2100"/>
        <source> of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</source>
        <translation> der Lizenz oder (nach Ihrer Wahl) eine spätere Version. Dieses Programm wird in der Hoffnung, dass es nützlich sein wird, aber OHNE JEGLICHE GEWÄHRLEISTUNG verteilt; ohne auch nur die implizierte Gewährleistung der MARKTGÄNGIGKEIT oder BRAUCHBARKEIT FÜR EINEN BESTIMMTEN ZWECK. Weitere Informationen finden Sie in der GNU General Public License. Sie sollten eine Kopie der GNU General Public License zusammen mit diesem Programm erhalten haben; Wenn nicht, schreiben Sie an die Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1630"/>
        <location filename="mainwindow.cpp" line="2099"/>
        <source>&lt;br&gt;&lt;font coor=&quot;green&quot; size=&quot;3&quot;&gt;&lt;b&gt;Copyright &amp;copy; 2007 - </source>
        <translation>&lt;br&gt;&lt;font coor=&quot;green&quot; size=&quot;3&quot;&gt;&lt;b&gt;Urheberrecht &amp;copy; 2007 - </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1630"/>
        <location filename="mainwindow.cpp" line="2099"/>
        <source>homepage</source>
        <translation>Homepage</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1632"/>
        <source>About </source>
        <translation>Über </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1632"/>
        <location filename="mainwindow.cpp" line="2103"/>
        <source>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</source>
        <translation>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>A small application for renaming pictures. EXIF ReName uses exif data from the picture file.&lt;br&gt;&lt;br&gt;</source>
        <translation>Eine kleine Anwendung zum Umbenennen von Bildern. EXIF ReName verwendet exif-Daten aus der Bilddatei.&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>Thank you Ivan Dolgov for the translation into Russian.</source>
        <translation>Vielen Dank Ivan Dolgov für die Übersetzung ins Russische.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>Thank you Roberto Nerozzi for the translation into Italien.</source>
        <translation>Vielen Dank Roberto Nerozzi für die Übersetzung ins Italienische.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>Thank you Cristian Sosa Noe for the translation into Spanish.</source>
        <translation>Vielen Dank Cristian Sosa Noe für die Übersetzung ins Spanische.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>Thank you Ben Weis for the translation into German.</source>
        <translation>Vielen Dank Ben Weis für die Übersetzung ins Deutsch.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1661"/>
        <location filename="mainwindow.cpp" line="1702"/>
        <location filename="mainwindow.cpp" line="1741"/>
        <location filename="mainwindow.cpp" line="1783"/>
        <location filename="mainwindow.cpp" line="1825"/>
        <location filename="mainwindow.cpp" line="1866"/>
        <source>The application needs to restart in order
to initialise the now language settings.

Do you want to close
the application now?</source>
        <translatorcomment>Typo! It must be &apos;new language settings&apos;.</translatorcomment>
        <translation>Die Anwendung muss neu gestartet werden,
um die neuen Spracheinstellungen zu initialisieren.

Möchten Sie die Anwendung
jetzt schließen?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1663"/>
        <location filename="mainwindow.cpp" line="1704"/>
        <location filename="mainwindow.cpp" line="1743"/>
        <location filename="mainwindow.cpp" line="1785"/>
        <location filename="mainwindow.cpp" line="1827"/>
        <location filename="mainwindow.cpp" line="1868"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1934"/>
        <source>Ready for action!</source>
        <translation>Einsatzbereit!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2103"/>
        <source>&lt;br&gt;A small application for renaming pictures. EXIF ReName uses exif data from the picture file.&lt;br&gt;&lt;br&gt;</source>
        <translation>&lt;br&gt;Eine kleine Anwendung zum Umbenennen von Bildern. EXIF ReName verwendet exif-Daten aus der Bilddatei.&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2103"/>
        <source>&lt;br&gt;&lt;br&gt;&lt;font color=&quot;#FF0000&quot; size=&quot;4&quot;&gt;Do you accept the license?&lt;/font&gt;</source>
        <translation>&lt;br&gt;&lt;br&gt;&lt;font color=&quot;#FF0000&quot; size=&quot;4&quot;&gt;Akzeptieren Sie die Lizenz?&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2141"/>
        <location filename="mainwindow.cpp" line="2149"/>
        <source> is updatet to </source>
        <translatorcomment>Typo!
&apos;updated&apos; is correct.</translatorcomment>
        <translation> ist aktualisiert zu </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2185"/>
        <source>Copied to new folder in: &lt;b&gt;</source>
        <translation>In neuen Ordner kopiert nach: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2185"/>
        <location filename="mainwindow.cpp" line="2189"/>
        <source>&lt;/b&gt; and renamed: </source>
        <translation>&lt;/b&gt; und umbenannt: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2189"/>
        <source>Copied to: &lt;b&gt;</source>
        <translation>Kopiert nach: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2200"/>
        <source>&lt;br /&gt;Selected file(s) in: &lt;b&gt;</source>
        <translation>&lt;br /&gt;Ausgewählte Datei(en) in: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2332"/>
        <source>Italiano</source>
        <translation>Italienisch</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2337"/>
        <source>Choose the language that will be used by EXIF Rename</source>
        <translation>Wählen Sie die Sprache aus, die von EXIF Rename verwendet wird</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2351"/>
        <source>You must restart the application to the Russian language settings to take effect.</source>
        <translation>Sie müssen die Anwendung neu starten, damit die russischen Spracheinstellungen wirksam werden.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2359"/>
        <source>You must restart the application to the Italien language settings to take effect.</source>
        <translation>Sie müssen die Anwendung neu starten, damit die italienischen Spracheinstellungen wirksam werden.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2367"/>
        <source>You must restart the application to the Swedish language settings to take effect.</source>
        <translation>Sie müssen die Anwendung neu starten, damit die schwedischen Spracheinstellungen wirksam werden.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2374"/>
        <location filename="mainwindow.cpp" line="2381"/>
        <source>You must restart the application to the Spanish language settings to take effect.</source>
        <translation>Sie müssen die Anwendung neu starten, damit die spanischen Spracheinstellungen wirksam werden.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2441"/>
        <source>All files</source>
        <translation>Alle Dateien</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2477"/>
        <source>Changes the EXIF information for these files</source>
        <translation>Ändert die EXIF-Informationen für diese Dateien</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2486"/>
        <source> ...selected</source>
        <translation> ...ausgewählt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2531"/>
        <source>MS-DOS-based version of Windows.</source>
        <translation>MS-DOS-basierte Version von Windows.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2534"/>
        <source>NT-based version of Windows.</source>
        <translation>NT-basierte Version von Windows.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2537"/>
        <source>CE-based version of Windows.</source>
        <translation>CE-basierte Version von Windows.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2540"/>
        <source>Windows 3.1 with Win 32s operating system.</source>
        <translation>Windows 3.1 mit Win-32-Betriebssystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2543"/>
        <source>Windows 95 operating system.</source>
        <translation>Windows 95 Betriebssystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2546"/>
        <source>Windows 98 operating system.</source>
        <translation>Windows 98 Betriebssystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2549"/>
        <source>Windows Me operating system.</source>
        <translation>Windows Me Betriebssystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2552"/>
        <source>Windows operating system version 4.0, corresponds to Windows NT.</source>
        <translation>Windows-Betriebssystemversion 4.0, entspricht Windows NT.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2555"/>
        <source>Windows operating system version 5.0, corresponds to Windows 2000.</source>
        <translation>Windows-Betriebssystemversion 5.0, entspricht Windows 2000.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2558"/>
        <source>Windows operating system version 5.1, corresponds to Windows XP.</source>
        <translation>Windows-Betriebssystemversion 5.1, entspricht Windows XP.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2561"/>
        <source>Windows operating system version 5.2, corresponds to Windows Server 2003, Windows Server 2003 R2, Windows Home Server, and Windows XP Professional x64 Edition.</source>
        <translation>Windows-Betriebssystemversion 5.2, entspricht Windows Server 2003, Windows Server 2003 R2, Windows Home Server und Windows XP Professional x64 Edition.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2564"/>
        <source>Windows operating system version 6.0, corresponds to Windows Vista and Windows Server 2008.</source>
        <translation>Windows-Betriebssystemversion 6.0, entspricht Windows Vista und Windows Server 2008.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2567"/>
        <source>Windows operating system version 6.1, corresponds to Windows 7 and Windows Server 2008 R2.</source>
        <translation>Windows-Betriebssystemversion 6.1, entspricht Windows 7 und Windows Server 2008 R2.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2570"/>
        <source>Windows operating system version 6.2, corresponds to Windows 8</source>
        <translation>Windows-Betriebssystemversion 6.2, entspricht Windows 8</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2573"/>
        <source>Operating system version 6.3, corresponds to Windows 8.1</source>
        <translation>Windows-Betriebssystemversion 6.3, entspricht Windows 8.1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2576"/>
        <source>Operating system version 10.0, corresponds to Windows 10</source>
        <translation>Windows-Betriebssystemversion 10.0, entspricht Windows 10</translation>
    </message>
    <message>
        <source>Windows operating system version 6.2, corresponds to Windows 8.</source>
        <translation type="obsolete">Windows-Betriebssystemversion 6.2, entspricht Windows 8.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2579"/>
        <source>Windows CE operating system.</source>
        <translation>Windows CE Betriebssystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2582"/>
        <source>Windows CE .NET operating system.</source>
        <translation>Windows CE .NET Betriebssystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2585"/>
        <source>Windows CE 5.x operating system.</source>
        <translation>Windows CE 5.x Betriebssystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2588"/>
        <source>Windows CE 6.x operating system.</source>
        <translation>Windows CE 6.x Betriebssystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2591"/>
        <source>Unknown Windows operating system</source>
        <translation>Unbekanntes Windows-Betriebssystem</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2600"/>
        <source>Architecture instruction set: </source>
        <translation>Architektur-Befehlssatz: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2611"/>
        <source> was created </source>
        <translation> wurde erstellt </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2621"/>
        <location filename="mainwindow.cpp" line="2623"/>
        <location filename="mainwindow.cpp" line="2658"/>
        <source>Compiled by</source>
        <translation>Kompiliert von</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2654"/>
        <source>Unknown version</source>
        <translation>Unbekannte Version</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2663"/>
        <source>Unknown compiler.</source>
        <translation>Unbekannter Kompiler.</translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <location filename="checkupdate.cpp" line="46"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="50"/>
        <source>Check for updates on startup</source>
        <translation>Beim Start nach Aktualisierungen suchen</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="62"/>
        <source>Contacting server...</source>
        <translation>Server wird kontaktiert...</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="134"/>
        <source>Error message: Host found but
the requested version information was not found.
Please try again later</source>
        <translation>Fehlermeldung: Host gefunden, aber
die angeforderte Versionsinformation wurde nicht gefunden.
Bitte versuchen Sie es später erneut</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="135"/>
        <location filename="checkupdate.cpp" line="149"/>
        <location filename="checkupdate.cpp" line="190"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="148"/>
        <source>Error message: </source>
        <translation>Fehlermeldung: </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="148"/>
        <source>
Please try again later</source>
        <translation>
Bitte versuchen Sie es später erneut</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source>New version available: </source>
        <translation>Neue Version verfügbar: </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source>Download it from</source>
        <translation>Herunterladen von</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source> homepage</source>
        <translation> Homepage</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="181"/>
        <source>Your current version is the latest available: </source>
        <translation>Ihre aktuelle Version ist die neueste verfügbare: </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="181"/>
        <source>Thank you for using</source>
        <translation>Danke für das Benutzen</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>New version available!</source>
        <translation>Neue Version verfügbar!</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>Version </source>
        <translation>Version </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>Please visit</source>
        <translation>Bitte besuchen</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>homepage</source>
        <translation>Homepage</translation>
    </message>
</context>
</TS>
