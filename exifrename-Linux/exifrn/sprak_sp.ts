<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_AR" sourcelanguage="en">
<context>
    <name>Exif_w</name>
    <message>
        <location filename="exif_w.ui" line="35"/>
        <source>Dialog</source>
        <translation>Dialogo</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="72"/>
        <source>Date and time</source>
        <translation>Fecha y hora</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="85"/>
        <source>Change the time stamp on</source>
        <translation>Cambiar la etiqueta de tiempo en</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="98"/>
        <source>TextLabel</source>
        <translation>Etiqueta de texto</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="111"/>
        <source>Year-Month-Day Hour-Minute-Second</source>
        <translation>Año-Mes-Dia-Hora-Minuto-Segundo</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="124"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="137"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="150"/>
        <source>Current Time</source>
        <translation>Hora actual</translation>
    </message>
    <message>
        <location filename="exif_w.cpp" line="72"/>
        <source>Incorrect date/time format, unable to write to file.
You must write in this way: </source>
        <translation>Formato incorrecto de fecha/hora, no se puede grabar el archivo.
Debe escribirlos de esta forma:</translation>
    </message>
    <message>
        <location filename="exif_w.cpp" line="101"/>
        <source>Unable to save EXIF data</source>
        <translation>No se pueden grabar los datos EXIF</translation>
    </message>
</context>
<context>
    <name>FilePattern</name>
    <message>
        <location filename="filepattern.ui" line="59"/>
        <source>file name</source>
        <translation>Nombre del archivo</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="72"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The numbers in the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The numbers in the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="80"/>
        <location filename="filepattern.ui" line="117"/>
        <location filename="filepattern.ui" line="266"/>
        <source>none</source>
        <translation>ninguno</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="85"/>
        <location filename="filepattern.ui" line="122"/>
        <location filename="filepattern.ui" line="271"/>
        <source>space</source>
        <translation>espacio</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="90"/>
        <location filename="filepattern.ui" line="127"/>
        <location filename="filepattern.ui" line="276"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="95"/>
        <location filename="filepattern.ui" line="132"/>
        <location filename="filepattern.ui" line="281"/>
        <source>_</source>
        <translation>_</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="109"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Date and time component part of the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Date and time component part of the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="149"/>
        <source>File name figures, separated by</source>
        <translation>Caracteres de nombre de archivo separados por</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="168"/>
        <source>Date and time, separated by</source>
        <translation>Fecha y hora separados por</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="187"/>
        <source>File extension (.jpg or .JPG)</source>
        <translation>Extención de archivo (.jpg  o .JPG)</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="200"/>
        <source>CAPITAL letter</source>
        <translation>MAYUSCULAS</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="216"/>
        <source>lowercase letter</source>
        <translation>minúsculas</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="232"/>
        <source>Save</source>
        <translation>Grabar</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="245"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="258"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The figures in the folder name are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The figures in the folder name are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="295"/>
        <source>Folder name figures, separated by</source>
        <translation>Palabras del Nombre de Carpeta, separadas por</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="314"/>
        <source>File name:</source>
        <translation>Nombre del archivo:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="333"/>
        <source>Folder name:</source>
        <translation>Nombre de la carpeta:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="352"/>
        <source>folder name</source>
        <translation>Nombre de la carpeta</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="373"/>
        <source>Only time in file name</source>
        <translation>Solo hora en nombre de archivo</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="834"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Extensions of files you want to be able to open, separated by cemikolon. S&lt;span style=&quot; color:#000000;&quot;&gt;uch as jpg and tif.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000&quot;&gt;Example:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;jpg;tif;cr2&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Extensions of files you want to be able to open, separated by cemikolon. S&lt;span style=&quot; color:#000000;&quot;&gt;uch as jpg and tif.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000&quot;&gt;Example:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;jpg;tif;cr2&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="867"/>
        <source>File extension:</source>
        <translation>Extensión de archivo:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="886"/>
        <source>*.</source>
        <translation>*.</translation>
    </message>
    <message>
        <location filename="filepattern.cpp" line="97"/>
        <location filename="filepattern.cpp" line="321"/>
        <source>Extension: .</source>
        <translation>Extensión: .</translation>
    </message>
    <message>
        <location filename="filepattern.cpp" line="97"/>
        <location filename="filepattern.cpp" line="321"/>
        <source> or .</source>
        <translation>o .</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="help.cpp" line="62"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="help.cpp" line="155"/>
        <source>Error reading
</source>
        <translation>Error al leer</translation>
    </message>
    <message>
        <location filename="help.cpp" line="155"/>
        <source>
info file missing!</source>
        <translation>Información del archivo faltante!</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="127"/>
        <source>Copy (do not rename)</source>
        <translation>Copiar (no renombrar)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="129"/>
        <source>Copy to...</source>
        <translation>Copiar a...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="155"/>
        <source>Preserve original file name</source>
        <translation>Preservar el nombre de archivo original</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="157"/>
        <location filename="mainwindow.cpp" line="190"/>
        <location filename="mainwindow.cpp" line="400"/>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Before date/time</source>
        <translation>Antes de fecha/hora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="158"/>
        <location filename="mainwindow.cpp" line="191"/>
        <location filename="mainwindow.cpp" line="406"/>
        <location filename="mainwindow.cpp" line="419"/>
        <source>After date/time</source>
        <translation>Después de fecha/hora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="188"/>
        <source>Insert new file name</source>
        <translation>Insertar nuevo nombre de archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="212"/>
        <source>Go!</source>
        <translation>Comezar!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="231"/>
        <source>Clear log</source>
        <translation>Limpiar log</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="233"/>
        <source>Save log</source>
        <translation>Guardar log</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="351"/>
        <source>&amp;Open...</source>
        <translation>&amp;Abrir...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="352"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="353"/>
        <source>Open jpeg image(s)</source>
        <translation>Abrir imagen(es) jpeg</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="357"/>
        <source>&amp;Rename</source>
        <translation>&amp;Renombrar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="358"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="359"/>
        <source>Rename original file(s)</source>
        <translation>Renombrar archivo(s) original(es)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="364"/>
        <source>&amp;Copy to...</source>
        <translation>&amp;Copiar a...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="365"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="366"/>
        <source>Copy original file(s) with new name to folder...</source>
        <translation>Copiar archivos originales renombrados a la carpeta...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="373"/>
        <source>E&amp;xit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="374"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="375"/>
        <source>Exit application</source>
        <translation>Salir de la aplicación</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="385"/>
        <source>Create folders (yyyy_mm_dd)</source>
        <translation>Crear carpetas (aaaa_mm_dd)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="387"/>
        <source>Create folders according to file creation date and copy the files into these folders</source>
        <translation>Crear carpetas de acuerdo a la fecha de creación de los archivos y copiarlos dentro de ellas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>No name exept date/time</source>
        <translation>Sin nombre, excepto fecha/hora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="394"/>
        <source>File name is only date/time</source>
        <translation>Nombre de archivo es sólo fecha/hora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="402"/>
        <source>Preserve original file name before date/time</source>
        <translation>Mantener el nombre de archivo original antes de fecha/hora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="408"/>
        <source>Preserve original file name after date/time</source>
        <translatorcomment>Cristian Sosa Noe</translatorcomment>
        <translation>Mantener nombre original despues de fecha/hora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="415"/>
        <source>Insert new file name before date/time</source>
        <translation>Insertar nuevo nombre de archivo antes de fecha/hora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="421"/>
        <source>Insert new file name after date/time</source>
        <translation>Insertar nuevo nombre de archivo luego de fecha/hora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="426"/>
        <location filename="mainwindow.cpp" line="427"/>
        <source>Edit date and time...</source>
        <translation>Editar fecha y hora...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="433"/>
        <source>Help...</source>
        <translation>Ayuda...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="434"/>
        <source>Help for application</source>
        <translation>Ayuda de la aplicación</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="438"/>
        <source>About...</source>
        <translation>Acerca de...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="439"/>
        <source>About application</source>
        <translation>Acerca de la aplicación</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="443"/>
        <source>About Qt...</source>
        <translation>Acerca de Qt...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="444"/>
        <source>About Qt C++ Application Development Framework</source>
        <translation>Acerca de Qt C++ Application Development Framework</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="448"/>
        <source>Check for updates...</source>
        <translation>Verificar actuallizaciones...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="449"/>
        <source>Check for new versions of EXIF ReName</source>
        <translation>Verificar nuevas versiones de EXIF ReName</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="455"/>
        <location filename="mainwindow.cpp" line="2331"/>
        <source>English</source>
        <translation>Inglés</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="456"/>
        <source>English language</source>
        <translation>Idioma Inglés</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2332"/>
        <source>Italiano</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="462"/>
        <source>Italian language</source>
        <translation>Idioma Italiano</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="461"/>
        <source>Italian</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="467"/>
        <location filename="mainwindow.cpp" line="2336"/>
        <source>German</source>
        <translation>Alemán</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="468"/>
        <source>German language</source>
        <translation>Lengua alemana</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="473"/>
        <location filename="mainwindow.cpp" line="2333"/>
        <source>Russian</source>
        <translation>Ruso</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="474"/>
        <source>Russian language</source>
        <translation>Idioma Ruso</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="479"/>
        <location filename="mainwindow.cpp" line="2334"/>
        <source>Swedish</source>
        <oldsource>Svenska</oldsource>
        <translation>Sueco</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="480"/>
        <source>Swedish language</source>
        <translation>Idioma Sueco</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="486"/>
        <source>Spanish language</source>
        <translation>lengua Española</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="492"/>
        <source>Delete personal settings</source>
        <translation>Borrar preferencias personales</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="493"/>
        <source>Remove your personal settings</source>
        <translation>Eliminar preferencias personales</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="497"/>
        <location filename="mainwindow.cpp" line="498"/>
        <source>Specify file name pattern...</source>
        <translation>Especificar formato de nombre de archivo...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="506"/>
        <source>Icons only</source>
        <translation>Solo iconos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="507"/>
        <source>Display only icons in the toolbar</source>
        <translation>Mostrar solo iconos en barra de herramientas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="510"/>
        <source>Text only</source>
        <translation>Solo texto</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="511"/>
        <source>Display only text in the toolbar</source>
        <translation>Mostrar solo texto en barra de herramientas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="514"/>
        <source>Text alongside icons</source>
        <translation>Texto junto a iconos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="515"/>
        <source>Display text alongside icons in the toolbar</source>
        <translation>Mostrar texto junto a iconos en la barra de herramientas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="519"/>
        <source>Text under icons</source>
        <translation>Texto debajo de los iconos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="520"/>
        <source>Display text under icons in the toolbar</source>
        <translation>Mostrar texto debajo de los iconos de la barra de tareas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="523"/>
        <source>No toolbar</source>
        <translation>Sin barra de tareas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="524"/>
        <source>No toolbar will be displayed</source>
        <translation>No se mostrará barra de tareas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="537"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="545"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="551"/>
        <source>&amp;Preserve original file name</source>
        <translation>&amp;Preservar nombre de archivo original</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="555"/>
        <source>&amp;Insert new file name</source>
        <translation>&amp;Insertar nuevo nombre de archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="562"/>
        <source>&amp;View</source>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="564"/>
        <location filename="mainwindow.cpp" line="655"/>
        <source>Toolbar</source>
        <translation>Barra de herramientas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="574"/>
        <source>&amp;Language</source>
        <translation>&amp;Idioma</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="585"/>
        <source>&amp;Tools</source>
        <translation>&amp;Herramientas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="590"/>
        <source>&amp;Help</source>
        <translation>&amp;Ayuda</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="605"/>
        <source>Do you want to remove your personal settings?
This is an appropriate action if you like to uninstall this application.
</source>
        <translation>Desea borrar las preferencias?
 Esto es apropiado si desea desinstalar la aplicación.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="605"/>
        <source> will exit after your personal data is deleted.</source>
        <translation>Saldrá luego de borrar sus datos personales.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="606"/>
        <location filename="mainwindow.cpp" line="1662"/>
        <location filename="mainwindow.cpp" line="1703"/>
        <location filename="mainwindow.cpp" line="1742"/>
        <location filename="mainwindow.cpp" line="1784"/>
        <location filename="mainwindow.cpp" line="1826"/>
        <location filename="mainwindow.cpp" line="1867"/>
        <location filename="mainwindow.cpp" line="2104"/>
        <source>Yes</source>
        <translation>Si</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="607"/>
        <location filename="mainwindow.cpp" line="1663"/>
        <location filename="mainwindow.cpp" line="1704"/>
        <location filename="mainwindow.cpp" line="1743"/>
        <location filename="mainwindow.cpp" line="1785"/>
        <location filename="mainwindow.cpp" line="1827"/>
        <location filename="mainwindow.cpp" line="1868"/>
        <location filename="mainwindow.cpp" line="2105"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="615"/>
        <source>Your personal settings are deleted and
 </source>
        <translation>Se borrarán sus preferencias personales y</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="615"/>
        <source> will exit</source>
        <translation>saldrá</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="621"/>
        <source>Your personal settings cannot be found.
New settings will be saved then you restart the program.</source>
        <translation>No se han encontrado preferencias personales.
Se guardarán nuevas preferencias luego de reiniciar el programa.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="653"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="673"/>
        <source>Create folders (yyyy</source>
        <translation>Crear carpetas (aaaa</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="673"/>
        <source>mm</source>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="673"/>
        <source>dd)</source>
        <translation>dd)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="692"/>
        <location filename="mainwindow.cpp" line="2410"/>
        <source>Open File</source>
        <translation>Abrir Archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="735"/>
        <location filename="mainwindow.cpp" line="2437"/>
        <source>Image files</source>
        <translation>Archivos de imagen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="762"/>
        <location filename="mainwindow.cpp" line="2465"/>
        <source>You do not have enough permissions to read
</source>
        <translation>No tiene suficientes permisos de lectura</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="785"/>
        <location filename="mainwindow.cpp" line="786"/>
        <source> File(s) selected</source>
        <translation>Archivo(s) seleccionados</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="795"/>
        <location filename="mainwindow.cpp" line="1953"/>
        <location filename="mainwindow.cpp" line="1998"/>
        <source>The original file will be renamed</source>
        <translation>El archivo original será renombrado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="941"/>
        <source>Copy pictures to folder</source>
        <translation>Copiar imagenes a carpeta</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="963"/>
        <source>You have not enough permissions
to write to </source>
        <translation>No tiene suficientes permisos para escribir en</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="969"/>
        <location filename="mainwindow.cpp" line="1094"/>
        <location filename="mainwindow.cpp" line="1965"/>
        <location filename="mainwindow.cpp" line="1967"/>
        <location filename="mainwindow.cpp" line="1988"/>
        <location filename="mainwindow.cpp" line="1990"/>
        <source>Copy to: </source>
        <translation>Copiar a:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1056"/>
        <source>rename</source>
        <translation>Renombrar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1058"/>
        <source>copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1064"/>
        <source>You have not selected any file(s).
Please select file(s) to </source>
        <translation>No ha seleccionado ningún archivo.
Por favor seleccione archivo(s)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1090"/>
        <source>Error in destination path
Please select another output directory!</source>
        <translation>Error en la ruta de destino
Por favor, selecciones un directorio de salida!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1178"/>
        <location filename="mainwindow.cpp" line="1241"/>
        <location filename="mainwindow.cpp" line="1292"/>
        <location filename="mainwindow.cpp" line="1348"/>
        <location filename="mainwindow.cpp" line="1407"/>
        <location filename="mainwindow.cpp" line="1466"/>
        <source>Unable to create directory 
</source>
        <translation>No se puede crear el directorio</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1178"/>
        <location filename="mainwindow.cpp" line="1241"/>
        <location filename="mainwindow.cpp" line="1292"/>
        <location filename="mainwindow.cpp" line="1348"/>
        <location filename="mainwindow.cpp" line="1407"/>
        <location filename="mainwindow.cpp" line="1466"/>
        <source>
check your permisions</source>
        <translation>Verifique sus permisos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1200"/>
        <location filename="mainwindow.cpp" line="1314"/>
        <location filename="mainwindow.cpp" line="1428"/>
        <source>Unable to delete:
</source>
        <translation>No se puede eliminar:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1376"/>
        <source>Indicate a new file name, please!</source>
        <translation>Indique nuevo nombre de archivo, por favor!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1491"/>
        <source>Done! </source>
        <translation>Hecho!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1491"/>
        <source> of </source>
        <translation>de</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1491"/>
        <source>file(s) )</source>
        <translation>archivo(s) )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1525"/>
        <source>/exif_rename_log.txt</source>
        <translation>/exif_rename_log.txt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1528"/>
        <source>Save log as...</source>
        <translation>Guardar logs como...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1536"/>
        <source>Text files (*.txt)</source>
        <translation>Archivo de texto (*.txt)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1536"/>
        <source>Any files (*)</source>
        <translation>Cualquier archivo (*)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1555"/>
        <source>Error writing to
</source>
        <translation>Error al escribir en</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1555"/>
        <source>
check your permissions</source>
        <translation>Verifique sus permisos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1560"/>
        <source>*** Log saved: </source>
        <translation>***Log guardado:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1566"/>
        <source>Log created by </source>
        <translation>Log creado por</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1568"/>
        <source>Copyright Ingemar Ceicer 2007 - </source>
        <translation>Copyright Ingemar Ceicer 2007 - </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1627"/>
        <location filename="mainwindow.cpp" line="2100"/>
        <source>Licensierat under GNU General Public License This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version </source>
        <translation>Licencia GNU General Public License. Este programe es libre; puede redistribuirse y/o modificarse bajo los términos de GNU General Public License como fue publicado por Free Software Foundation; en cualquier vesión </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1627"/>
        <location filename="mainwindow.cpp" line="2100"/>
        <source> of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</source>
        <translation> de la licencia, o (a su opción) cualquier versión posterior. Este programa se distribuye esperando que sea util, pero SIN NINGUNA GARANTIA; sin siquiera la garantia implícita de COMERCIALIZACION o APLICACIÓN PARA UN USO EN PARTICULAR. Vea la GNU General Public License para mas detalles. Usted debe haber recibido una copia de la GNU General Public License con este programa; si no es así, escriba a Free Software Foundation Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1630"/>
        <location filename="mainwindow.cpp" line="2099"/>
        <source>&lt;br&gt;&lt;font coor=&quot;green&quot; size=&quot;3&quot;&gt;&lt;b&gt;Copyright &amp;copy; 2007 - </source>
        <translation>&lt;br&gt;&lt;font coor=&quot;green&quot; size=&quot;3&quot;&gt;&lt;b&gt;Copyright &amp;copy; 2007 - </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1630"/>
        <location filename="mainwindow.cpp" line="2099"/>
        <source>homepage</source>
        <translation>Pagina principal</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1632"/>
        <source>About </source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1632"/>
        <location filename="mainwindow.cpp" line="2103"/>
        <source>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</source>
        <translation>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>A small application for renaming pictures. EXIF ReName uses exif data from the picture file.&lt;br&gt;&lt;br&gt;</source>
        <translation>Una pequeña aplicación para renombrar imagenes. EXIF ReName usa los datos exif del archivo de imagen.&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>Thank you Ivan Dolgov for the translation into Russian.</source>
        <translation>Gracias a Ivan Dologov por la traducción a Ruso.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>Thank you Roberto Nerozzi for the translation into Italien.</source>
        <translation>Gracias a Roberto Nerozzi por la traducción al Italiano.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>Thank you Cristian Sosa Noe for the translation into Spanish.</source>
        <translation>Gracias Cristian Sosa Noe para la traducción al Español.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>Thank you Ben Weis for the translation into German.</source>
        <translation>Gracias Ben Weis para la traducción al Alemán.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1661"/>
        <location filename="mainwindow.cpp" line="1702"/>
        <location filename="mainwindow.cpp" line="1741"/>
        <location filename="mainwindow.cpp" line="1783"/>
        <location filename="mainwindow.cpp" line="1825"/>
        <location filename="mainwindow.cpp" line="1866"/>
        <source>The application needs to restart in order
to initialise the now language settings.

Do you want to close
the application now?</source>
        <translation>La aplicación necesita reiniciarse para aplicar el nuevo idioma.

Desea cerrar la aplicación ahora?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1663"/>
        <location filename="mainwindow.cpp" line="1704"/>
        <location filename="mainwindow.cpp" line="1743"/>
        <location filename="mainwindow.cpp" line="1785"/>
        <location filename="mainwindow.cpp" line="1827"/>
        <location filename="mainwindow.cpp" line="1868"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1934"/>
        <source>Ready for action!</source>
        <translation>Listo para la acción!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2103"/>
        <source>&lt;br&gt;A small application for renaming pictures. EXIF ReName uses exif data from the picture file.&lt;br&gt;&lt;br&gt;</source>
        <translation>&lt;br&gt;Una pequeña aplicación para renombrar imagenes. EXIF ReName usa los datos exif del archivo de imagen.&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2103"/>
        <source>&lt;br&gt;&lt;br&gt;&lt;font color=&quot;#FF0000&quot; size=&quot;4&quot;&gt;Do you accept the license?&lt;/font&gt;</source>
        <translation>&lt;br&gt;&lt;br&gt;&lt;font color=&quot;#FF0000&quot; size=&quot;4&quot;&gt;Acepta la licencia?&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2141"/>
        <location filename="mainwindow.cpp" line="2149"/>
        <source> is updatet to </source>
        <translation>se actualizó a</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2185"/>
        <source>Copied to new folder in: &lt;b&gt;</source>
        <translation>Copiado a nueva carpeta en:&lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2185"/>
        <location filename="mainwindow.cpp" line="2189"/>
        <source>&lt;/b&gt; and renamed: </source>
        <translation>&lt;/b&gt; y renombrado: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2189"/>
        <source>Copied to: &lt;b&gt;</source>
        <translation>Copiado a: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2200"/>
        <source>&lt;br /&gt;Selected file(s) in: &lt;b&gt;</source>
        <translation>&lt;br /&gt;Archivo(s) seleccionado(s) en: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="485"/>
        <location filename="mainwindow.cpp" line="2335"/>
        <source>Spanish</source>
        <translation>Español</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2337"/>
        <source>Choose the language that will be used by EXIF Rename</source>
        <translation>Eija el idioma que usará EXIF ReName</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2351"/>
        <source>You must restart the application to the Russian language settings to take effect.</source>
        <translation>Debe reiniciar el programa para aplicar el idioma Ruso.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2359"/>
        <source>You must restart the application to the Italien language settings to take effect.</source>
        <translation>Debe reiniciar el programa para aplicar el idioma Italiano.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2367"/>
        <source>You must restart the application to the Swedish language settings to take effect.</source>
        <translation>Debe reiniciar el programa para aplicar el idioma Sueco.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2374"/>
        <location filename="mainwindow.cpp" line="2381"/>
        <source>You must restart the application to the Spanish language settings to take effect.</source>
        <translation>Debe reiniciar el programa para aplicar el idioma Español.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2441"/>
        <source>All files</source>
        <translation>Todos los archivos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2477"/>
        <source>Changes the EXIF information for these files</source>
        <translation>Cambia la información EXIF de estos archivos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2486"/>
        <source> ...selected</source>
        <translation>...seleccionado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2531"/>
        <source>MS-DOS-based version of Windows.</source>
        <translation>Versión de Windows basada en MS-DOS.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2534"/>
        <source>NT-based version of Windows.</source>
        <translation>Versión de Windows basada en NT.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2537"/>
        <source>CE-based version of Windows.</source>
        <translation>Versión de Windows basada en CE.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2540"/>
        <source>Windows 3.1 with Win 32s operating system.</source>
        <translation>Sistema Operativo Windows 3.1.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2543"/>
        <source>Windows 95 operating system.</source>
        <translation>Sistema Operativo Windows 95.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2546"/>
        <source>Windows 98 operating system.</source>
        <translation>Sistema Operativo Windows 98.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2549"/>
        <source>Windows Me operating system.</source>
        <translation>Sistema Operativo Windows ME.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2552"/>
        <source>Windows operating system version 4.0, corresponds to Windows NT.</source>
        <translation>Sistema operativo Windows version 4.0, corresponde a Windows NT.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2555"/>
        <source>Windows operating system version 5.0, corresponds to Windows 2000.</source>
        <translation>Sistema operativo Windows version 5.0, corresponde a Windows 2000.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2558"/>
        <source>Windows operating system version 5.1, corresponds to Windows XP.</source>
        <translation>Sistema operativo Windows version 5.1, corresponde a Windows  XP.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2561"/>
        <source>Windows operating system version 5.2, corresponds to Windows Server 2003, Windows Server 2003 R2, Windows Home Server, and Windows XP Professional x64 Edition.</source>
        <translation>Sistema operativo Windows version 5.2, corresponde a Windows Server 2003, Windows Server 2003 R2, Windows Home Server y Windows XP Professional edición X64.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2564"/>
        <source>Windows operating system version 6.0, corresponds to Windows Vista and Windows Server 2008.</source>
        <translation>Sistema operativo Windows version 6.0, corresponde a Windows Vista y Windows Server 2008.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2567"/>
        <source>Windows operating system version 6.1, corresponds to Windows 7 and Windows Server 2008 R2.</source>
        <translation>Sistema operativo Windows version 6.1, corresponde a Windows 7 y Windows Server 2008 R2.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2570"/>
        <source>Windows operating system version 6.2, corresponds to Windows 8</source>
        <oldsource>Windows operating system version 6.2, corresponds to Windows 8.</oldsource>
        <translation>Sistema operativo Windows version 6.2, corresponde a Windows 8</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2573"/>
        <source>Operating system version 6.3, corresponds to Windows 8.1</source>
        <translation>Sistema operativo Windows version 6.3, corresponde a Windows 8.1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2576"/>
        <source>Operating system version 10.0, corresponds to Windows 10</source>
        <translation>Sistema operativo Windows version 10.0, corresponde a Windows 10</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2579"/>
        <source>Windows CE operating system.</source>
        <translation>Sistema operativo Windows CE.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2582"/>
        <source>Windows CE .NET operating system.</source>
        <translation>Sistema operativo Windows CE.NET.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2585"/>
        <source>Windows CE 5.x operating system.</source>
        <translation>Sistema operativo Windows CE 5.x.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2588"/>
        <source>Windows CE 6.x operating system.</source>
        <translation>Sistema operativo Windows CE 6.x.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2591"/>
        <source>Unknown Windows operating system</source>
        <translation>Sistema operativo Windows Unone</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2600"/>
        <source>Architecture instruction set: </source>
        <translation>Set de instrucciones de arquitectura:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2611"/>
        <source> was created </source>
        <translation> fue creado </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2621"/>
        <location filename="mainwindow.cpp" line="2623"/>
        <location filename="mainwindow.cpp" line="2658"/>
        <source>Compiled by</source>
        <translation>Compilado por</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2654"/>
        <source>Unknown version</source>
        <translation>Versión desconocida</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2663"/>
        <source>Unknown compiler.</source>
        <translation>Compilador desconocido.</translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <location filename="checkupdate.cpp" line="46"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="50"/>
        <source>Check for updates on startup</source>
        <translation>Verificar actualizaciones an el arranque</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="62"/>
        <source>Contacting server...</source>
        <translation>Contactando server...</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="134"/>
        <source>Error message: Host found but
the requested version information was not found.
Please try again later</source>
        <translation>Mensaje de error: Host encontrado, pero no la versión solicitada. Por favor, intente nuevamente</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="135"/>
        <location filename="checkupdate.cpp" line="149"/>
        <location filename="checkupdate.cpp" line="190"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="148"/>
        <source>Error message: </source>
        <translation>Mensaje de error:</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="148"/>
        <source>
Please try again later</source>
        <translation>Por favor, intente nuevamente</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source>New version available: </source>
        <translation>Nueva versión disponible:</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source>Download it from</source>
        <translation>Descargar de</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source> homepage</source>
        <translation>Página principal</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="181"/>
        <source>Your current version is the latest available: </source>
        <translation>La versión instalada es la última disponible:</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="181"/>
        <source>Thank you for using</source>
        <translation>Gracias por usar</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>New version available!</source>
        <translation>Nueva versión disponible!</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>Version </source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>Please visit</source>
        <translation>Por favor, visite</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>homepage</source>
        <translation>Página principal</translation>
    </message>
</context>
</TS>
