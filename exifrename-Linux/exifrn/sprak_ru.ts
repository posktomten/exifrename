<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="ru_RU">
<context>
    <name>Exif_w</name>
    <message>
        <location filename="exif_w.ui" line="35"/>
        <source>Dialog</source>
        <translation>Диалоговое окно</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="72"/>
        <source>Date and time</source>
        <translation>Дата и время</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="85"/>
        <source>Change the time stamp on</source>
        <translation>Изменить время на</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="98"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="111"/>
        <source>Year-Month-Day Hour-Minute-Second</source>
        <translation>Год-Месяц-День Час-Минуты-Секунды</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="124"/>
        <source>OK</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="137"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="150"/>
        <source>Current Time</source>
        <translation>Текущее время</translation>
    </message>
    <message>
        <location filename="exif_w.cpp" line="72"/>
        <source>Incorrect date/time format, unable to write to file.
You must write in this way: </source>
        <translation>Неверный формат даты или времени, невозможно записать в файл.
Необходимый формат: </translation>
    </message>
    <message>
        <location filename="exif_w.cpp" line="101"/>
        <source>Unable to save EXIF data</source>
        <translation>Невозможно сохранить EXIF данные</translation>
    </message>
</context>
<context>
    <name>FilePattern</name>
    <message>
        <location filename="filepattern.ui" line="59"/>
        <source>file name</source>
        <translation>имя файла</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="72"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The numbers in the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The numbers in the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="80"/>
        <location filename="filepattern.ui" line="117"/>
        <location filename="filepattern.ui" line="266"/>
        <source>none</source>
        <translation>none</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="85"/>
        <location filename="filepattern.ui" line="122"/>
        <location filename="filepattern.ui" line="271"/>
        <source>space</source>
        <translation>пробел</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="90"/>
        <location filename="filepattern.ui" line="127"/>
        <location filename="filepattern.ui" line="276"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="95"/>
        <location filename="filepattern.ui" line="132"/>
        <location filename="filepattern.ui" line="281"/>
        <source>_</source>
        <translation>_</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="109"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Date and time component part of the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Date and time component part of the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="149"/>
        <source>File name figures, separated by</source>
        <translation>Имя файла, разделенное</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="168"/>
        <source>Date and time, separated by</source>
        <translation>День и время, разделенные</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="187"/>
        <source>File extension (.jpg or .JPG)</source>
        <translation>Расширение файла (.jpg or .JPG)</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="200"/>
        <source>CAPITAL letter</source>
        <translation>ПРОПИСНАЯ буква</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="216"/>
        <source>lowercase letter</source>
        <translation>строчная буква</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="232"/>
        <source>Save</source>
        <translation>Сохранить</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="245"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="258"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The figures in the folder name are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The figures in the folder name are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="295"/>
        <source>Folder name figures, separated by</source>
        <translation>Имя каталога, разделенное</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="314"/>
        <source>File name:</source>
        <translation>Имя файла:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="333"/>
        <source>Folder name:</source>
        <translation>Имя папки:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="352"/>
        <source>folder name</source>
        <translation>имя папки</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="373"/>
        <source>Only time in file name</source>
        <translation>Только время в имени файла</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="834"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Extensions of files you want to be able to open, separated by cemikolon. S&lt;span style=&quot; color:#000000;&quot;&gt;uch as jpg and tif.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000&quot;&gt;Example:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;jpg;tif;cr2&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;p, li { white-space: pre-wrap; }&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Расширения файлов, которые вы хотите открыть, разделенные точкой с запятой, Н&lt;span style=&quot; color:#000000;&quot;&gt;апример jpg и tiff.&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000&quot;&gt;Example:&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;jpg;tif;cr2&lt;/span&gt;&lt;/p&gt;&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="867"/>
        <source>File extension:</source>
        <translation>Расширение файла:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="886"/>
        <source>*.</source>
        <translation>*.</translation>
    </message>
    <message>
        <location filename="filepattern.cpp" line="97"/>
        <location filename="filepattern.cpp" line="321"/>
        <source>Extension: .</source>
        <translation>Расширение: .</translation>
    </message>
    <message>
        <location filename="filepattern.cpp" line="97"/>
        <location filename="filepattern.cpp" line="321"/>
        <source> or .</source>
        <translation> или .</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="help.cpp" line="62"/>
        <source>Close</source>
        <translation>Закрыть</translation>
    </message>
    <message>
        <location filename="help.cpp" line="155"/>
        <source>Error reading
</source>
        <translation>Ошибка чтения
</translation>
    </message>
    <message>
        <location filename="help.cpp" line="155"/>
        <source>
info file missing!</source>
        <translation>
нет информации о файле!</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="127"/>
        <source>Copy (do not rename)</source>
        <translation>Копировать (без переименования)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="129"/>
        <source>Copy to...</source>
        <translation>Копировать в...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="155"/>
        <source>Preserve original file name</source>
        <translation>Сохранить оригинальное имя файла</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="157"/>
        <location filename="mainwindow.cpp" line="190"/>
        <location filename="mainwindow.cpp" line="400"/>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Before date/time</source>
        <translation>До даты/времени</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="158"/>
        <location filename="mainwindow.cpp" line="191"/>
        <location filename="mainwindow.cpp" line="406"/>
        <location filename="mainwindow.cpp" line="419"/>
        <source>After date/time</source>
        <translation>После даты/времени</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="188"/>
        <source>Insert new file name</source>
        <translation>Вставить новое имя файла</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="212"/>
        <source>Go!</source>
        <translation>Вперед!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="231"/>
        <source>Clear log</source>
        <translation>Очистить отчет</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="233"/>
        <source>Save log</source>
        <translation>Сохранить отчет</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="351"/>
        <source>&amp;Open...</source>
        <translation>&amp;Открыть...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="352"/>
        <source>Ctrl+O</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="353"/>
        <source>Open jpeg image(s)</source>
        <translation>Открыть jpeg изображения</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="357"/>
        <source>&amp;Rename</source>
        <translation>&amp;Переименовать</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="358"/>
        <source>Ctrl+R</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="359"/>
        <source>Rename original file(s)</source>
        <translation>Переименовать оригинальные файлы</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="364"/>
        <source>&amp;Copy to...</source>
        <translation>&amp;Копировать в...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="365"/>
        <source>Ctrl+C</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="366"/>
        <source>Copy original file(s) with new name to folder...</source>
        <translation>Копировать оригинальные файлы с новым именем в папку...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="373"/>
        <source>E&amp;xit</source>
        <translation>В&amp;ыход</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="374"/>
        <source>Ctrl+Q</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="375"/>
        <source>Exit application</source>
        <translation>Выход из приложения</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="385"/>
        <source>Create folders (yyyy_mm_dd)</source>
        <translation>Создать папки (гггг_мм_дд)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="387"/>
        <source>Create folders according to file creation date and copy the files into these folders</source>
        <translation>Создать папки по дате создания файлов и копировать туда файлы</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="392"/>
        <source>No name exept date/time</source>
        <translation>Никаких имен кроме даты или времени</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="394"/>
        <source>File name is only date/time</source>
        <translation>В имени файла только дата или время</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="402"/>
        <source>Preserve original file name before date/time</source>
        <translation>Сохранить оригинальное имя файла до даты/времени</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="408"/>
        <source>Preserve original file name after date/time</source>
        <translation>Сохранить оригинальное имя файла после даты/времени</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="415"/>
        <source>Insert new file name before date/time</source>
        <translation>Вставить новое имя файла до даты/времени</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="421"/>
        <source>Insert new file name after date/time</source>
        <translation>Вставить новое имя файла после даты/времени</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="426"/>
        <location filename="mainwindow.cpp" line="427"/>
        <source>Edit date and time...</source>
        <translation>Редактировать дату и время...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="433"/>
        <source>Help...</source>
        <translation>Помощь...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="434"/>
        <source>Help for application</source>
        <translation>Помощь по приложению</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="438"/>
        <source>About...</source>
        <translation>О программе...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="439"/>
        <source>About application</source>
        <translation>О приложении</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="443"/>
        <source>About Qt...</source>
        <translation>Узнать о Qt...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="444"/>
        <source>About Qt C++ Application Development Framework</source>
        <translation>Узнать о О Qt C++ Application Development Framework</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="448"/>
        <source>Check for updates...</source>
        <translation>Проверить обновления...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="449"/>
        <source>Check for new versions of EXIF ReName</source>
        <translation>Проверить новую версию EXIF ReName</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="455"/>
        <location filename="mainwindow.cpp" line="2331"/>
        <source>English</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="456"/>
        <source>English language</source>
        <translation>Английский язык</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2332"/>
        <source>Italiano</source>
        <translation>итальянский</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="462"/>
        <source>Italian language</source>
        <translation>Итальянский язык</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="461"/>
        <source>Italian</source>
        <translation>итальянский</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="467"/>
        <location filename="mainwindow.cpp" line="2336"/>
        <source>German</source>
        <translation>Немецкий</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="468"/>
        <source>German language</source>
        <translation>немецкий язык</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="473"/>
        <location filename="mainwindow.cpp" line="2333"/>
        <source>Russian</source>
        <translation>Pусский</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="474"/>
        <source>Russian language</source>
        <translation>Pусский язык</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="479"/>
        <location filename="mainwindow.cpp" line="2334"/>
        <source>Swedish</source>
        <oldsource>Svenska</oldsource>
        <translation>шведский</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="480"/>
        <source>Swedish language</source>
        <translation>Шведский язык</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="486"/>
        <source>Spanish language</source>
        <translation>Аниш язык</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="492"/>
        <source>Delete personal settings</source>
        <translation>Удалить персональные настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="493"/>
        <source>Remove your personal settings</source>
        <translation>Удалить персональные настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="497"/>
        <location filename="mainwindow.cpp" line="498"/>
        <source>Specify file name pattern...</source>
        <translation>Уточнить шаблон имени файла...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="506"/>
        <source>Icons only</source>
        <translation>Только иконки</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="507"/>
        <source>Display only icons in the toolbar</source>
        <translation>Показывать только иконки на панели инструментов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="510"/>
        <source>Text only</source>
        <translation>Только текст</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="511"/>
        <source>Display only text in the toolbar</source>
        <translation>Показывать только текст на панели инструментов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="514"/>
        <source>Text alongside icons</source>
        <translation>Текст вместе с иконками</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="515"/>
        <source>Display text alongside icons in the toolbar</source>
        <translation>Показывать текст вместе с иконками на панели инструментов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="519"/>
        <source>Text under icons</source>
        <translation>Текст под иконками</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="520"/>
        <source>Display text under icons in the toolbar</source>
        <translation>Показывать текст под иконками на панели инструментов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="523"/>
        <source>No toolbar</source>
        <translation>Без панели инструментов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="524"/>
        <source>No toolbar will be displayed</source>
        <translation>Панель инструментов не будет отображаться</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="537"/>
        <source>&amp;File</source>
        <translation>&amp;Файл</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="545"/>
        <source>&amp;Edit</source>
        <translation>&amp;Правка</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="551"/>
        <source>&amp;Preserve original file name</source>
        <translation>&amp;Сохранить оригинальное имя файла</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="555"/>
        <source>&amp;Insert new file name</source>
        <translation>Вставить &amp;новое имя файла</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="562"/>
        <source>&amp;View</source>
        <translation>&amp;Вид</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="564"/>
        <location filename="mainwindow.cpp" line="655"/>
        <source>Toolbar</source>
        <translation>Панель инструментов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="574"/>
        <source>&amp;Language</source>
        <translation>&amp;Язык</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="585"/>
        <source>&amp;Tools</source>
        <translation>С&amp;ервис</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="590"/>
        <source>&amp;Help</source>
        <translation>&amp;Помощь</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="605"/>
        <source>Do you want to remove your personal settings?
This is an appropriate action if you like to uninstall this application.
</source>
        <translation>Хотите удалить персональные настройки?
Это рекомендуется делать, если вы собираетесь удалить приложение.
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="605"/>
        <source> will exit after your personal data is deleted.</source>
        <translation> будет совершен выход после удаления персональных настроек.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="606"/>
        <location filename="mainwindow.cpp" line="1662"/>
        <location filename="mainwindow.cpp" line="1703"/>
        <location filename="mainwindow.cpp" line="1742"/>
        <location filename="mainwindow.cpp" line="1784"/>
        <location filename="mainwindow.cpp" line="1826"/>
        <location filename="mainwindow.cpp" line="1867"/>
        <location filename="mainwindow.cpp" line="2104"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="607"/>
        <location filename="mainwindow.cpp" line="1663"/>
        <location filename="mainwindow.cpp" line="1704"/>
        <location filename="mainwindow.cpp" line="1743"/>
        <location filename="mainwindow.cpp" line="1785"/>
        <location filename="mainwindow.cpp" line="1827"/>
        <location filename="mainwindow.cpp" line="1868"/>
        <location filename="mainwindow.cpp" line="2105"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="615"/>
        <source>Your personal settings are deleted and
 </source>
        <translation>Персональные настройки удалены и
 </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="615"/>
        <source> will exit</source>
        <translation> будет совершен выход</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="621"/>
        <source>Your personal settings cannot be found.
New settings will be saved then you restart the program.</source>
        <translation>Персональные настройки не найдены.
Новые настройки будут применены после рестарта приложения.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="653"/>
        <source>File</source>
        <translation>Файл</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="673"/>
        <source>Create folders (yyyy</source>
        <translation>Создать папки (гггг</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="673"/>
        <source>mm</source>
        <translation>мм</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="673"/>
        <source>dd)</source>
        <translation>дд)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="692"/>
        <location filename="mainwindow.cpp" line="2410"/>
        <source>Open File</source>
        <translation>Открыть файл</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="735"/>
        <location filename="mainwindow.cpp" line="2437"/>
        <source>Image files</source>
        <translation>Файлы изображений</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="762"/>
        <location filename="mainwindow.cpp" line="2465"/>
        <source>You do not have enough permissions to read
</source>
        <translation>У вас нет прав на чтение
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="785"/>
        <location filename="mainwindow.cpp" line="786"/>
        <source> File(s) selected</source>
        <translation>Выбранные файлы</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="795"/>
        <location filename="mainwindow.cpp" line="1953"/>
        <location filename="mainwindow.cpp" line="1998"/>
        <source>The original file will be renamed</source>
        <translation>Оригинальные файлы будут переименованы</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="941"/>
        <source>Copy pictures to folder</source>
        <translation>Копировать изображения в папку</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="963"/>
        <source>You have not enough permissions
to write to </source>
        <translation>У вас не достаточно прав
на запись </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="969"/>
        <location filename="mainwindow.cpp" line="1094"/>
        <location filename="mainwindow.cpp" line="1965"/>
        <location filename="mainwindow.cpp" line="1967"/>
        <location filename="mainwindow.cpp" line="1988"/>
        <location filename="mainwindow.cpp" line="1990"/>
        <source>Copy to: </source>
        <translation>Копировать в: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1056"/>
        <source>rename</source>
        <translation>переименование</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1058"/>
        <source>copy</source>
        <translation>копирование</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1064"/>
        <source>You have not selected any file(s).
Please select file(s) to </source>
        <translation>Вы не выбрали файлы.
Пожалуйста, выберите файлы для </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1090"/>
        <source>Error in destination path
Please select another output directory!</source>
        <translation>Ошибка в пути назначения
Пожалуйста, выберите другую директорию для записи!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1178"/>
        <location filename="mainwindow.cpp" line="1241"/>
        <location filename="mainwindow.cpp" line="1292"/>
        <location filename="mainwindow.cpp" line="1348"/>
        <location filename="mainwindow.cpp" line="1407"/>
        <location filename="mainwindow.cpp" line="1466"/>
        <source>Unable to create directory 
</source>
        <translation>Невозможно создать каталог
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1178"/>
        <location filename="mainwindow.cpp" line="1241"/>
        <location filename="mainwindow.cpp" line="1292"/>
        <location filename="mainwindow.cpp" line="1348"/>
        <location filename="mainwindow.cpp" line="1407"/>
        <location filename="mainwindow.cpp" line="1466"/>
        <source>
check your permisions</source>
        <translation>
проверьте свои права</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1200"/>
        <location filename="mainwindow.cpp" line="1314"/>
        <location filename="mainwindow.cpp" line="1428"/>
        <source>Unable to delete:
</source>
        <translation>Невозможно удалить:
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1376"/>
        <source>Indicate a new file name, please!</source>
        <translation>Укажите новое имя файла!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1491"/>
        <source>Done! </source>
        <translation>Готово! </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1491"/>
        <source> of </source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1491"/>
        <source>file(s) )</source>
        <translation>файл(ы) )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1525"/>
        <source>/exif_rename_log.txt</source>
        <translation>/exif_rename_log.txt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1528"/>
        <source>Save log as...</source>
        <translation>Сохранить отчет как...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1536"/>
        <source>Text files (*.txt)</source>
        <translation>Текстовые файлы (*.txt)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1536"/>
        <source>Any files (*)</source>
        <translation>Другие файлы (*)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1555"/>
        <source>Error writing to
</source>
        <translation>Ошибка записи в
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1555"/>
        <source>
check your permissions</source>
        <translation>
проверьте свои права</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1560"/>
        <source>*** Log saved: </source>
        <translation>*** Отчет сохранен: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1566"/>
        <source>Log created by </source>
        <translation>Отчет создан </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1568"/>
        <source>Copyright Ingemar Ceicer 2007 - </source>
        <translation>Copyright Ingemar Ceicer 2007 - </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1627"/>
        <location filename="mainwindow.cpp" line="2100"/>
        <source>Licensierat under GNU General Public License This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version </source>
        <translation>Licensierat under GNU General Public License Это свободное ПО, вы можете распространять и/или модифицировать его в рамках GNU General Public License</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1627"/>
        <location filename="mainwindow.cpp" line="2100"/>
        <source> of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</source>
        <translation>Программа  распространяется без каких-либо гарантий, автор надеется, что она окажется полезной вам. Всю информацию более подробно читайте в тексте GNU General Public License, копия прилагается к этой программе. Иначе напишите по адресу Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1630"/>
        <location filename="mainwindow.cpp" line="2099"/>
        <source>&lt;br&gt;&lt;font coor=&quot;green&quot; size=&quot;3&quot;&gt;&lt;b&gt;Copyright &amp;copy; 2007 - </source>
        <translation>&lt;br&gt;&lt;font coor=&quot;green&quot; size=&quot;3&quot;&gt;&lt;b&gt;Copyright &amp;copy; 2007 - </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1630"/>
        <location filename="mainwindow.cpp" line="2099"/>
        <source>homepage</source>
        <translation>homepage</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1632"/>
        <source>About </source>
        <translation>О программе </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1632"/>
        <location filename="mainwindow.cpp" line="2103"/>
        <source>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</source>
        <translation>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>A small application for renaming pictures. EXIF ReName uses exif data from the picture file.&lt;br&gt;&lt;br&gt;</source>
        <translation>Небольшое приложение для переименования изображений. EXIF ReName использует exif-данные из файла изображения.&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>Thank you Ivan Dolgov for the translation into Russian.</source>
        <translation>Благодарим Ивана Долгова за перевод на русский язык.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>Thank you Roberto Nerozzi for the translation into Italien.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>Thank you Cristian Sosa Noe for the translation into Spanish.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1633"/>
        <source>Thank you Ben Weis for the translation into German.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1661"/>
        <location filename="mainwindow.cpp" line="1702"/>
        <location filename="mainwindow.cpp" line="1741"/>
        <location filename="mainwindow.cpp" line="1783"/>
        <location filename="mainwindow.cpp" line="1825"/>
        <location filename="mainwindow.cpp" line="1866"/>
        <source>The application needs to restart in order
to initialise the now language settings.

Do you want to close
the application now?</source>
        <translation>Приложение нужно перезапустить
чтобы новые языковые настройки вступили в силу.

Хотите закрыть
приложение сейчас?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1663"/>
        <location filename="mainwindow.cpp" line="1704"/>
        <location filename="mainwindow.cpp" line="1743"/>
        <location filename="mainwindow.cpp" line="1785"/>
        <location filename="mainwindow.cpp" line="1827"/>
        <location filename="mainwindow.cpp" line="1868"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1934"/>
        <source>Ready for action!</source>
        <translation>Готово к работе!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2103"/>
        <source>&lt;br&gt;A small application for renaming pictures. EXIF ReName uses exif data from the picture file.&lt;br&gt;&lt;br&gt;</source>
        <translation>&lt;br&gt;Небольшое приложение для переименования изображений. EXIF ReName использует exif-данные из файла изображения.&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2103"/>
        <source>&lt;br&gt;&lt;br&gt;&lt;font color=&quot;#FF0000&quot; size=&quot;4&quot;&gt;Do you accept the license?&lt;/font&gt;</source>
        <translation>&lt;br&gt;&lt;br&gt;&lt;font color=&quot;#FF0000&quot; size=&quot;4&quot;&gt;Do you accept the license?&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2141"/>
        <location filename="mainwindow.cpp" line="2149"/>
        <source> is updatet to </source>
        <translation> обновлено до </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2185"/>
        <source>Copied to new folder in: &lt;b&gt;</source>
        <translation>Скопировано в новый каталог: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2185"/>
        <location filename="mainwindow.cpp" line="2189"/>
        <source>&lt;/b&gt; and renamed: </source>
        <translation>&lt;/b&gt; и переименовано:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2189"/>
        <source>Copied to: &lt;b&gt;</source>
        <translation>Скопировано в: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2200"/>
        <source>&lt;br /&gt;Selected file(s) in: &lt;b&gt;</source>
        <translation>&lt;br /&gt;Выбранные файл(ы) в: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="485"/>
        <location filename="mainwindow.cpp" line="2335"/>
        <source>Spanish</source>
        <translation>испанский</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2337"/>
        <source>Choose the language that will be used by EXIF Rename</source>
        <translation>Выберите язык для EXIF Rename</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2351"/>
        <source>You must restart the application to the Russian language settings to take effect.</source>
        <translation>Вы должны перезапустить приложение, чтобы русский язык настройки вступили в силу.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2359"/>
        <source>You must restart the application to the Italien language settings to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2374"/>
        <location filename="mainwindow.cpp" line="2381"/>
        <source>You must restart the application to the Spanish language settings to take effect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2531"/>
        <source>MS-DOS-based version of Windows.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2534"/>
        <source>NT-based version of Windows.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2537"/>
        <source>CE-based version of Windows.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2540"/>
        <source>Windows 3.1 with Win 32s operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2543"/>
        <source>Windows 95 operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2546"/>
        <source>Windows 98 operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2549"/>
        <source>Windows Me operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2552"/>
        <source>Windows operating system version 4.0, corresponds to Windows NT.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2555"/>
        <source>Windows operating system version 5.0, corresponds to Windows 2000.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2558"/>
        <source>Windows operating system version 5.1, corresponds to Windows XP.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2561"/>
        <source>Windows operating system version 5.2, corresponds to Windows Server 2003, Windows Server 2003 R2, Windows Home Server, and Windows XP Professional x64 Edition.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2564"/>
        <source>Windows operating system version 6.0, corresponds to Windows Vista and Windows Server 2008.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2567"/>
        <source>Windows operating system version 6.1, corresponds to Windows 7 and Windows Server 2008 R2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2570"/>
        <source>Windows operating system version 6.2, corresponds to Windows 8</source>
        <oldsource>Windows operating system version 6.2, corresponds to Windows 8.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2573"/>
        <source>Operating system version 6.3, corresponds to Windows 8.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2576"/>
        <source>Operating system version 10.0, corresponds to Windows 10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2579"/>
        <source>Windows CE operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2582"/>
        <source>Windows CE .NET operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2585"/>
        <source>Windows CE 5.x operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2588"/>
        <source>Windows CE 6.x operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2591"/>
        <source>Unknown Windows operating system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2600"/>
        <source>Architecture instruction set: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2611"/>
        <source> was created </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2621"/>
        <location filename="mainwindow.cpp" line="2623"/>
        <location filename="mainwindow.cpp" line="2658"/>
        <source>Compiled by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2654"/>
        <source>Unknown version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2663"/>
        <source>Unknown compiler.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2367"/>
        <source>You must restart the application to the Swedish language settings to take effect.</source>
        <translation>Вы должны перезапустить приложение, чтобы шведские языковые параметры вступили в силу.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2441"/>
        <source>All files</source>
        <translation>Все файлы</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2477"/>
        <source>Changes the EXIF information for these files</source>
        <translation>Изменить EXIF-данные для этих файлов</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2486"/>
        <source> ...selected</source>
        <translation> ...выбраны</translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <location filename="checkupdate.cpp" line="46"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="50"/>
        <source>Check for updates on startup</source>
        <translation>Проверять обновления при запуске</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="62"/>
        <source>Contacting server...</source>
        <translation>Соединение с сервером...</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="134"/>
        <source>Error message: Host found but
the requested version information was not found.
Please try again later</source>
        <translation>Ошибка: хост найден, но
информация по версии продукта отсутствует.
Пожалуйста, попробуйте позже</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="135"/>
        <location filename="checkupdate.cpp" line="149"/>
        <location filename="checkupdate.cpp" line="190"/>
        <source>OK</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="148"/>
        <source>Error message: </source>
        <translation>Ошибка: </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="148"/>
        <source>
Please try again later</source>
        <translation>Пожалуйста, попробуйте позже</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source>New version available: </source>
        <translation>Новая версия доступна: </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source>Download it from</source>
        <translation>Скачать из</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source> homepage</source>
        <translation>домашняя страница</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="181"/>
        <source>Your current version is the latest available: </source>
        <translation>Текущая версия является последней доступной: </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="181"/>
        <source>Thank you for using</source>
        <translation>Спасибо за использование</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>New version available!</source>
        <translation>Новая версия доступна!</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>Version </source>
        <translation>Версия </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>Please visit</source>
        <translation>Пожалуйста, посетите</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>homepage</source>
        <translation>homepage</translation>
    </message>
</context>
</TS>
