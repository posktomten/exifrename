/*
    EXIF ReName
    Copyright (C) 2007-2019 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "help.h"
#include "config.h"
#include "mainwindow.h"

#include<fstream>
#include<string>
#include<vector>
#include<iostream>
//#include <QStringBuilder>


//#define QT_USE_FAST_CONCATENATION

QString configure_path = QDir::toNativeSeparators(QDir::homePath() + "/.exifrename");


string my_configure_path = configure_path.toStdString();



using namespace std;



Config::Config(QString namn, int nummer)
{
    //Läser in configurationsfilen i minnet
    //vid skapandet
    namn = namn.trimmed();
    //Hela sökvägen
    QString configure = configure_path + QDir::toNativeSeparators(namn);
    string configure_s = configure.toStdString();
    conf_file = new string;
    *conf_file = configure_s;
    cr = new Createuser;
    checkConf(configure_s, nummer);
    readConf(configure_s);
    delete cr;
}



Config::~Config()
{
    //Skriver configurationsfilen innan döden
    writeConf();
}

// Lämnar ut "values" grundade på "name"
string Config::getConf(string name)
{
    vector<string>::iterator it;
    string s;

    for(it = name_values->begin(); it < name_values->end(); it++) {
        if(it->substr(0, 1) != "#") { // hoppa över kommaterade rader direkt
            // Söker efter samma delsträng som skickas hit med samma längd
            // som hitskickade strängen. Samt att nästa tecken är "="
            // För att undvika fel typ: gustav gustavson
            if((it->substr(0, name.size()) == name) && (it->substr(name.size(), 1) == "=")) {
                // Returnerar från tecknet efter "=" tecknet och til radens slut
                s = it->substr(name.size() + 1, it->size());
                return s;
            }
        }
    }

    s = "nothing";
    return s;
}

bool Config::setConf(string name, string value)
{
    vector<string>::iterator it;

    for(it = name_values->begin(); it < name_values->end(); it++) {
        if(it->substr(0, 1) != "#") { // hoppa över kommaterade rader direkt
            // Söker efter strängen "name" som skickas hit med samma längd
            // som hitskickade strängen. Samt att nästa tecken är "="
            // För att undvika fel typ: gustav gustavson
            if((it->substr(0, name.size()) == name) && (it->substr(name.size(), 1) == "=")) {
                // Returnerar från tecknet efter "=" tecknet och til radens slut
                // "name" har hittats, nu skall nya värdet läggaas in
                name.append("=");
                name.append(value);
                *it = name;
                return true;
            }
        }
    }

    return false;
}



void Config::newConf(string name, string value)
{
    string pair = name + "=" + value;
    name_values->push_back(pair);
}



// Läser configurationsvektorn
bool Config::readConf(string conf)
{
    name_values = new vector<string>;
    string conf_values;
    ifstream infil;
    infil.open(conf.c_str());

    if(!(infil.good())) {
        return false;
    }

    while(getline(infil, conf_values)) {
        name_values->push_back(conf_values);
    }

    infil.close();
    return true;
}

// skriver configurationsvektorn till fil
bool Config::writeConf()
{
    ofstream utfil;
    utfil.open(conf_file->c_str(), ios::out);

    if(!(utfil.is_open())) {
        return false;
    }

    vector<string>::iterator it;

    for(it = name_values->begin(); it < name_values->end(); it++) {
        utfil << *it;
        utfil << "\n";
    }

    delete conf_file;
    return true;
}
// kollar om konfigurationsfilen finns
void Config::checkConf(string conf, int nummer)
{
    ifstream infil2;
    infil2.open(conf.c_str());
    infil2.seekg(0, ios::end);

    if(infil2.tellg() < 1) {
        infil2.close();
        createConffile(conf, nummer);
    }
}
// skapar en default konfigurationsfil
bool Config::createConffile(string conf, int nummer)
{
    cr->mkDir();

    switch(nummer) { // olika konfiguration för olika konfigurationsfiler
        case 1: {
            ofstream utfil2;
            utfil2.open(conf.c_str(), ios::out);

            if(utfil2.good()) {
                utfil2 << "first_time=1" << endl;
                utfil2 << "version="VERSION << endl;
                utfil2 << "language=eng" << endl;
                utfil2 << "copy=0" << endl;
                utfil2 << "preserve=0" << endl;
                utfil2 << "preserve_before=0" << endl;
                utfil2 << "insert=0" << endl;
                utfil2 << "insert_before=0" << endl;
                utfil2 << "file_name=" << endl;
                utfil2 << "copy_path=" << endl;
                utfil2 << "open_path=" << endl;
                utfil2 << "log_path=" << endl;
                utfil2 << "vidd=749" << endl;
                utfil2 << "hojd=401" << endl;
                utfil2 << "x_lage=150" << endl;
                utfil2 << "y_lage=150" << endl;
                utfil2 << "hvidd=600" << endl;
                utfil2 << "hhojd=675" << endl;
                utfil2 << "hx_lage=150" << endl;
                utfil2 << "hy_lage=150" << endl;
                utfil2 << "ToolButtonStyle=3" << endl;
                utfil2 << "create_folder=0" << endl;
                utfil2 << "check_onstart=1" << endl;
                utfil2.close();
                return true;
                break;
            } else {
                return false;
                break;
            }
        }

        case 2: {
            ofstream utfil3;
            utfil3.open(conf.c_str(), ios::out);

            if(utfil3.good()) {
                utfil3 << "separator=-" << endl;
                utfil3 << "date_time_separator=_" << endl;
                utfil3 << "folder_separator=-" << endl;
                utfil3 << "extension=lowercase" << endl;
                utfil3 << "onlytime=0" << endl;
                utfil3 << "extension_name=jpg" << endl;
                utfil3 << "current_extension=jpg" << endl;
                utfil3 << "current_filter=" << endl;
                utfil3.close();
                return true;
                break;
            } else {
                return false;
                break;
            }
        }

        default:
            return false;
    }

    return false;
}
