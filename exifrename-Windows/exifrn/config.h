
/*
    EXIF ReName
    Copyright (C) 2011-2013 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/


#ifndef CONFIGH
#define CONFIGH
#include<string>
#include<vector>
#include "createuser.h"

using namespace std;


class Config
{


private:


vector<string> *name_values;
bool createConffile(string conf,int nummer);
void checkConf(string conf,int nummer);
bool readConf(string conf);
bool writeConf();
string *conf_file;
Createuser *cr;




public:
Config(QString namn,int nummer);
~Config();



void newConf(string name,string value);
string getConf(string name);
bool setConf(string name,string value);

};

#endif
