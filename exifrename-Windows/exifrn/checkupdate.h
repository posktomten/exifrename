
/*
    EXIF ReName
    Copyright (C) 2011-2013 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#ifndef CHECKUPDATE_H
#define CHECKUPDATE_H
#include "config.h"
#include <QDebug>
#include <QDialog>
#include <QVBoxLayout>
#include <QPushButton>
#include <QTextEdit>
#include <QLabel>
#include <QtNetwork>
#include <QFileInfo>
#include <QDir>
#include <QFile>
#include <QHttp>
#include <QMessageBox>
#include <QColor>
#include <QCheckBox>
#include <QFont>
#include <QString>
#include <QTimer>



class Update : public QDialog
{
    Q_OBJECT
public:

~Update();
void startUpdate(Config *kk);

void startUppCheck(Config *kk);

protected:


private slots:
	
	void close_me();

	void do_update();
	void handleResponse(bool);
	void auto_check(int);
        void checkTystUpdate(bool);
	void setValue();


private:
	int httpGetId;
	QHttp *http;
	QFile *file;
        Config *k;
	QLabel *ted_animo;

	QVBoxLayout *topTopLayout;
	QPushButton *pu_terminate;
	QLabel *ted_info;
	QCheckBox *ch_automatiskt;
	QTimer *timer;
	int *index;

	 bool jfr(QString ny_version);
	 bool fortsatta;

         void closeEvent ( QCloseEvent * event );

};

#endif
