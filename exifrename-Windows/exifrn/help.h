/*
    EXIF ReName
    Copyright (C) 2011-2013 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/
#ifndef HELPH
#define HELPH
#include "config.h"

#include <QVBoxLayout>
#include <QPushButton>
#include <QDialog>
#include <QTextEdit>
#include <QMainWindow>
#include <string>



class Help : public QDialog
{
    Q_OBJECT
public:

   void startHelp(std::string fil,Config *k);




protected:

	void closeEvent(QCloseEvent *event);
private slots:

	void closeX();
private:


	 QVBoxLayout *topTopLayout;
	 QPushButton *pu;
	 QTextEdit *te;
	 void read_info(std::string fil);
         void getConfig(Config *k);
         void closeHelp();
         Config *kh;

};

#endif
