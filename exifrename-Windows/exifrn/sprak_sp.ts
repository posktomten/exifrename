<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_AR" sourcelanguage="en">
<context>
    <name>Exif_w</name>
    <message>
        <location filename="exif_w.ui" line="29"/>
        <source>Dialog</source>
        <translation>Dialogo</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="65"/>
        <source>Date and time</source>
        <translation>Fecha y hora</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="78"/>
        <source>Change the time stamp on</source>
        <translation>Cambiar la etiqueta de tiempo en</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="91"/>
        <source>TextLabel</source>
        <translation>Etiqueta de texto</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="104"/>
        <source>Year-Month-Day Hour-Minute-Second</source>
        <translation>Año-Mes-Dia-Hora-Minuto-Segundo</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="117"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="130"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="143"/>
        <source>Current Time</source>
        <translation>Hora actual</translation>
    </message>
    <message>
        <location filename="exif_w.cpp" line="72"/>
        <source>Incorrect date/time format, unable to write to file.
You must write in this way: </source>
        <translation>Formato incorrecto de fecha/hora, no se puede grabar el archivo.
Debe escribirlos de esta forma:</translation>
    </message>
    <message>
        <location filename="exif_w.cpp" line="101"/>
        <source>Unable to save EXIF data</source>
        <translation>No se pueden grabar los datos EXIF</translation>
    </message>
</context>
<context>
    <name>FilePattern</name>
    <message>
        <location filename="filepattern.ui" line="58"/>
        <source>file name</source>
        <translation>Nombre del archivo</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="71"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The numbers in the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The numbers in the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="79"/>
        <location filename="filepattern.ui" line="116"/>
        <location filename="filepattern.ui" line="265"/>
        <source>none</source>
        <translation>ninguno</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="84"/>
        <location filename="filepattern.ui" line="121"/>
        <location filename="filepattern.ui" line="270"/>
        <source>space</source>
        <translation>espacio</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="89"/>
        <location filename="filepattern.ui" line="126"/>
        <location filename="filepattern.ui" line="275"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="94"/>
        <location filename="filepattern.ui" line="131"/>
        <location filename="filepattern.ui" line="280"/>
        <source>_</source>
        <translation>_</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="108"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Date and time component part of the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Date and time component part of the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="148"/>
        <source>File name figures, separated by</source>
        <translation>Caracteres de nombre de archivo separados por</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="167"/>
        <source>Date and time, separated by</source>
        <translation>Fecha y hora separados por</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="186"/>
        <source>File extension (.jpg or .JPG)</source>
        <translation>Extención de archivo (.jpg  o .JPG)</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="199"/>
        <source>CAPITAL letter</source>
        <translation>MAYUSCULAS</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="215"/>
        <source>lowercase letter</source>
        <translation>minúsculas</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="231"/>
        <source>Save</source>
        <translation>Grabar</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="244"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="257"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The figures in the folder name are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The figures in the folder name are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="294"/>
        <source>Folder name figures, separated by</source>
        <translation>Palabras del Nombre de Carpeta, separadas por</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="313"/>
        <source>File name:</source>
        <translation>Nombre del archivo:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="332"/>
        <source>Folder name:</source>
        <translation>Nombre de la carpeta:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="351"/>
        <source>folder name</source>
        <translation>Nombre de la carpeta</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="367"/>
        <source>Only time in file name</source>
        <translation>Solo hora en nombre de archivo</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="801"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Extensions of files you want to be able to open, separated by cemikolon. S&lt;span style=&quot; color:#000000;&quot;&gt;uch as jpg and tif.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000&quot;&gt;Example:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;jpg;tif;cr2&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Extensions of files you want to be able to open, separated by cemikolon. S&lt;span style=&quot; color:#000000;&quot;&gt;uch as jpg and tif.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000&quot;&gt;Example:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;jpg;tif;cr2&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="834"/>
        <source>File extension:</source>
        <translation>Extensión de archivo:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="853"/>
        <source>*.</source>
        <translation>*.</translation>
    </message>
    <message>
        <location filename="filepattern.cpp" line="97"/>
        <location filename="filepattern.cpp" line="321"/>
        <source>Extension: .</source>
        <translation>Extensión: .</translation>
    </message>
    <message>
        <location filename="filepattern.cpp" line="97"/>
        <location filename="filepattern.cpp" line="321"/>
        <source> or .</source>
        <translation>o .</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="help.cpp" line="55"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="help.cpp" line="148"/>
        <source>Error reading
</source>
        <translation>Error al leer</translation>
    </message>
    <message>
        <location filename="help.cpp" line="148"/>
        <source>
info file missing!</source>
        <translation>Información del archivo faltante!</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="120"/>
        <source>Copy (do not rename)</source>
        <translation>Copiar (no renombrar)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="122"/>
        <source>Copy to...</source>
        <translation>Copiar a...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="148"/>
        <source>Preserve original file name</source>
        <translation>Preservar el nombre de archivo original</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="150"/>
        <location filename="mainwindow.cpp" line="183"/>
        <location filename="mainwindow.cpp" line="392"/>
        <location filename="mainwindow.cpp" line="405"/>
        <source>Before date/time</source>
        <translation>Antes de fecha/hora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="151"/>
        <location filename="mainwindow.cpp" line="184"/>
        <location filename="mainwindow.cpp" line="398"/>
        <location filename="mainwindow.cpp" line="411"/>
        <source>After date/time</source>
        <translation>Después de fecha/hora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="181"/>
        <source>Insert new file name</source>
        <translation>Insertar nuevo nombre de archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="205"/>
        <source>Go!</source>
        <translation>Comezar!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="224"/>
        <source>Clear log</source>
        <translation>Limpiar log</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="226"/>
        <source>Save log</source>
        <translation>Guardar log</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="343"/>
        <source>&amp;Open...</source>
        <translation>&amp;Abrir...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="344"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="345"/>
        <source>Open jpeg image(s)</source>
        <translation>Abrir imagen(es) jpeg</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="349"/>
        <source>&amp;Rename</source>
        <translation>&amp;Renombrar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="350"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="351"/>
        <source>Rename original file(s)</source>
        <translation>Renombrar archivo(s) original(es)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="356"/>
        <source>&amp;Copy to...</source>
        <translation>&amp;Copiar a...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="357"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="358"/>
        <source>Copy original file(s) with new name to folder...</source>
        <translation>Copiar archivos originales renombrados a la carpeta...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="365"/>
        <source>E&amp;xit</source>
        <translation>&amp;Salir</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="366"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="367"/>
        <source>Exit application</source>
        <translation>Salir de la aplicación</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="377"/>
        <source>Create folders (yyyy_mm_dd)</source>
        <translation>Crear carpetas (aaaa_mm_dd)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="379"/>
        <source>Create folders according to file creation date and copy the files into these folders</source>
        <translation>Crear carpetas de acuerdo a la fecha de creación de los archivos y copiarlos dentro de ellas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="384"/>
        <source>No name exept date/time</source>
        <translation>Sin nombre, excepto fecha/hora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="386"/>
        <source>File name is only date/time</source>
        <translation>Nombre de archivo es sólo fecha/hora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="394"/>
        <source>Preserve original file name before date/time</source>
        <translation>Mantener el nombre de archivo original antes de fecha/hora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="400"/>
        <source>Preserve original file name after date/time</source>
        <translatorcomment>Cristian Sosa Noe</translatorcomment>
        <translation>Mantener nombre original despues de fecha/hora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>Insert new file name before date/time</source>
        <translation>Insertar nuevo nombre de archivo antes de fecha/hora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Insert new file name after date/time</source>
        <translation>Insertar nuevo nombre de archivo luego de fecha/hora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="418"/>
        <location filename="mainwindow.cpp" line="419"/>
        <source>Edit date and time...</source>
        <translation>Editar fecha y hora...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="425"/>
        <source>Help...</source>
        <translation>Ayuda...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="426"/>
        <source>Help for application</source>
        <translation>Ayuda de la aplicación</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="430"/>
        <source>About...</source>
        <translation>Acerca de...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="431"/>
        <source>About application</source>
        <translation>Acerca de la aplicación</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="435"/>
        <source>About Qt...</source>
        <translation>Acerca de Qt...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="436"/>
        <source>About Qt C++ Application Development Framework</source>
        <translation>Acerca de Qt C++ Application Development Framework</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="440"/>
        <source>Check for updates...</source>
        <translation>Verificar actuallizaciones...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="441"/>
        <source>Check for new versions of EXIF ReName</source>
        <translation>Verificar nuevas versiones de EXIF ReName</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="447"/>
        <location filename="mainwindow.cpp" line="2306"/>
        <source>English</source>
        <translation>Inglés</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="448"/>
        <source>English language</source>
        <translation>Idioma Inglés</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2307"/>
        <source>Italiano</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="454"/>
        <source>Italian language</source>
        <translation>Idioma Italiano</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="453"/>
        <source>Italian</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="459"/>
        <location filename="mainwindow.cpp" line="2311"/>
        <source>German</source>
        <translation>Alemán</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="460"/>
        <source>German language</source>
        <translation>Lengua alemana</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="465"/>
        <location filename="mainwindow.cpp" line="2308"/>
        <source>Russian</source>
        <translation>Ruso</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="466"/>
        <source>Russian language</source>
        <translation>Idioma Ruso</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="471"/>
        <location filename="mainwindow.cpp" line="2309"/>
        <source>Swedish</source>
        <oldsource>Svenska</oldsource>
        <translation>Sueco</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="472"/>
        <source>Swedish language</source>
        <translation>Idioma Sueco</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="478"/>
        <source>Spanish language</source>
        <translation>lengua Española</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="484"/>
        <source>Delete personal settings</source>
        <translation>Borrar preferencias personales</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="485"/>
        <source>Remove your personal settings</source>
        <translation>Eliminar preferencias personales</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="489"/>
        <location filename="mainwindow.cpp" line="490"/>
        <source>Specify file name pattern...</source>
        <translation>Especificar formato de nombre de archivo...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="498"/>
        <source>Icons only</source>
        <translation>Solo iconos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="499"/>
        <source>Display only icons in the toolbar</source>
        <translation>Mostrar solo iconos en barra de herramientas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="502"/>
        <source>Text only</source>
        <translation>Solo texto</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="503"/>
        <source>Display only text in the toolbar</source>
        <translation>Mostrar solo texto en barra de herramientas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="506"/>
        <source>Text alongside icons</source>
        <translation>Texto junto a iconos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="507"/>
        <source>Display text alongside icons in the toolbar</source>
        <translation>Mostrar texto junto a iconos en la barra de herramientas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="511"/>
        <source>Text under icons</source>
        <translation>Texto debajo de los iconos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="512"/>
        <source>Display text under icons in the toolbar</source>
        <translation>Mostrar texto debajo de los iconos de la barra de tareas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="515"/>
        <source>No toolbar</source>
        <translation>Sin barra de tareas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="516"/>
        <source>No toolbar will be displayed</source>
        <translation>No se mostrará barra de tareas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="528"/>
        <source>&amp;File</source>
        <translation>&amp;Archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="536"/>
        <source>&amp;Edit</source>
        <translation>&amp;Editar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="542"/>
        <source>&amp;Preserve original file name</source>
        <translation>&amp;Preservar nombre de archivo original</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="546"/>
        <source>&amp;Insert new file name</source>
        <translation>&amp;Insertar nuevo nombre de archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="553"/>
        <source>&amp;View</source>
        <translation>&amp;Ver</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="555"/>
        <location filename="mainwindow.cpp" line="645"/>
        <source>Toolbar</source>
        <translation>Barra de herramientas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="565"/>
        <source>&amp;Language</source>
        <translation>&amp;Idioma</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="576"/>
        <source>&amp;Tools</source>
        <translation>&amp;Herramientas</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="581"/>
        <source>&amp;Help</source>
        <translation>&amp;Ayuda</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="596"/>
        <source>Do you want to remove your personal settings?
This is an appropriate action if you like to uninstall this application.
</source>
        <translation>Desea borrar las preferencias?
 Esto es apropiado si desea desinstalar la aplicación.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="596"/>
        <source> will exit after your personal data is deleted.</source>
        <translation>Saldrá luego de borrar sus datos personales.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="597"/>
        <location filename="mainwindow.cpp" line="1643"/>
        <location filename="mainwindow.cpp" line="1684"/>
        <location filename="mainwindow.cpp" line="1723"/>
        <location filename="mainwindow.cpp" line="1765"/>
        <location filename="mainwindow.cpp" line="1807"/>
        <location filename="mainwindow.cpp" line="1848"/>
        <location filename="mainwindow.cpp" line="2079"/>
        <source>Yes</source>
        <translation>Si</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="598"/>
        <location filename="mainwindow.cpp" line="1644"/>
        <location filename="mainwindow.cpp" line="1685"/>
        <location filename="mainwindow.cpp" line="1724"/>
        <location filename="mainwindow.cpp" line="1766"/>
        <location filename="mainwindow.cpp" line="1808"/>
        <location filename="mainwindow.cpp" line="1849"/>
        <location filename="mainwindow.cpp" line="2080"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="606"/>
        <source>Your personal settings are deleted and
 </source>
        <translation>Se borrarán sus preferencias personales y</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="606"/>
        <source> will exit</source>
        <translation>saldrá</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="612"/>
        <source>Your personal settings cannot be found.
New settings will be saved then you restart the program.</source>
        <translation>No se han encontrado preferencias personales.
Se guardarán nuevas preferencias luego de reiniciar el programa.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="643"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="663"/>
        <source>Create folders (yyyy</source>
        <translation>Crear carpetas (aaaa</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="663"/>
        <source>mm</source>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="663"/>
        <source>dd)</source>
        <translation>dd)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="682"/>
        <location filename="mainwindow.cpp" line="2385"/>
        <source>Open File</source>
        <translation>Abrir Archivo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="725"/>
        <location filename="mainwindow.cpp" line="2412"/>
        <source>Image files</source>
        <translation>Archivos de imagen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="752"/>
        <location filename="mainwindow.cpp" line="2440"/>
        <source>You do not have enough permissions to read
</source>
        <translation>No tiene suficientes permisos de lectura</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="775"/>
        <location filename="mainwindow.cpp" line="776"/>
        <source> File(s) selected</source>
        <translation>Archivo(s) seleccionados</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="785"/>
        <location filename="mainwindow.cpp" line="1933"/>
        <location filename="mainwindow.cpp" line="1978"/>
        <source>The original file will be renamed</source>
        <translation>El archivo original será renombrado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="931"/>
        <source>Copy pictures to folder</source>
        <translation>Copiar imagenes a carpeta</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="953"/>
        <source>You have not enough permissions
to write to </source>
        <translation>No tiene suficientes permisos para escribir en</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="959"/>
        <location filename="mainwindow.cpp" line="1084"/>
        <location filename="mainwindow.cpp" line="1945"/>
        <location filename="mainwindow.cpp" line="1947"/>
        <location filename="mainwindow.cpp" line="1968"/>
        <location filename="mainwindow.cpp" line="1970"/>
        <source>Copy to: </source>
        <translation>Copiar a:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1046"/>
        <source>rename</source>
        <translation>Renombrar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1048"/>
        <source>copy</source>
        <translation>Copiar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1054"/>
        <source>You have not selected any file(s).
Please select file(s) to </source>
        <translation>No ha seleccionado ningún archivo.
Por favor seleccione archivo(s)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1080"/>
        <source>Error in destination path
Please select another output directory!</source>
        <translation>Error en la ruta de destino
Por favor, selecciones un directorio de salida!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1168"/>
        <location filename="mainwindow.cpp" line="1231"/>
        <location filename="mainwindow.cpp" line="1282"/>
        <location filename="mainwindow.cpp" line="1338"/>
        <location filename="mainwindow.cpp" line="1397"/>
        <location filename="mainwindow.cpp" line="1456"/>
        <source>Unable to create directory 
</source>
        <translation>No se puede crear el directorio</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1168"/>
        <location filename="mainwindow.cpp" line="1231"/>
        <location filename="mainwindow.cpp" line="1282"/>
        <location filename="mainwindow.cpp" line="1338"/>
        <location filename="mainwindow.cpp" line="1397"/>
        <location filename="mainwindow.cpp" line="1456"/>
        <source>
check your permisions</source>
        <translation>Verifique sus permisos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1190"/>
        <location filename="mainwindow.cpp" line="1304"/>
        <location filename="mainwindow.cpp" line="1418"/>
        <source>Unable to delete:
</source>
        <translation>No se puede eliminar:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1366"/>
        <source>Indicate a new file name, please!</source>
        <translation>Indique nuevo nombre de archivo, por favor!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1481"/>
        <source>Done! </source>
        <translation>Hecho!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1481"/>
        <source> of </source>
        <translation>de</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1481"/>
        <source>file(s) )</source>
        <translation>archivo(s) )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1509"/>
        <source>/exif_rename_log.txt</source>
        <translation>/exif_rename_log.txt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1512"/>
        <source>Save log as...</source>
        <translation>Guardar logs como...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1519"/>
        <source>Text files (*.txt)</source>
        <translation>Archivo de texto (*.txt)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1519"/>
        <source>Any files (*)</source>
        <translation>Cualquier archivo (*)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1538"/>
        <source>Error writing to
</source>
        <translation>Error al escribir en</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1538"/>
        <source>
check your permissions</source>
        <translation>Verifique sus permisos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1543"/>
        <source>*** Log saved: </source>
        <translation>***Log guardado:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1549"/>
        <source>Log created by </source>
        <translation>Log creado por</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1551"/>
        <source>Copyright Ingemar Ceicer 2007 - </source>
        <translation>Copyright Ingemar Ceicer 2007 - </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1609"/>
        <location filename="mainwindow.cpp" line="2075"/>
        <source>Licensierat under GNU General Public License This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version </source>
        <translation>Licencia GNU General Public License. Este programe es libre; puede redistribuirse y/o modificarse bajo los términos de GNU General Public License como fue publicado por Free Software Foundation; en cualquier vesión </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1609"/>
        <location filename="mainwindow.cpp" line="2075"/>
        <source> of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</source>
        <translation> de la licencia, o (a su opción) cualquier versión posterior. Este programa se distribuye esperando que sea util, pero SIN NINGUNA GARANTIA; sin siquiera la garantia implícita de COMERCIALIZACION o APLICACIÓN PARA UN USO EN PARTICULAR. Vea la GNU General Public License para mas detalles. Usted debe haber recibido una copia de la GNU General Public License con este programa; si no es así, escriba a Free Software Foundation Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1612"/>
        <location filename="mainwindow.cpp" line="2074"/>
        <source>&lt;br&gt;&lt;font coor=&quot;green&quot; size=&quot;3&quot;&gt;&lt;b&gt;Copyright &amp;copy; 2007 - </source>
        <translation>&lt;br&gt;&lt;font coor=&quot;green&quot; size=&quot;3&quot;&gt;&lt;b&gt;Copyright &amp;copy; 2007 - </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1612"/>
        <location filename="mainwindow.cpp" line="2074"/>
        <source>homepage</source>
        <translation>Pagina principal</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1613"/>
        <source>About </source>
        <translation>Acerca de</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1613"/>
        <location filename="mainwindow.cpp" line="2078"/>
        <source>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</source>
        <translation>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1614"/>
        <source>A small application for renaming pictures. EXIF ReName uses exif data from the picture file.&lt;br&gt;&lt;br&gt;</source>
        <translation>Una pequeña aplicación para renombrar imagenes. EXIF ReName usa los datos exif del archivo de imagen.&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1614"/>
        <source>Thank you Ivan Dolgov for the translation into Russian.</source>
        <translation>Gracias a Ivan Dologov por la traducción a Ruso.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1614"/>
        <source>Thank you Roberto Nerozzi for the translation into Italien.</source>
        <translation>Gracias a Roberto Nerozzi por la traducción al Italiano.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1614"/>
        <source>Thank you Cristian Sosa Noe for the translation into Spanish.</source>
        <translation>Gracias Cristian Sosa Noe para la traducción al Español.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1614"/>
        <source>Thank you Ben Weis for the translation into German.</source>
        <translation>Gracias Ben Weis para la traducción al Alemán.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1642"/>
        <location filename="mainwindow.cpp" line="1683"/>
        <location filename="mainwindow.cpp" line="1722"/>
        <location filename="mainwindow.cpp" line="1764"/>
        <location filename="mainwindow.cpp" line="1806"/>
        <location filename="mainwindow.cpp" line="1847"/>
        <source>The application needs to restart in order
to initialise the now language settings.

Do you want to close
the application now?</source>
        <translation>La aplicación necesita reiniciarse para aplicar el nuevo idioma.

Desea cerrar la aplicación ahora?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1644"/>
        <location filename="mainwindow.cpp" line="1685"/>
        <location filename="mainwindow.cpp" line="1724"/>
        <location filename="mainwindow.cpp" line="1766"/>
        <location filename="mainwindow.cpp" line="1808"/>
        <location filename="mainwindow.cpp" line="1849"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1914"/>
        <source>Ready for action!</source>
        <translation>Listo para la acción!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2078"/>
        <source>&lt;br&gt;A small application for renaming pictures. EXIF ReName uses exif data from the picture file.&lt;br&gt;&lt;br&gt;</source>
        <translation>&lt;br&gt;Una pequeña aplicación para renombrar imagenes. EXIF ReName usa los datos exif del archivo de imagen.&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2078"/>
        <source>&lt;br&gt;&lt;br&gt;&lt;font color=&quot;#FF0000&quot; size=&quot;4&quot;&gt;Do you accept the license?&lt;/font&gt;</source>
        <translation>&lt;br&gt;&lt;br&gt;&lt;font color=&quot;#FF0000&quot; size=&quot;4&quot;&gt;Acepta la licencia?&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2116"/>
        <location filename="mainwindow.cpp" line="2124"/>
        <source> is updatet to </source>
        <translation>se actualizó a</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2160"/>
        <source>Copied to new folder in: &lt;b&gt;</source>
        <translation>Copiado a nueva carpeta en:&lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2160"/>
        <location filename="mainwindow.cpp" line="2164"/>
        <source>&lt;/b&gt; and renamed: </source>
        <translation>&lt;/b&gt; y renombrado: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2164"/>
        <source>Copied to: &lt;b&gt;</source>
        <translation>Copiado a: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2175"/>
        <source>&lt;br /&gt;Selected file(s) in: &lt;b&gt;</source>
        <translation>&lt;br /&gt;Archivo(s) seleccionado(s) en: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="477"/>
        <location filename="mainwindow.cpp" line="2310"/>
        <source>Spanish</source>
        <translation>Español</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2312"/>
        <source>Choose the language that will be used by EXIF Rename</source>
        <translation>Eija el idioma que usará EXIF ReName</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2326"/>
        <source>You must restart the application to the Russian language settings to take effect.</source>
        <translation>Debe reiniciar el programa para aplicar el idioma Ruso.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2334"/>
        <source>You must restart the application to the Italien language settings to take effect.</source>
        <translation>Debe reiniciar el programa para aplicar el idioma Italiano.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2342"/>
        <source>You must restart the application to the Swedish language settings to take effect.</source>
        <translation>Debe reiniciar el programa para aplicar el idioma Sueco.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2349"/>
        <location filename="mainwindow.cpp" line="2356"/>
        <source>You must restart the application to the Spanish language settings to take effect.</source>
        <translation>Debe reiniciar el programa para aplicar el idioma Español.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2416"/>
        <source>All files</source>
        <translation>Todos los archivos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2452"/>
        <source>Changes the EXIF information for these files</source>
        <translation>Cambia la información EXIF de estos archivos</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2461"/>
        <source> ...selected</source>
        <translation>...seleccionado</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2506"/>
        <source>MS-DOS-based version of Windows.</source>
        <translation>Versión de Windows basada en MS-DOS.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2509"/>
        <source>NT-based version of Windows.</source>
        <translation>Versión de Windows basada en NT.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2512"/>
        <source>CE-based version of Windows.</source>
        <translation>Versión de Windows basada en CE.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2515"/>
        <source>Windows 3.1 with Win 32s operating system.</source>
        <translation>Sistema Operativo Windows 3.1.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2518"/>
        <source>Windows 95 operating system.</source>
        <translation>Sistema Operativo Windows 95.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2521"/>
        <source>Windows 98 operating system.</source>
        <translation>Sistema Operativo Windows 98.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2524"/>
        <source>Windows Me operating system.</source>
        <translation>Sistema Operativo Windows ME.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2527"/>
        <source>Windows operating system version 4.0, corresponds to Windows NT.</source>
        <translation>Sistema operativo Windows version 4.0, corresponde a Windows NT.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2530"/>
        <source>Windows operating system version 5.0, corresponds to Windows 2000.</source>
        <translation>Sistema operativo Windows version 5.0, corresponde a Windows 2000.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2533"/>
        <source>Windows operating system version 5.1, corresponds to Windows XP.</source>
        <translation>Sistema operativo Windows version 5.1, corresponde a Windows  XP.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2536"/>
        <source>Windows operating system version 5.2, corresponds to Windows Server 2003, Windows Server 2003 R2, Windows Home Server, and Windows XP Professional x64 Edition.</source>
        <translation>Sistema operativo Windows version 5.2, corresponde a Windows Server 2003, Windows Server 2003 R2, Windows Home Server y Windows XP Professional edición X64.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2539"/>
        <source>Windows operating system version 6.0, corresponds to Windows Vista and Windows Server 2008.</source>
        <translation>Sistema operativo Windows version 6.0, corresponde a Windows Vista y Windows Server 2008.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2542"/>
        <source>Windows operating system version 6.1, corresponds to Windows 7 and Windows Server 2008 R2.</source>
        <translation>Sistema operativo Windows version 6.1, corresponde a Windows 7 y Windows Server 2008 R2.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2545"/>
        <source>Windows operating system version 6.2, corresponds to Windows 8</source>
        <oldsource>Windows operating system version 6.2, corresponds to Windows 8.</oldsource>
        <translation>Sistema operativo Windows version 6.2, corresponde a Windows 8</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2548"/>
        <source>Operating system version 6.3, corresponds to Windows 8.1</source>
        <translation>Sistema operativo Windows version 6.3, corresponde a Windows 8.1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2551"/>
        <source>Operating system version 10.0, corresponds to Windows 10</source>
        <translation>Sistema operativo Windows version 10.0, corresponde a Windows 10</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2554"/>
        <source>Windows CE operating system.</source>
        <translation>Sistema operativo Windows CE.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2557"/>
        <source>Windows CE .NET operating system.</source>
        <translation>Sistema operativo Windows CE.NET.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2560"/>
        <source>Windows CE 5.x operating system.</source>
        <translation>Sistema operativo Windows CE 5.x.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2563"/>
        <source>Windows CE 6.x operating system.</source>
        <translation>Sistema operativo Windows CE 6.x.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2566"/>
        <source>Unknown Windows operating system</source>
        <translation>Sistema operativo Windows Unone</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2575"/>
        <source>Architecture instruction set: </source>
        <translation>Set de instrucciones de arquitectura:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2586"/>
        <source> was created </source>
        <translation> fue creado </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2596"/>
        <location filename="mainwindow.cpp" line="2598"/>
        <location filename="mainwindow.cpp" line="2633"/>
        <source>Compiled by</source>
        <translation>Compilado por</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2629"/>
        <source>Unknown version</source>
        <translation>Versión desconocida</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2638"/>
        <source>Unknown compiler.</source>
        <translation>Compilador desconocido.</translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <location filename="checkupdate.cpp" line="46"/>
        <source>Cancel</source>
        <translation>Cancelar</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="50"/>
        <source>Check for updates on startup</source>
        <translation>Verificar actualizaciones an el arranque</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="62"/>
        <source>Contacting server...</source>
        <translation>Contactando server...</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="134"/>
        <source>Error message: Host found but
the requested version information was not found.
Please try again later</source>
        <translation>Mensaje de error: Host encontrado, pero no la versión solicitada. Por favor, intente nuevamente</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="135"/>
        <location filename="checkupdate.cpp" line="149"/>
        <location filename="checkupdate.cpp" line="190"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="148"/>
        <source>Error message: </source>
        <translation>Mensaje de error:</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="148"/>
        <source>
Please try again later</source>
        <translation>Por favor, intente nuevamente</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source>New version available: </source>
        <translation>Nueva versión disponible:</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source>Download it from</source>
        <translation>Descargar de</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source> homepage</source>
        <translation>Página principal</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="181"/>
        <source>Your current version is the latest available: </source>
        <translation>La versión instalada es la última disponible:</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="181"/>
        <source>Thank you for using</source>
        <translation>Gracias por usar</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>New version available!</source>
        <translation>Nueva versión disponible!</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>Version </source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>Please visit</source>
        <translation>Por favor, visite</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>homepage</source>
        <translation>Página principal</translation>
    </message>
</context>
</TS>
