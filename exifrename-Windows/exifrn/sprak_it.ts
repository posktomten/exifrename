<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="it_IT" sourcelanguage="en">
<context>
    <name>Exif_w</name>
    <message>
        <location filename="exif_w.ui" line="29"/>
        <source>Dialog</source>
        <translation>Finestra</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="65"/>
        <source>Date and time</source>
        <translation>Data e ora</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="78"/>
        <source>Change the time stamp on</source>
        <translation>Cambia il time stamp in</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="91"/>
        <source>TextLabel</source>
        <translation>Etichetta</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="104"/>
        <source>Year-Month-Day Hour-Minute-Second</source>
        <translation>Anno Mese Giorno Ora Minuti Secondi</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="117"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="130"/>
        <source>Cancel</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="143"/>
        <source>Current Time</source>
        <translation>Ora corrente</translation>
    </message>
    <message>
        <location filename="exif_w.cpp" line="72"/>
        <source>Incorrect date/time format, unable to write to file.
You must write in this way: </source>
        <translation>Data/ora non corrette, impossibile scrivere nel file. Devi scriverli in questo modo:</translation>
    </message>
    <message>
        <location filename="exif_w.cpp" line="101"/>
        <source>Unable to save EXIF data</source>
        <translation>Impossibile salvare i dati EXIF</translation>
    </message>
</context>
<context>
    <name>FilePattern</name>
    <message>
        <location filename="filepattern.ui" line="58"/>
        <source>file name</source>
        <translation>Nome file</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="71"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The numbers in the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;I numeri nel nome file sono separati da ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="79"/>
        <location filename="filepattern.ui" line="116"/>
        <location filename="filepattern.ui" line="265"/>
        <source>none</source>
        <translation>nessuno</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="84"/>
        <location filename="filepattern.ui" line="121"/>
        <location filename="filepattern.ui" line="270"/>
        <source>space</source>
        <translation>spazio</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="89"/>
        <location filename="filepattern.ui" line="126"/>
        <location filename="filepattern.ui" line="275"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="94"/>
        <location filename="filepattern.ui" line="131"/>
        <location filename="filepattern.ui" line="280"/>
        <source>_</source>
        <translation>_</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="108"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Date and time component part of the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Data e temponel nome file sono  separati da...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="148"/>
        <source>File name figures, separated by</source>
        <translation>Nome file, separato da</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="167"/>
        <source>Date and time, separated by</source>
        <translation>Data e ora, separata da</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="186"/>
        <source>File extension (.jpg or .JPG)</source>
        <translation>Estensione (.jpg o .JPG)</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="199"/>
        <source>CAPITAL letter</source>
        <translation>Lettere MAIUSCOLE</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="215"/>
        <source>lowercase letter</source>
        <translation>Lettere minuscole</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="231"/>
        <source>Save</source>
        <translation>Salva</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="244"/>
        <source>Cancel</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="257"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The figures in the folder name are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;le immagini nella cartella sono separate da ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="294"/>
        <source>Folder name figures, separated by</source>
        <translation>Nome cartella, separata da</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="313"/>
        <source>File name:</source>
        <translation>Nome file:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="332"/>
        <source>Folder name:</source>
        <translation>Nome cartella:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="351"/>
        <source>folder name</source>
        <translation>nome cartella</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="367"/>
        <source>Only time in file name</source>
        <translation>Solo l&apos; ora nel nome file</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="801"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Extensions of files you want to be able to open, separated by cemikolon. S&lt;span style=&quot; color:#000000;&quot;&gt;uch as jpg and tif.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000&quot;&gt;Example:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;jpg;tif;cr2&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;L&apos; estensione dei file che vuoi aprire , separate da ; . S&lt;span style=&quot; color:#000000;&quot;&gt;come jpg e tif.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000&quot;&gt;Example:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;jpg;tif;cr2&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="834"/>
        <source>File extension:</source>
        <translation>Estensione:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="853"/>
        <source>*.</source>
        <translation>*.</translation>
    </message>
    <message>
        <location filename="filepattern.cpp" line="97"/>
        <location filename="filepattern.cpp" line="321"/>
        <source>Extension: .</source>
        <translation>Estensione: .</translation>
    </message>
    <message>
        <location filename="filepattern.cpp" line="97"/>
        <location filename="filepattern.cpp" line="321"/>
        <source> or .</source>
        <translation> o .</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="help.cpp" line="55"/>
        <source>Close</source>
        <translation>Chiudi</translation>
    </message>
    <message>
        <location filename="help.cpp" line="148"/>
        <source>Error reading
</source>
        <translation>Errore lettura</translation>
    </message>
    <message>
        <location filename="help.cpp" line="148"/>
        <source>
info file missing!</source>
        <translation>
Info mancanti !</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="120"/>
        <source>Copy (do not rename)</source>
        <translation>Copia (non rimoninare)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="122"/>
        <source>Copy to...</source>
        <translation>Copia in...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="148"/>
        <source>Preserve original file name</source>
        <translation>Mantieni i nomi originali</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="150"/>
        <location filename="mainwindow.cpp" line="183"/>
        <location filename="mainwindow.cpp" line="392"/>
        <location filename="mainwindow.cpp" line="405"/>
        <source>Before date/time</source>
        <translation>Pirma di data/ora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="151"/>
        <location filename="mainwindow.cpp" line="184"/>
        <location filename="mainwindow.cpp" line="398"/>
        <location filename="mainwindow.cpp" line="411"/>
        <source>After date/time</source>
        <translation>Dopo di data/ora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="181"/>
        <source>Insert new file name</source>
        <translation>Inserisci un nuovo nome</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="205"/>
        <source>Go!</source>
        <translation>Vai !</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="224"/>
        <source>Clear log</source>
        <translation>Cancella Log</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="226"/>
        <source>Save log</source>
        <translation>Salva Log</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="343"/>
        <source>&amp;Open...</source>
        <translation>&amp;Apri...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="344"/>
        <source>Ctrl+O</source>
        <translation>Ctrl+O</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="345"/>
        <source>Open jpeg image(s)</source>
        <translation>Apri immagini jpeg</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="349"/>
        <source>&amp;Rename</source>
        <translation>&amp;Rinomina</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="350"/>
        <source>Ctrl+R</source>
        <translation>Ctrl+R
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="351"/>
        <source>Rename original file(s)</source>
        <translation>Rinomina gli originali</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="356"/>
        <source>&amp;Copy to...</source>
        <translation>&amp;Copia in...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="357"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="358"/>
        <source>Copy original file(s) with new name to folder...</source>
        <translation>Copia gli originali con un nuovo nome nella cartella...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="365"/>
        <source>E&amp;xit</source>
        <translation>E&amp;sci</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="366"/>
        <source>Ctrl+Q</source>
        <translation>Ctrl+Q</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="367"/>
        <source>Exit application</source>
        <translation>Esci</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="377"/>
        <source>Create folders (yyyy_mm_dd)</source>
        <translation>Crea cartella (aaaa_mm_gg)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="379"/>
        <source>Create folders according to file creation date and copy the files into these folders</source>
        <translation>Crea cartelle in base alla data di creazione del file e copiaceli dentro</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="384"/>
        <source>No name exept date/time</source>
        <translation>Nessun nome eccetto data/ora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="386"/>
        <source>File name is only date/time</source>
        <translation>Nome file solo data/ora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="394"/>
        <source>Preserve original file name before date/time</source>
        <translation>Preserva nome originale prima data/ora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="400"/>
        <source>Preserve original file name after date/time</source>
        <translation>Preserva nome originale dopo data/ora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>Insert new file name before date/time</source>
        <translation>Inserisci nuovo nome prima di data/ora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Insert new file name after date/time</source>
        <translation>Inserisci nuovo nome dopo di data/ora</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="418"/>
        <location filename="mainwindow.cpp" line="419"/>
        <source>Edit date and time...</source>
        <translation>Cambia data e ora...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="425"/>
        <source>Help...</source>
        <translation>Aiuto...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="426"/>
        <source>Help for application</source>
        <translation>Aiuto per il programma</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="430"/>
        <source>About...</source>
        <translation>A proposito...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="431"/>
        <source>About application</source>
        <translation>A proposito del programma</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="435"/>
        <source>About Qt...</source>
        <translation>A proposito di Qt...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="436"/>
        <source>About Qt C++ Application Development Framework</source>
        <translation>A proposito di Qt C++ e applicazione Framework</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="440"/>
        <source>Check for updates...</source>
        <translation>Cerca aggiornamenti...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="441"/>
        <source>Check for new versions of EXIF ReName</source>
        <translation>Cerca una nuova versione di Exif ReName</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="447"/>
        <location filename="mainwindow.cpp" line="2306"/>
        <source>English</source>
        <translation>Inglese</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="448"/>
        <source>English language</source>
        <translation>Lingua inglese</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2307"/>
        <source>Italiano</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="454"/>
        <source>Italian language</source>
        <translation>italiano lingua</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="453"/>
        <source>Italian</source>
        <translation>Italiano</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="459"/>
        <location filename="mainwindow.cpp" line="2311"/>
        <source>German</source>
        <translation>Tedesco</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="460"/>
        <source>German language</source>
        <translation>Lingua tedesca</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="465"/>
        <location filename="mainwindow.cpp" line="2308"/>
        <source>Russian</source>
        <translation>Russo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="466"/>
        <source>Russian language</source>
        <translation>Lingua russa</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="471"/>
        <location filename="mainwindow.cpp" line="2309"/>
        <source>Swedish</source>
        <oldsource>Svenska</oldsource>
        <translation>Svedese</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="472"/>
        <source>Swedish language</source>
        <translation>Lingua svedese</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="478"/>
        <source>Spanish language</source>
        <translation>
lingua Spagnola</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="484"/>
        <source>Delete personal settings</source>
        <translation>Cancella i settaggi personali</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="485"/>
        <source>Remove your personal settings</source>
        <translation>Rimuovi i tuoi settaggi personali</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="489"/>
        <location filename="mainwindow.cpp" line="490"/>
        <source>Specify file name pattern...</source>
        <translation>Specifica il pattern da usare nei nomi...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="498"/>
        <source>Icons only</source>
        <translation>Solo icone</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="499"/>
        <source>Display only icons in the toolbar</source>
        <translation>Mostra solo le icone nella barra degli strumenti</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="502"/>
        <source>Text only</source>
        <translation>Solo testo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="503"/>
        <source>Display only text in the toolbar</source>
        <translation>Mostra solo testo nella barra degli strumenti</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="506"/>
        <source>Text alongside icons</source>
        <translation>Testo accanto ad icone</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="507"/>
        <source>Display text alongside icons in the toolbar</source>
        <translation>Mostra testo accanto ad icone nella barra degli strumenti</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="511"/>
        <source>Text under icons</source>
        <translation>Testo sotto icone</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="512"/>
        <source>Display text under icons in the toolbar</source>
        <translation>Mostra testo sotto ad icone nella barra degli strumenti</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="515"/>
        <source>No toolbar</source>
        <translation>Nessuna barra degli strumenti</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="516"/>
        <source>No toolbar will be displayed</source>
        <translation>Nessuna barra degli strumenti sara&apos; mostrata</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="528"/>
        <source>&amp;File</source>
        <translation>&amp;File</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="536"/>
        <source>&amp;Edit</source>
        <translation>&amp;Modifica</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="542"/>
        <source>&amp;Preserve original file name</source>
        <translation>&amp;Preserva nome file originale</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="546"/>
        <source>&amp;Insert new file name</source>
        <translation>&amp;Aggiungi nuovo nome</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="553"/>
        <source>&amp;View</source>
        <translation>&amp;Visualizza</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="555"/>
        <location filename="mainwindow.cpp" line="645"/>
        <source>Toolbar</source>
        <translation>Barra degli strumenti</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="565"/>
        <source>&amp;Language</source>
        <translation>&amp;Lingua</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="576"/>
        <source>&amp;Tools</source>
        <translation>&amp;Strumenti</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="581"/>
        <source>&amp;Help</source>
        <translation>&amp;Aiuto</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="596"/>
        <source>Do you want to remove your personal settings?
This is an appropriate action if you like to uninstall this application.
</source>
        <translation>Vuoi rimuovere i tuoi settaggi personali?
E&apos; utile farlo se stai disinstallando il programma.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="596"/>
        <source> will exit after your personal data is deleted.</source>
        <translation>uscira&apos; dopo aver cancellato i dati personali.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="597"/>
        <location filename="mainwindow.cpp" line="1643"/>
        <location filename="mainwindow.cpp" line="1684"/>
        <location filename="mainwindow.cpp" line="1723"/>
        <location filename="mainwindow.cpp" line="1765"/>
        <location filename="mainwindow.cpp" line="1807"/>
        <location filename="mainwindow.cpp" line="1848"/>
        <location filename="mainwindow.cpp" line="2079"/>
        <source>Yes</source>
        <translation>Si</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="598"/>
        <location filename="mainwindow.cpp" line="1644"/>
        <location filename="mainwindow.cpp" line="1685"/>
        <location filename="mainwindow.cpp" line="1724"/>
        <location filename="mainwindow.cpp" line="1766"/>
        <location filename="mainwindow.cpp" line="1808"/>
        <location filename="mainwindow.cpp" line="1849"/>
        <location filename="mainwindow.cpp" line="2080"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="606"/>
        <source>Your personal settings are deleted and
 </source>
        <translation>I tuoi dati personali saranno cancellati e </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="606"/>
        <source> will exit</source>
        <translation>uscira&apos;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="612"/>
        <source>Your personal settings cannot be found.
New settings will be saved then you restart the program.</source>
        <translation>I tuoi settaggi personali non sono stati trovati.
Nuovi settaggi saranno salvati al riavvio del programma.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="643"/>
        <source>File</source>
        <translation>File</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="663"/>
        <source>Create folders (yyyy</source>
        <translation>Crea cartella (aaaa</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="663"/>
        <source>mm</source>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="663"/>
        <source>dd)</source>
        <translation>gg)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="682"/>
        <location filename="mainwindow.cpp" line="2385"/>
        <source>Open File</source>
        <translation>Apri file</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="725"/>
        <location filename="mainwindow.cpp" line="2412"/>
        <source>Image files</source>
        <translation>File immagini</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="752"/>
        <location filename="mainwindow.cpp" line="2440"/>
        <source>You do not have enough permissions to read
</source>
        <translation>Non hai i permessi per leggere</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="775"/>
        <location filename="mainwindow.cpp" line="776"/>
        <source> File(s) selected</source>
        <translation>file selezionati</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="785"/>
        <location filename="mainwindow.cpp" line="1933"/>
        <location filename="mainwindow.cpp" line="1978"/>
        <source>The original file will be renamed</source>
        <translation>I file originali saranno rinominati</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="931"/>
        <source>Copy pictures to folder</source>
        <translation>Copia immagini nella cartella</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="953"/>
        <source>You have not enough permissions
to write to </source>
        <translation>Non hai i permessi per scrivere in</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="959"/>
        <location filename="mainwindow.cpp" line="1084"/>
        <location filename="mainwindow.cpp" line="1945"/>
        <location filename="mainwindow.cpp" line="1947"/>
        <location filename="mainwindow.cpp" line="1968"/>
        <location filename="mainwindow.cpp" line="1970"/>
        <source>Copy to: </source>
        <translation>Copia in: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1046"/>
        <source>rename</source>
        <translation>Rinomina</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1048"/>
        <source>copy</source>
        <translation>copia</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1054"/>
        <source>You have not selected any file(s).
Please select file(s) to </source>
        <translation>Non hai selezionato nessun file.
Selezionane per </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1080"/>
        <source>Error in destination path
Please select another output directory!</source>
        <translation>Errore nella destinazione
Seleziona un&apos; altra cartella destinazione!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1168"/>
        <location filename="mainwindow.cpp" line="1231"/>
        <location filename="mainwindow.cpp" line="1282"/>
        <location filename="mainwindow.cpp" line="1338"/>
        <location filename="mainwindow.cpp" line="1397"/>
        <location filename="mainwindow.cpp" line="1456"/>
        <source>Unable to create directory 
</source>
        <translation>Impossibile creare la cartella </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1168"/>
        <location filename="mainwindow.cpp" line="1231"/>
        <location filename="mainwindow.cpp" line="1282"/>
        <location filename="mainwindow.cpp" line="1338"/>
        <location filename="mainwindow.cpp" line="1397"/>
        <location filename="mainwindow.cpp" line="1456"/>
        <source>
check your permisions</source>
        <translation>controlla i permessi amministratore</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1190"/>
        <location filename="mainwindow.cpp" line="1304"/>
        <location filename="mainwindow.cpp" line="1418"/>
        <source>Unable to delete:
</source>
        <translation>Impossibile cancellare:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1366"/>
        <source>Indicate a new file name, please!</source>
        <translation>Indica un nuovo nome file, grazie!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1481"/>
        <source>Done! </source>
        <translation>Fatto!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1481"/>
        <source> of </source>
        <translation>di</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1481"/>
        <source>file(s) )</source>
        <translation>file</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1509"/>
        <source>/exif_rename_log.txt</source>
        <translation>/exif_log_rinominati.txt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1512"/>
        <source>Save log as...</source>
        <translation>Salva il log come...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1519"/>
        <source>Text files (*.txt)</source>
        <translation>File di testo(*.txt)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1519"/>
        <source>Any files (*)</source>
        <translation>Tutti i file (*)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1538"/>
        <source>Error writing to
</source>
        <translation>Errore scrivendo in</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1538"/>
        <source>
check your permissions</source>
        <translation>controlla i permessi amministratore</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1543"/>
        <source>*** Log saved: </source>
        <translation>*** Log salvato: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1549"/>
        <source>Log created by </source>
        <translation>Log creato da </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1551"/>
        <source>Copyright Ingemar Ceicer 2007 - </source>
        <translation>Copyright Ingemar Ceicer 2007 - </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1609"/>
        <location filename="mainwindow.cpp" line="2075"/>
        <source>Licensierat under GNU General Public License This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version </source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1609"/>
        <location filename="mainwindow.cpp" line="2075"/>
        <source> of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1612"/>
        <location filename="mainwindow.cpp" line="2074"/>
        <source>&lt;br&gt;&lt;font coor=&quot;green&quot; size=&quot;3&quot;&gt;&lt;b&gt;Copyright &amp;copy; 2007 - </source>
        <translation>&lt;br&gt;&lt;font color=&quot;green&quot; size=&quot;3&quot;&gt;Copyright &amp;copy; 2007 - </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1612"/>
        <location filename="mainwindow.cpp" line="2074"/>
        <source>homepage</source>
        <translation>homepace</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1613"/>
        <source>About </source>
        <translation>A proposito</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1613"/>
        <location filename="mainwindow.cpp" line="2078"/>
        <source>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</source>
        <translation>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1614"/>
        <source>A small application for renaming pictures. EXIF ReName uses exif data from the picture file.&lt;br&gt;&lt;br&gt;</source>
        <translation>Un&apos; applicazione per rinominare le immagini. Exif ReNamer usa i dati exif contenuti nelle foto &lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1614"/>
        <source>Thank you Ivan Dolgov for the translation into Russian.</source>
        <translation>Grazie a Ivan Dolgovi per la traduzione in russo.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1614"/>
        <source>Thank you Roberto Nerozzi for the translation into Italien.</source>
        <translation>Grazie a Roberto Nerozzii per la traduzione in italiano.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1614"/>
        <source>Thank you Cristian Sosa Noe for the translation into Spanish.</source>
        <translation>Grazie a Cristian Sosa Noe per la traduzione in Spagnolo.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1614"/>
        <source>Thank you Ben Weis for the translation into German.</source>
        <translation>Grazie a Ben Weis per la traduzione in Tedesco.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1642"/>
        <location filename="mainwindow.cpp" line="1683"/>
        <location filename="mainwindow.cpp" line="1722"/>
        <location filename="mainwindow.cpp" line="1764"/>
        <location filename="mainwindow.cpp" line="1806"/>
        <location filename="mainwindow.cpp" line="1847"/>
        <source>The application needs to restart in order
to initialise the now language settings.

Do you want to close
the application now?</source>
        <translation>Bisogna riavviare per cambiare lingua.

Vuoi chiudere il programma ?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1644"/>
        <location filename="mainwindow.cpp" line="1685"/>
        <location filename="mainwindow.cpp" line="1724"/>
        <location filename="mainwindow.cpp" line="1766"/>
        <location filename="mainwindow.cpp" line="1808"/>
        <location filename="mainwindow.cpp" line="1849"/>
        <source>Cancel</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1914"/>
        <source>Ready for action!</source>
        <translation>Pronto a partire!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2078"/>
        <source>&lt;br&gt;A small application for renaming pictures. EXIF ReName uses exif data from the picture file.&lt;br&gt;&lt;br&gt;</source>
        <translation>&lt;br&gt;Un&apos; applicazione per rinominare le immagini. Exif ReNamer usa i dati exif contenuti nelle foto &lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2078"/>
        <source>&lt;br&gt;&lt;br&gt;&lt;font color=&quot;#FF0000&quot; size=&quot;4&quot;&gt;Do you accept the license?&lt;/font&gt;</source>
        <translation>&lt;br&gt;&lt;br&gt;&lt;font color=&quot;#FF0000&quot; size=&quot;4&quot;&gt;Accetti la licenza?&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2116"/>
        <location filename="mainwindow.cpp" line="2124"/>
        <source> is updatet to </source>
        <translation>e&apos; aggiornato a </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2160"/>
        <source>Copied to new folder in: &lt;b&gt;</source>
        <translation>Copiato nella cartella: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2160"/>
        <location filename="mainwindow.cpp" line="2164"/>
        <source>&lt;/b&gt; and renamed: </source>
        <translation>&lt;/b&gt; e rinominato:</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2164"/>
        <source>Copied to: &lt;b&gt;</source>
        <translation>Copiato in: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2175"/>
        <source>&lt;br /&gt;Selected file(s) in: &lt;b&gt;</source>
        <translation>&lt;br /&gt;Seleziona file in: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="477"/>
        <location filename="mainwindow.cpp" line="2310"/>
        <source>Spanish</source>
        <translation>Spagnolo</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2312"/>
        <source>Choose the language that will be used by EXIF Rename</source>
        <translation>Seleziona la lingua da uare con EXIF ReName</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2326"/>
        <source>You must restart the application to the Russian language settings to take effect.</source>
        <translation>Devi far ripartire il programma in russo perche&apos; i cambiamenti abbiano effetto.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2334"/>
        <source>You must restart the application to the Italien language settings to take effect.</source>
        <translation>Devi far ripartire il programma in italiano perche&apos; i cambiamenti abbiano effetto.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2342"/>
        <source>You must restart the application to the Swedish language settings to take effect.</source>
        <translation>Devi far ripartire il programma in svedese perche&apos; i cambiamenti abbiano effetto.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2349"/>
        <location filename="mainwindow.cpp" line="2356"/>
        <source>You must restart the application to the Spanish language settings to take effect.</source>
        <translation>Devi far ripartire il programma in spagnolo perche&apos; i cambiamenti abbiano effetto.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2416"/>
        <source>All files</source>
        <translation>Tutti i file</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2452"/>
        <source>Changes the EXIF information for these files</source>
        <translation>Camvia i dati EXIF per questi file</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2461"/>
        <source> ...selected</source>
        <translation> ...selezionati</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2506"/>
        <source>MS-DOS-based version of Windows.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2509"/>
        <source>NT-based version of Windows.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2512"/>
        <source>CE-based version of Windows.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2515"/>
        <source>Windows 3.1 with Win 32s operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2518"/>
        <source>Windows 95 operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2521"/>
        <source>Windows 98 operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2524"/>
        <source>Windows Me operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2527"/>
        <source>Windows operating system version 4.0, corresponds to Windows NT.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2530"/>
        <source>Windows operating system version 5.0, corresponds to Windows 2000.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2533"/>
        <source>Windows operating system version 5.1, corresponds to Windows XP.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2536"/>
        <source>Windows operating system version 5.2, corresponds to Windows Server 2003, Windows Server 2003 R2, Windows Home Server, and Windows XP Professional x64 Edition.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2539"/>
        <source>Windows operating system version 6.0, corresponds to Windows Vista and Windows Server 2008.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2542"/>
        <source>Windows operating system version 6.1, corresponds to Windows 7 and Windows Server 2008 R2.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2545"/>
        <source>Windows operating system version 6.2, corresponds to Windows 8</source>
        <oldsource>Windows operating system version 6.2, corresponds to Windows 8.</oldsource>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2548"/>
        <source>Operating system version 6.3, corresponds to Windows 8.1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2551"/>
        <source>Operating system version 10.0, corresponds to Windows 10</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2554"/>
        <source>Windows CE operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2557"/>
        <source>Windows CE .NET operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2560"/>
        <source>Windows CE 5.x operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2563"/>
        <source>Windows CE 6.x operating system.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2566"/>
        <source>Unknown Windows operating system</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2575"/>
        <source>Architecture instruction set: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2586"/>
        <source> was created </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2596"/>
        <location filename="mainwindow.cpp" line="2598"/>
        <location filename="mainwindow.cpp" line="2633"/>
        <source>Compiled by</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2629"/>
        <source>Unknown version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2638"/>
        <source>Unknown compiler.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <location filename="checkupdate.cpp" line="46"/>
        <source>Cancel</source>
        <translation>Cancella</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="50"/>
        <source>Check for updates on startup</source>
        <translation>Cerca aggiornamenti all&apos; avvio</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="62"/>
        <source>Contacting server...</source>
        <translation>Sto chiacchierando col server...</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="134"/>
        <source>Error message: Host found but
the requested version information was not found.
Please try again later</source>
        <translation>Errore: Server presente ma
la nuova versione non ancora rilasciata.
Prova piu&apos; tardi</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="135"/>
        <location filename="checkupdate.cpp" line="149"/>
        <location filename="checkupdate.cpp" line="190"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="148"/>
        <source>Error message: </source>
        <translation>Errore:</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="148"/>
        <source>
Please try again later</source>
        <translation>
Prova piu&apos; tardi</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source>New version available: </source>
        <translation>Nuova versione disponibile: </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source>Download it from</source>
        <translation>Scaricando da</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source> homepage</source>
        <translation>Homepage</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="181"/>
        <source>Your current version is the latest available: </source>
        <translation>La tua versione e&apos; l&apos; ultima rilasciata: </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="181"/>
        <source>Thank you for using</source>
        <translation>Grazie per aver usato</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>New version available!</source>
        <translation>Nuova versione disponibile!</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>Version </source>
        <translation>Versione</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>Please visit</source>
        <translation>Per piacere visita </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>homepage</source>
        <translation>Hompage</translation>
    </message>
</context>
</TS>
