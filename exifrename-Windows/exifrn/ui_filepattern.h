/********************************************************************************
** Form generated from reading UI file 'filepattern.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_FILEPATTERN_H
#define UI_FILEPATTERN_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>

QT_BEGIN_NAMESPACE

class Ui_FilePattern
{
public:
    QLabel *lblExample;
    QComboBox *cbSeparator;
    QComboBox *cbDateTimeSeparator;
    QLabel *lblSep;
    QLabel *lblSepDateTime;
    QLabel *lblExtension;
    QRadioButton *raStor;
    QRadioButton *raLiten;
    QPushButton *pbOk;
    QPushButton *pbCancel;
    QComboBox *cbFolderSeparator;
    QLabel *lblSepFolder;
    QLabel *lblVexample;
    QLabel *lblVfolderExample;
    QLabel *lblFolderExample;
    QCheckBox *chOnlyTime;
    QLineEdit *leExtension;
    QLabel *lblExtension_2;
    QLabel *lblVisaextension;

    void setupUi(QDialog *FilePattern)
    {
        if (FilePattern->objectName().isEmpty())
            FilePattern->setObjectName(QString::fromUtf8("FilePattern"));
        FilePattern->setWindowModality(Qt::ApplicationModal);
        FilePattern->resize(487, 320);
        FilePattern->setMinimumSize(QSize(487, 320));
        FilePattern->setMaximumSize(QSize(487, 320));
        FilePattern->setBaseSize(QSize(487, 320));
        QFont font;
        font.setFamily(QString::fromUtf8("MS Shell Dlg 2"));
        FilePattern->setFont(font);
        FilePattern->setContextMenuPolicy(Qt::DefaultContextMenu);
        FilePattern->setModal(true);
        lblExample = new QLabel(FilePattern);
        lblExample->setObjectName(QString::fromUtf8("lblExample"));
        lblExample->setGeometry(QRect(13, 244, 950, 20));
        cbSeparator = new QComboBox(FilePattern);
        cbSeparator->setObjectName(QString::fromUtf8("cbSeparator"));
        cbSeparator->setGeometry(QRect(180, 60, 121, 23));
        cbDateTimeSeparator = new QComboBox(FilePattern);
        cbDateTimeSeparator->setObjectName(QString::fromUtf8("cbDateTimeSeparator"));
        cbDateTimeSeparator->setGeometry(QRect(350, 60, 121, 23));
        lblSep = new QLabel(FilePattern);
        lblSep->setObjectName(QString::fromUtf8("lblSep"));
        lblSep->setGeometry(QRect(180, 10, 141, 51));
        lblSep->setTextFormat(Qt::PlainText);
        lblSep->setWordWrap(true);
        lblSepDateTime = new QLabel(FilePattern);
        lblSepDateTime->setObjectName(QString::fromUtf8("lblSepDateTime"));
        lblSepDateTime->setGeometry(QRect(350, 10, 131, 51));
        lblSepDateTime->setTextFormat(Qt::PlainText);
        lblSepDateTime->setWordWrap(true);
        lblExtension = new QLabel(FilePattern);
        lblExtension->setObjectName(QString::fromUtf8("lblExtension"));
        lblExtension->setGeometry(QRect(10, 90, 940, 17));
        raStor = new QRadioButton(FilePattern);
        raStor->setObjectName(QString::fromUtf8("raStor"));
        raStor->setGeometry(QRect(10, 110, 431, 21));
        raStor->setAutoExclusive(true);
        raLiten = new QRadioButton(FilePattern);
        raLiten->setObjectName(QString::fromUtf8("raLiten"));
        raLiten->setGeometry(QRect(10, 130, 201, 21));
        raLiten->setAutoExclusive(true);
        pbOk = new QPushButton(FilePattern);
        pbOk->setObjectName(QString::fromUtf8("pbOk"));
        pbOk->setGeometry(QRect(13, 274, 75, 25));
        pbCancel = new QPushButton(FilePattern);
        pbCancel->setObjectName(QString::fromUtf8("pbCancel"));
        pbCancel->setGeometry(QRect(103, 274, 75, 25));
        cbFolderSeparator = new QComboBox(FilePattern);
        cbFolderSeparator->setObjectName(QString::fromUtf8("cbFolderSeparator"));
        cbFolderSeparator->setGeometry(QRect(10, 60, 121, 23));
        lblSepFolder = new QLabel(FilePattern);
        lblSepFolder->setObjectName(QString::fromUtf8("lblSepFolder"));
        lblSepFolder->setGeometry(QRect(10, 10, 151, 51));
        lblSepFolder->setTextFormat(Qt::PlainText);
        lblSepFolder->setWordWrap(true);
        lblVexample = new QLabel(FilePattern);
        lblVexample->setObjectName(QString::fromUtf8("lblVexample"));
        lblVexample->setGeometry(QRect(13, 224, 151, 17));
        lblVexample->setTextFormat(Qt::PlainText);
        lblVexample->setScaledContents(false);
        lblVfolderExample = new QLabel(FilePattern);
        lblVfolderExample->setObjectName(QString::fromUtf8("lblVfolderExample"));
        lblVfolderExample->setGeometry(QRect(13, 174, 151, 17));
        lblVfolderExample->setTextFormat(Qt::PlainText);
        lblVfolderExample->setScaledContents(false);
        lblFolderExample = new QLabel(FilePattern);
        lblFolderExample->setObjectName(QString::fromUtf8("lblFolderExample"));
        lblFolderExample->setGeometry(QRect(13, 194, 331, 20));
        chOnlyTime = new QCheckBox(FilePattern);
        chOnlyTime->setObjectName(QString::fromUtf8("chOnlyTime"));
        chOnlyTime->setGeometry(QRect(220, 130, 251, 21));
        chOnlyTime->setCheckable(true);
        chOnlyTime->setTristate(false);
        leExtension = new QLineEdit(FilePattern);
        leExtension->setObjectName(QString::fromUtf8("leExtension"));
        leExtension->setGeometry(QRect(350, 280, 121, 20));
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(170, 0, 0, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        QBrush brush2(QColor(255, 0, 0, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Light, brush2);
        QBrush brush3(QColor(212, 0, 0, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush3);
        QBrush brush4(QColor(85, 0, 0, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush4);
        QBrush brush5(QColor(113, 0, 0, 255));
        brush5.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        QBrush brush6(QColor(255, 255, 255, 255));
        brush6.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush6);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush);
        QBrush brush7(QColor(212, 127, 127, 255));
        brush7.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush7);
        QBrush brush8(QColor(255, 255, 220, 255));
        brush8.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush6);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush7);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush5);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush6);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush8);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        leExtension->setPalette(palette);
        leExtension->setAutoFillBackground(true);
        leExtension->setMaxLength(40);
        leExtension->setFrame(false);
        lblExtension_2 = new QLabel(FilePattern);
        lblExtension_2->setObjectName(QString::fromUtf8("lblExtension_2"));
        lblExtension_2->setGeometry(QRect(183, 275, 141, 31));
        lblExtension_2->setTextFormat(Qt::PlainText);
        lblExtension_2->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);
        lblVisaextension = new QLabel(FilePattern);
        lblVisaextension->setObjectName(QString::fromUtf8("lblVisaextension"));
        lblVisaextension->setGeometry(QRect(330, 280, 16, 17));

        retranslateUi(FilePattern);

        QMetaObject::connectSlotsByName(FilePattern);
    } // setupUi

    void retranslateUi(QDialog *FilePattern)
    {
#ifndef QT_NO_WHATSTHIS
        FilePattern->setWhatsThis(QString());
#endif // QT_NO_WHATSTHIS
        lblExample->setText(QApplication::translate("FilePattern", "file name", 0, QApplication::UnicodeUTF8));
        cbSeparator->clear();
        cbSeparator->insertItems(0, QStringList()
         << QApplication::translate("FilePattern", "none", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilePattern", "space", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilePattern", "-", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilePattern", "_", 0, QApplication::UnicodeUTF8)
        );
#ifndef QT_NO_WHATSTHIS
        cbSeparator->setWhatsThis(QApplication::translate("FilePattern", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt;\">The numbers in the filename are separated by ...</span></p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        cbDateTimeSeparator->clear();
        cbDateTimeSeparator->insertItems(0, QStringList()
         << QApplication::translate("FilePattern", "none", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilePattern", "space", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilePattern", "-", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilePattern", "_", 0, QApplication::UnicodeUTF8)
        );
#ifndef QT_NO_WHATSTHIS
        cbDateTimeSeparator->setWhatsThis(QApplication::translate("FilePattern", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt;\">Date and time component part of the filename are separated by ...</span></p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
#ifndef QT_NO_WHATSTHIS
        lblSep->setWhatsThis(QString());
#endif // QT_NO_WHATSTHIS
        lblSep->setText(QApplication::translate("FilePattern", "File name figures, separated by", 0, QApplication::UnicodeUTF8));
        lblSepDateTime->setText(QApplication::translate("FilePattern", "Date and time, separated by", 0, QApplication::UnicodeUTF8));
        lblExtension->setText(QApplication::translate("FilePattern", "File extension (.jpg or .JPG)", 0, QApplication::UnicodeUTF8));
        raStor->setText(QApplication::translate("FilePattern", "CAPITAL letter", 0, QApplication::UnicodeUTF8));
        raLiten->setText(QApplication::translate("FilePattern", "lowercase letter", 0, QApplication::UnicodeUTF8));
        pbOk->setText(QApplication::translate("FilePattern", "Save", 0, QApplication::UnicodeUTF8));
        pbCancel->setText(QApplication::translate("FilePattern", "Cancel", 0, QApplication::UnicodeUTF8));
        cbFolderSeparator->clear();
        cbFolderSeparator->insertItems(0, QStringList()
         << QApplication::translate("FilePattern", "none", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilePattern", "space", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilePattern", "-", 0, QApplication::UnicodeUTF8)
         << QApplication::translate("FilePattern", "_", 0, QApplication::UnicodeUTF8)
        );
#ifndef QT_NO_WHATSTHIS
        cbFolderSeparator->setWhatsThis(QApplication::translate("FilePattern", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" font-size:10pt;\">The figures in the folder name are separated by ...</span></p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        lblSepFolder->setText(QApplication::translate("FilePattern", "Folder name figures, separated by", 0, QApplication::UnicodeUTF8));
        lblVexample->setText(QApplication::translate("FilePattern", "File name:", 0, QApplication::UnicodeUTF8));
        lblVfolderExample->setText(QApplication::translate("FilePattern", "Folder name:", 0, QApplication::UnicodeUTF8));
        lblFolderExample->setText(QApplication::translate("FilePattern", "folder name", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        chOnlyTime->setWhatsThis(QString());
#endif // QT_NO_WHATSTHIS
        chOnlyTime->setText(QApplication::translate("FilePattern", "Only time in file name", 0, QApplication::UnicodeUTF8));
#ifndef QT_NO_WHATSTHIS
        leExtension->setWhatsThis(QApplication::translate("FilePattern", "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.0//EN\" \"http://www.w3.org/TR/REC-html40/strict.dtd\">\n"
"<html><head><meta name=\"qrichtext\" content=\"1\" /><style type=\"text/css\">\n"
"p, li { white-space: pre-wrap; }\n"
"</style></head><body style=\" font-family:'MS Shell Dlg 2'; font-size:8.25pt; font-weight:400; font-style:normal;\">\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\">Extensions of files you want to be able to open, separated by cemikolon. S<span style=\" color:#000000;\">uch as jpg and tif.</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;\"></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" color:#000000\">Example:</span></p>\n"
"<p style=\" margin-top:0px; margin-bottom:0px; margin-left:0px; margin-ri"
                        "ght:0px; -qt-block-indent:0; text-indent:0px;\"><span style=\" color:#000000;\">jpg;tif;cr2</span></p>\n"
"<p style=\"-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;\"></p></body></html>", 0, QApplication::UnicodeUTF8));
#endif // QT_NO_WHATSTHIS
        leExtension->setInputMask(QString());
        lblExtension_2->setText(QApplication::translate("FilePattern", "File extension:", 0, QApplication::UnicodeUTF8));
        lblVisaextension->setText(QApplication::translate("FilePattern", "*.", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class FilePattern: public Ui_FilePattern {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_FILEPATTERN_H
