/*
    EXIF ReName
    Copyright (C) 2011-2013 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/



#include "filepattern.h"
#include "ui_filepattern.h"
#include "mainwindow.h"
#include <QMessageBox>
#include <QRegExpValidator>
#include <QPlastiqueStyle>

#include <string>
#include <sstream>
FilePattern::FilePattern(QWidget *parent) :
    QDialog(parent),
    m_ui(new Ui::FilePattern)
{

m_ui->setupUi(this);


setConfig();
connect( m_ui->cbSeparator, SIGNAL (currentIndexChanged(int) ), this, SLOT( display_value(int)));
connect( m_ui->cbDateTimeSeparator, SIGNAL (currentIndexChanged(int) ), this, SLOT( display_value(int)));
connect( m_ui->cbFolderSeparator, SIGNAL (currentIndexChanged(int) ), this, SLOT( display_folder_value(int)));
connect( m_ui->raStor, SIGNAL (toggled(bool) ), this, SLOT( display_value(bool)));
connect( m_ui->raLiten, SIGNAL (toggled(bool) ), this, SLOT( display_value(bool)));
connect( m_ui->chOnlyTime, SIGNAL (toggled(bool) ), this, SLOT( display_value(bool)));
connect( m_ui->leExtension, SIGNAL (textEdited(QString)), this, SLOT( display_value(QString)));
connect( m_ui->leExtension, SIGNAL (cursorPositionChanged(int,int)), this, SLOT( cursorChanged(int,int)));
connect( m_ui->pbCancel, SIGNAL (clicked() ), this, SLOT( close()));
connect( m_ui->pbOk, SIGNAL (clicked() ), this, SLOT( spara()));

QRegExp rx("[a-z0-9;\t\r\n\v\f]*");
QValidator *validator = new QRegExpValidator(rx, this);

m_ui->leExtension->setValidator(validator);


//m_ui->leExtension->setInputMask("*");
}

FilePattern::~FilePattern()
{


    delete m_ui;


}


void FilePattern::display_value(int i)
{
   // m_ui->lblExample->setText(format_date_time());


}

void FilePattern::display_value(QString s)
{

    if (s.at(s.size()-1) == ';')
        s.chop(1);

    s = s.prepend(" [ ");
    s = s.append(" ]");

    for (int i=0;i<s.size();i++)
    {
        if (s.at(i)==';')
        {
            s = s.replace(i,1," | ");

            i=i+3;
        }

    }


    m_ui->lblExample->setText(format_date_time());
    m_ui->lblExtension->setText(tr("Extension: .")+s.toUpper()+tr(" or .")+s.toLower());

    if (m_ui->raLiten->isChecked()==true)
        {

            m_ui->lblExample->setText(m_ui->lblExample->text().toLower());
        }
    if (m_ui->raStor->isChecked()==true)
        {
            m_ui->lblExample->setText(m_ui->lblExample->text().toUpper());
        }

}

void FilePattern::display_value(bool i)
{

    if (m_ui->raStor->isChecked()==true)
        m_ui->lblExample->setText(format_date_time().toUpper());
    if (m_ui->raLiten->isChecked()==true)
        m_ui->lblExample->setText(format_date_time().toLower());

}

void FilePattern::display_folder_value(int i)
{
    m_ui->lblFolderExample->setText(format_folder_date_time());

}


QString FilePattern::format_date_time()
{
QDateTime dateTime = QDateTime::currentDateTime();
QString sep = m_ui->cbSeparator->currentText();
QString dt_sep = m_ui->cbDateTimeSeparator->currentText();



if (sep == "none")
    sep = "";
if (sep == "space")
    sep = " ";

if (dt_sep == "none")
    dt_sep = "";
if (dt_sep == "space")
    dt_sep = " ";

QString extension = m_ui->leExtension->text().trimmed();

if (extension.at(extension.size()-1) == ';')
    extension.chop(1);

extension = extension.prepend(" [ ");
extension = extension.append(" ]");

for (int i=0;i<extension.size();i++)
{
    if (extension.at(i)==';')
    {
        extension = extension.replace(i,1," | ");

        i=i+3;
    }

}



QString returneras;

if (m_ui->chOnlyTime->isChecked() == false)
    returneras =  dateTime.toString("yyyy"+sep+"MM"+sep+"dd"+dt_sep+"hh"+sep+"mm"+sep+"ss."+extension);

if (m_ui->chOnlyTime->isChecked() == true)
    returneras =  dateTime.toString("hh"+sep+"mm"+sep+"ss."+extension);

return returneras;

}


QString FilePattern::format_folder_date_time()
{
QDateTime dateTime = QDateTime::currentDateTime();
QString folder_sep = m_ui->cbFolderSeparator->currentText();



if (folder_sep == "none")
    folder_sep = "";
if (folder_sep == "space")
    folder_sep = " ";

QString returneras;




 returneras =  dateTime.toString("yyyy"+folder_sep+"MM"+folder_sep+"dd");

return returneras;

}

void FilePattern::closeEvent(QCloseEvent *event)
{

    event->accept();

}

void FilePattern::spara()
{

    QString conf_file_name = QDir::toNativeSeparators("/exif_rename3.conf");
    k2 = new Config(conf_file_name,2);
    QString s = m_ui->cbSeparator->currentText();
    std::string s_s = s.toStdString();
    k2->setConf("separator",s_s);

    s = m_ui->cbDateTimeSeparator->currentText();
    s_s = s.toStdString();
    k2->setConf("date_time_separator",s_s);

    s = m_ui->cbFolderSeparator->currentText();
    s_s = s.toStdString();
    k2->setConf("folder_separator",s_s);

    if ( m_ui->chOnlyTime->isChecked() == true)
        k2->setConf("onlytime","1");
    if ( m_ui->chOnlyTime->isChecked() == false)
        k2->setConf("onlytime","0");

    if (m_ui->raLiten->isChecked())
        k2->setConf("extension","lowercase");
    if (m_ui->raStor->isChecked())
        k2->setConf("extension","uppercase");

    s = m_ui->leExtension->text().trimmed();
    s_s = s.toStdString();
    k2->setConf("extension_name",s_s);

delete k2;
close();

}

void FilePattern::setConfig()
{

    QString conf_file_name = QDir::toNativeSeparators("/exif_rename3.conf");
    k2 = new Config(conf_file_name,2);


    int i=0;
    std::string s;



 s = k2->getConf("separator"); // none, space, -, _
    if (s=="nothing")
        i=2;
    if (s=="none")
        i=0;
    if (s=="space")
        i=1;
    if (s=="-")
        i=2;
    if (s=="_")
        i=3;

 m_ui->cbSeparator->setCurrentIndex(i);





    s = k2->getConf("date_time_separator"); // none, space, -, _
    if (s=="nothing")
        i=3;
    if (s=="none")
        i=0;
    if (s=="space")
        i=1;
    if (s=="-")
        i=2;
    if (s=="_")
        i=3;

       m_ui->cbDateTimeSeparator->setCurrentIndex(i);



    s = k2->getConf("folder_separator"); // none, space, -, _
    if (s=="nothing")
        i=2;
    if (s=="none")
        i=0;
    if (s=="space")
        i=1;
    if (s=="-")
        i=2;
    if (s=="_")
        i=3;

 m_ui->cbFolderSeparator->setCurrentIndex(i);



         s = k2->getConf("extension_name");
         QString s_s = QString::fromStdString(s);
         s_s=s_s.trimmed();
            if (s_s=="nothing")
            {
                m_ui->leExtension->setText(STANDARD_EXTENSION);
                k2->newConf("extension_name",STANDARD_EXTENSION);
                s_s="STANDARD_EXTENSION";

            }
            else
            {
                m_ui->leExtension->setText(s_s);
                m_ui->lblExtension->setText(tr("Extension: .")+s_s.toLower()+tr(" or .")+s_s.toUpper());



            }




    s = k2->getConf("onlytime");
    if ( s == "nothing")
        m_ui->chOnlyTime->setChecked(false);
    if ( s == "0")
        m_ui->chOnlyTime->setChecked(false);
    if ( s == "1")
        m_ui->chOnlyTime->setChecked(true);










    m_ui->lblExample->setText(format_date_time());
    // lblexample_setText();
    m_ui->lblFolderExample->setText(format_folder_date_time());

    s = k2->getConf("extension");

            if (s=="nothing")
                {
                m_ui->raLiten->setChecked(true);
                }
            if (s=="lowercase")
                {
                m_ui->raLiten->setChecked(true);
                }
            if (s=="uppercase")
                {
                m_ui->raStor->setChecked(true);

                }
delete k2;



}


void FilePattern::cursorChanged(int old, int ny)
{
QString s = m_ui->leExtension->text().trimmed();
int i = s.size();
if ( old < ny)
m_ui->leExtension->setCursorPosition(i);
//QMessageBox::critical(this,TITLE_STRING,"BBB");

}
