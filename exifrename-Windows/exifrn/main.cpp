/*
    EXIF ReName
    Copyright (C) 2011-2013 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include <QApplication>
#include <QFont>
#include "mainwindow.h"
#include "help.h"
#include "config.h"
#include <QtGui>
#include <string>
#include <QTranslator>
#include <QString>
#include <QMessageBox>
//#include <QStringBuilder>


// Windows
   const QString INSTALL_DIR = QDir::toNativeSeparators(QDir::currentPath()+"/");
// Slackware
// const QString INSTALL_DIR = "/usr/local/exifrename/";

int main(int argc, char *argv[])
{


    QApplication app(argc, argv);


    QTranslator translator;

    QString configure = QDir::toNativeSeparators("/exif_rename.conf");
     Config *kk = new Config(configure,1);

     std::string sprak = kk->getConf("language");
     delete kk;


     if (sprak == "sv")
     {
        // Linux
     translator.load(INSTALL_DIR+"sprak_sv.qm");
       // translator.load("sprak_sv.qm");
     app.installTranslator(&translator);

     }


     if (sprak == "ru")
     {
        // Linux
     translator.load(INSTALL_DIR+"sprak_ru.qm");
       // translator.load("sprak_ru.qm");
     app.installTranslator(&translator);

     }

     // italienska
     if (sprak == "it")
     {
        // Linux
     translator.load(INSTALL_DIR+"sprak_it.qm");
       // translator.load("sprak_it.qm");
     app.installTranslator(&translator);

     }

     // spanska
     if (sprak == "sp")
     {
        // Linux
     translator.load(INSTALL_DIR+"sprak_sp.qm");
       // translator.load("sprak_it.qm");
     app.installTranslator(&translator);

     }

     if (sprak == "de")
     {
        // Linux
     translator.load(INSTALL_DIR+"sprak_de.qm");
       // translator.load("sprak_it.qm");
     app.installTranslator(&translator);

     }


     QFont font;

     font.setPixelSize(14);
    app.QApplication::setFont(font);
     MainWindow mw;

    mw.show();

    return app.exec();
}
