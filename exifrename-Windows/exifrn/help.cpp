﻿/*
    EXIF ReName
    Copyright (C) 2011-2013 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/


#include "help.h"
#include "config.h"
#include "mainwindow.h"
#include <QVBoxLayout>
#include <QPushButton>
#include <QDialog>
#include <QCloseEvent>
#include <QFile>
#include <QTextEdit>
#include <QString>
#include <QTextStream>
#include <QMessageBox>
#include <string>
#include <sstream>
#include <QDir>
//#include <QStringBuilder>




// Windows
   const QString INSTALL_DIR = QDir::toNativeSeparators(QDir::currentPath()+"/");
// Slackware
// const QString INSTALL_DIR = "/usr/local/exifrename/";


void Help::startHelp(std::string fil,Config *k)
{


  setWindowTitle(TITLE_STRING);


      topTopLayout = new QVBoxLayout(this);


     pu = new QPushButton(tr("Close"));
     pu->setPalette(QColor(255, 255, 255));
     pu->setFixedSize(100,30);
     te = new QTextEdit;
     te->QFrame::setFrameStyle(0);
     te->setPalette(QColor(255, 255, 153));
     te->setReadOnly(true);
     topTopLayout->addWidget(te);
     topTopLayout->addWidget(pu);



      connect( pu, SIGNAL( pressed()  ) , this, SLOT( closeX() ) );

     kh = k;
    getConfig(kh);
     read_info(fil);

}



void Help::closeHelp()
{

    QRect rect;
rect=this->geometry();



int wi = rect.width();
int he = rect.height();
int x_lage=rect.x();
int y_lage=rect.y();

std::stringstream ss;
std::string str;
ss << wi;
ss >> str;

kh->setConf("hvidd",str);
ss.clear();

ss << he;
ss >> str;

kh->setConf("hhojd",str);


ss.clear();

ss << x_lage;
ss >> str;
kh->setConf("hx_lage",str);

ss.clear();
ss << y_lage;
ss >> str;
kh->setConf("hy_lage",str);


}

void Help::closeX()
{

    closeHelp();
    close();

}

void Help::closeEvent(QCloseEvent *event)
{
    event->accept();
    closeHelp();
    close();
}

void Help::read_info(std::string fil)
{


                QString qfil = INSTALL_DIR+QString::fromStdString(fil);

                QString allt;
                QFile file(qfil);
                if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
                {
					//Om översatt hjälpfil saknas, pröva den engelska
					qfil = INSTALL_DIR+QString("info_eng.html");
					file.setFileName(qfil);
					if (!file.open(QIODevice::ReadOnly | QIODevice::Text))
                     {
                         QMessageBox::critical(this, TITLE_STRING,tr("Error reading\n")+qfil+tr("\ninfo file missing!"));
                         return;
					}
                }

     while (!file.atEnd())
      {
         QString line = file.readLine();
            allt += line;
     }
                file.close();

            // te->setText(allt);
            // qt-4.1.4
        te->setHtml(allt);
}
void Help::getConfig(Config *k)
{

  int w,h,x,y;
  std::string s;
  std::stringstream ss;

  s=k->getConf("hvidd");;
  ss << s;
  ss >> w;
  ss.clear();

  s=k->getConf("hhojd");;
  ss << s;
  ss >> h;
  ss.clear();

  s=k->getConf("hx_lage");;
  ss << s;
  ss >> x;
  ss.clear();

  s=k->getConf("hy_lage");;
  ss << s;
  ss >> y;



  setGeometry(x,y,w,h);

}


