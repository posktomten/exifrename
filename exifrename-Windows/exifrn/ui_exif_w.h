/********************************************************************************
** Form generated from reading UI file 'exif_w.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_EXIF_W_H
#define UI_EXIF_W_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_Exif_w
{
public:
    QLineEdit *leDateTime;
    QLabel *lblDateTime;
    QLabel *lblvTidsstampel;
    QLabel *lblTidsstampel;
    QLabel *lblDateTime_2;
    QPushButton *pbOk;
    QPushButton *pbCancel;
    QPushButton *pbCurrentTime;

    void setupUi(QDialog *Exif_w)
    {
        if (Exif_w->objectName().isEmpty())
            Exif_w->setObjectName(QString::fromUtf8("Exif_w"));
        Exif_w->setWindowModality(Qt::ApplicationModal);
        Exif_w->resize(370, 170);
        Exif_w->setMinimumSize(QSize(370, 170));
        Exif_w->setMaximumSize(QSize(16777215, 170));
        leDateTime = new QLineEdit(Exif_w);
        leDateTime->setObjectName(QString::fromUtf8("leDateTime"));
        leDateTime->setGeometry(QRect(10, 100, 221, 31));
        QFont font;
        font.setPointSize(10);
        leDateTime->setFont(font);
        leDateTime->setAutoFillBackground(false);
        leDateTime->setMaxLength(19);
        lblDateTime = new QLabel(Exif_w);
        lblDateTime->setObjectName(QString::fromUtf8("lblDateTime"));
        lblDateTime->setGeometry(QRect(10, 60, 271, 16));
        lblvTidsstampel = new QLabel(Exif_w);
        lblvTidsstampel->setObjectName(QString::fromUtf8("lblvTidsstampel"));
        lblvTidsstampel->setGeometry(QRect(10, 10, 311, 17));
        lblTidsstampel = new QLabel(Exif_w);
        lblTidsstampel->setObjectName(QString::fromUtf8("lblTidsstampel"));
        lblTidsstampel->setGeometry(QRect(10, 30, 341, 17));
        lblDateTime_2 = new QLabel(Exif_w);
        lblDateTime_2->setObjectName(QString::fromUtf8("lblDateTime_2"));
        lblDateTime_2->setGeometry(QRect(10, 80, 291, 16));
        pbOk = new QPushButton(Exif_w);
        pbOk->setObjectName(QString::fromUtf8("pbOk"));
        pbOk->setGeometry(QRect(10, 140, 75, 25));
        pbCancel = new QPushButton(Exif_w);
        pbCancel->setObjectName(QString::fromUtf8("pbCancel"));
        pbCancel->setGeometry(QRect(90, 140, 75, 25));
        pbCurrentTime = new QPushButton(Exif_w);
        pbCurrentTime->setObjectName(QString::fromUtf8("pbCurrentTime"));
        pbCurrentTime->setGeometry(QRect(250, 100, 101, 31));

        retranslateUi(Exif_w);

        QMetaObject::connectSlotsByName(Exif_w);
    } // setupUi

    void retranslateUi(QDialog *Exif_w)
    {
        Exif_w->setWindowTitle(QApplication::translate("Exif_w", "Dialog", 0, QApplication::UnicodeUTF8));
        leDateTime->setInputMask(QString());
        lblDateTime->setText(QApplication::translate("Exif_w", "Date and time", 0, QApplication::UnicodeUTF8));
        lblvTidsstampel->setText(QApplication::translate("Exif_w", "Change the time stamp on", 0, QApplication::UnicodeUTF8));
        lblTidsstampel->setText(QApplication::translate("Exif_w", "TextLabel", 0, QApplication::UnicodeUTF8));
        lblDateTime_2->setText(QApplication::translate("Exif_w", "Year-Month-Day Hour-Minute-Second", 0, QApplication::UnicodeUTF8));
        pbOk->setText(QApplication::translate("Exif_w", "OK", 0, QApplication::UnicodeUTF8));
        pbCancel->setText(QApplication::translate("Exif_w", "Cancel", 0, QApplication::UnicodeUTF8));
        pbCurrentTime->setText(QApplication::translate("Exif_w", "Current Time", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Exif_w: public Ui_Exif_w {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_EXIF_W_H
