<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="de_DE">
<context>
    <name>Exif_w</name>
    <message>
        <location filename="exif_w.ui" line="29"/>
        <source>Dialog</source>
        <translation>Dialog</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="65"/>
        <source>Date and time</source>
        <translation>Datum und Uhrzeit</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="78"/>
        <source>Change the time stamp on</source>
        <translation>Zeitstempel ändern auf</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="91"/>
        <source>TextLabel</source>
        <translation>TextBeschriftung</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="104"/>
        <source>Year-Month-Day Hour-Minute-Second</source>
        <translation>Jahr-Monat-Tag Stunde-Minute-Sekunde</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="117"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="130"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="exif_w.ui" line="143"/>
        <source>Current Time</source>
        <translation>Aktuelle Uhrzeit</translation>
    </message>
    <message>
        <location filename="exif_w.cpp" line="72"/>
        <source>Incorrect date/time format, unable to write to file.
You must write in this way: </source>
        <translation>Falsches Datum/Zeit-Format, kann nicht in Datei schreiben.
Sie müssen so schreiben: </translation>
    </message>
    <message>
        <location filename="exif_w.cpp" line="101"/>
        <source>Unable to save EXIF data</source>
        <translation>EXIF-Daten können nicht gespeichert werden</translation>
    </message>
</context>
<context>
    <name>FilePattern</name>
    <message>
        <location filename="filepattern.ui" line="58"/>
        <source>file name</source>
        <translation>Dateiname</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="71"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The numbers in the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Die Zahlen im Dateinamen sind getrennt durch ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="79"/>
        <location filename="filepattern.ui" line="116"/>
        <location filename="filepattern.ui" line="265"/>
        <source>none</source>
        <translation>kein</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="84"/>
        <location filename="filepattern.ui" line="121"/>
        <location filename="filepattern.ui" line="270"/>
        <source>space</source>
        <translation>Leerzeichen</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="89"/>
        <location filename="filepattern.ui" line="126"/>
        <location filename="filepattern.ui" line="275"/>
        <source>-</source>
        <translation>-</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="94"/>
        <location filename="filepattern.ui" line="131"/>
        <location filename="filepattern.ui" line="280"/>
        <source>_</source>
        <translation>_</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="108"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Date and time component part of the filename are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Datum- und Zeitbestandteil des Dateinamens sind durch getrennt durch ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="148"/>
        <source>File name figures, separated by</source>
        <translation>Dateinamenzahlen, getrennt durch</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="167"/>
        <source>Date and time, separated by</source>
        <translation>Datum und Zeit, getrennt durch</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="186"/>
        <source>File extension (.jpg or .JPG)</source>
        <translation>Dateierweiterung (.jpg oder .JPG)</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="199"/>
        <source>CAPITAL letter</source>
        <translation>GROSSbuchstabe</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="215"/>
        <source>lowercase letter</source>
        <translation>kleinbuchstabe</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="231"/>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="244"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="257"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;The figures in the folder name are separated by ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; font-size:10pt;&quot;&gt;Die Zahlen im Ordnernamen sind getrennt durch ...&lt;/span&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="294"/>
        <source>Folder name figures, separated by</source>
        <translation>Ordnernamenzahlen, getrennt durch</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="313"/>
        <source>File name:</source>
        <translation>Dateiname:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="332"/>
        <source>Folder name:</source>
        <translation>Ordnername:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="351"/>
        <source>folder name</source>
        <translation>Ordnername</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="367"/>
        <source>Only time in file name</source>
        <translation>Nur Zeit in Dateiname</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="801"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Extensions of files you want to be able to open, separated by cemikolon. S&lt;span style=&quot; color:#000000;&quot;&gt;uch as jpg and tif.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000&quot;&gt;Example:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;jpg;tif;cr2&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;MS Shell Dlg 2&apos;; font-size:8.25pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Dateierweiterungen, die Sie öffnen können, getrennt durch Strichpunkt. &lt;span style=&quot; color:#000000;&quot;&gt;Wie jpeg und tiff.&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000&quot;&gt;Beispiel:&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;span style=&quot; color:#000000;&quot;&gt;jpg;tif;cr2&lt;/span&gt;&lt;/p&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; color:#000000;&quot;&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="834"/>
        <source>File extension:</source>
        <translation>Dateierweiterung:</translation>
    </message>
    <message>
        <location filename="filepattern.ui" line="853"/>
        <source>*.</source>
        <translation>*.</translation>
    </message>
    <message>
        <location filename="filepattern.cpp" line="97"/>
        <location filename="filepattern.cpp" line="321"/>
        <source>Extension: .</source>
        <translation>Erweiterung: .</translation>
    </message>
    <message>
        <location filename="filepattern.cpp" line="97"/>
        <location filename="filepattern.cpp" line="321"/>
        <source> or .</source>
        <translation> oder .</translation>
    </message>
</context>
<context>
    <name>Help</name>
    <message>
        <location filename="help.cpp" line="55"/>
        <source>Close</source>
        <translation>Schließen</translation>
    </message>
    <message>
        <location filename="help.cpp" line="148"/>
        <source>Error reading
</source>
        <translation>Fehler beim Lesen
</translation>
    </message>
    <message>
        <location filename="help.cpp" line="148"/>
        <source>
info file missing!</source>
        <translation>
Info-Datei fehlt!</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="mainwindow.cpp" line="120"/>
        <source>Copy (do not rename)</source>
        <translation>Kopieren (nicht umbenennen)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="122"/>
        <source>Copy to...</source>
        <translation>Kopieren nach...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="148"/>
        <source>Preserve original file name</source>
        <translation>Originaldateiname beibehalten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="150"/>
        <location filename="mainwindow.cpp" line="183"/>
        <location filename="mainwindow.cpp" line="392"/>
        <location filename="mainwindow.cpp" line="405"/>
        <source>Before date/time</source>
        <translation>Vor Datum/Zeit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="151"/>
        <location filename="mainwindow.cpp" line="184"/>
        <location filename="mainwindow.cpp" line="398"/>
        <location filename="mainwindow.cpp" line="411"/>
        <source>After date/time</source>
        <translation>Nach Datum/Zeit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="181"/>
        <source>Insert new file name</source>
        <translation>Neuen Dateinamen einfügen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="205"/>
        <source>Go!</source>
        <translation>Los!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="224"/>
        <source>Clear log</source>
        <translation>Protokoll löschen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="226"/>
        <source>Save log</source>
        <translation>Protokoll speichern</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="343"/>
        <source>&amp;Open...</source>
        <translation>&amp;Öffnen...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="344"/>
        <source>Ctrl+O</source>
        <translation>Strg+O</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="345"/>
        <source>Open jpeg image(s)</source>
        <translation>jpeg-Bild(er) öffnen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="349"/>
        <source>&amp;Rename</source>
        <translation>&amp;Umbenennen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="350"/>
        <source>Ctrl+R</source>
        <translation>Strg+R</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="351"/>
        <source>Rename original file(s)</source>
        <translation>Originaldatei(en) umbenennen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="356"/>
        <source>&amp;Copy to...</source>
        <translation>&amp;Kopieren nach...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="357"/>
        <source>Ctrl+C</source>
        <translation>Strg+C</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="358"/>
        <source>Copy original file(s) with new name to folder...</source>
        <translation>Originaldatei(en) mit neuem Namen in Ordner kopieren...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="365"/>
        <source>E&amp;xit</source>
        <translation>&amp;Beenden</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="366"/>
        <source>Ctrl+Q</source>
        <translation>Strg+Q</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="367"/>
        <source>Exit application</source>
        <translation>Anwendung beenden</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="377"/>
        <source>Create folders (yyyy_mm_dd)</source>
        <translation>Ordner erstellen (jjjj_mm_tt)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="379"/>
        <source>Create folders according to file creation date and copy the files into these folders</source>
        <translation>Ordner nach Dateierstellungsdatum erstellen und Dateien in diese Ordner kopieren</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="384"/>
        <source>No name exept date/time</source>
        <translatorcomment>Typo in &apos;exept&apos;. It must be &apos;except&apos;.</translatorcomment>
        <translation>Kein Name außer Datum/Zeit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="386"/>
        <source>File name is only date/time</source>
        <translation>Dateiname ist nur Datum/Zeit</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="394"/>
        <source>Preserve original file name before date/time</source>
        <translation>Originaldateinamen vor Datum/Zeit beibehalten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="400"/>
        <source>Preserve original file name after date/time</source>
        <translation>Originaldateinamen nach Datum/Zeit beibehalten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="407"/>
        <source>Insert new file name before date/time</source>
        <translation>Neuen Dateinamen vor Datum/Zeit einfügen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="413"/>
        <source>Insert new file name after date/time</source>
        <translation>Neuen Dateinamen nach Datum/Zeit einfügen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="418"/>
        <location filename="mainwindow.cpp" line="419"/>
        <source>Edit date and time...</source>
        <translation>Datum und Zeit bearbeiten...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="425"/>
        <source>Help...</source>
        <translation>Hilfe...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="426"/>
        <source>Help for application</source>
        <translation>Hilfe zur Anwendung</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="430"/>
        <source>About...</source>
        <translation>Über...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="431"/>
        <source>About application</source>
        <translation>Über die Anwendung</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="435"/>
        <source>About Qt...</source>
        <translation>Über Qt...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="436"/>
        <source>About Qt C++ Application Development Framework</source>
        <translation>Über Qt C++ Rahmenwerk zur Anwendungsentwicklung</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="440"/>
        <source>Check for updates...</source>
        <translation>Nach Aktualisierungen suchen...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="441"/>
        <source>Check for new versions of EXIF ReName</source>
        <translation>Nach neuen Versionen von EXIF ReName suchen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="447"/>
        <location filename="mainwindow.cpp" line="2306"/>
        <source>English</source>
        <translation>Englisch</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="448"/>
        <source>English language</source>
        <translation>Englische Sprache</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="453"/>
        <source>Italian</source>
        <translation>Italienisch</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="454"/>
        <source>Italian language</source>
        <translation>Italienische Sprache</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="459"/>
        <location filename="mainwindow.cpp" line="2311"/>
        <source>German</source>
        <translation>Deutsche</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="460"/>
        <source>German language</source>
        <translation>idioma alemandeutsche Sprache</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="465"/>
        <location filename="mainwindow.cpp" line="2308"/>
        <source>Russian</source>
        <translation>Russisch</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="466"/>
        <source>Russian language</source>
        <translation>Russische Sprache</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="471"/>
        <location filename="mainwindow.cpp" line="2309"/>
        <source>Swedish</source>
        <translation>Schwedisch</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="472"/>
        <source>Swedish language</source>
        <translation>Schwedische Sprache</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="477"/>
        <location filename="mainwindow.cpp" line="2310"/>
        <source>Spanish</source>
        <translation>Spanisch</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="478"/>
        <source>Spanish language</source>
        <translation>Spanische Sprache</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="484"/>
        <source>Delete personal settings</source>
        <translation>Persönliche Einstellungen löschen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="485"/>
        <source>Remove your personal settings</source>
        <translation>Entfernen Sie Ihre persönlichen Einstellungen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="489"/>
        <location filename="mainwindow.cpp" line="490"/>
        <source>Specify file name pattern...</source>
        <translation>Dateinamensmuster angeben...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="498"/>
        <source>Icons only</source>
        <translation>Nur Symbole</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="499"/>
        <source>Display only icons in the toolbar</source>
        <translation>Nur Symbole in der Werkzeugleiste anzeigen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="502"/>
        <source>Text only</source>
        <translation>Nur Text</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="503"/>
        <source>Display only text in the toolbar</source>
        <translation>Nur Text in der Werkzeugleiste anzeigen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="506"/>
        <source>Text alongside icons</source>
        <translation>Text neben Symbolen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="507"/>
        <source>Display text alongside icons in the toolbar</source>
        <translation>Text neben Symbolen in der Werkzeugleiste anzeigen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="511"/>
        <source>Text under icons</source>
        <translation>Text unter Symbolen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="512"/>
        <source>Display text under icons in the toolbar</source>
        <translation>Text unter Symbolen in der Werkzeugleiste anzeigen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="515"/>
        <source>No toolbar</source>
        <translation>Keine Werkzeugleiste</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="516"/>
        <source>No toolbar will be displayed</source>
        <translation>Es wird keine Werkzeugleiste angezeigt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="528"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="536"/>
        <source>&amp;Edit</source>
        <translation>&amp;Bearbeiten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="542"/>
        <source>&amp;Preserve original file name</source>
        <translation>Originaldateiname &amp;beibehalten</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="546"/>
        <source>&amp;Insert new file name</source>
        <translation>Neuen Dateinamen e&amp;ingeben</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="553"/>
        <source>&amp;View</source>
        <translation>&amp;Ansicht</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="555"/>
        <location filename="mainwindow.cpp" line="645"/>
        <source>Toolbar</source>
        <translation>Werkzeugleiste</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="565"/>
        <source>&amp;Language</source>
        <translation>&amp;Sprache</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="576"/>
        <source>&amp;Tools</source>
        <translation>&amp;Werkzeuge</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="581"/>
        <source>&amp;Help</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="596"/>
        <source>Do you want to remove your personal settings?
This is an appropriate action if you like to uninstall this application.
</source>
        <translation>Möchten Sie Ihre persönlichen Einstellungen entfernen?
Dies ist eine geeignete Aktion, wenn Sie diese Anwendung deinstallieren möchten.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="596"/>
        <source> will exit after your personal data is deleted.</source>
        <translation> wird beendet, nachdem Ihre persönlichen Daten gelöscht wurden.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="597"/>
        <location filename="mainwindow.cpp" line="1643"/>
        <location filename="mainwindow.cpp" line="1684"/>
        <location filename="mainwindow.cpp" line="1723"/>
        <location filename="mainwindow.cpp" line="1765"/>
        <location filename="mainwindow.cpp" line="1807"/>
        <location filename="mainwindow.cpp" line="1848"/>
        <location filename="mainwindow.cpp" line="2079"/>
        <source>Yes</source>
        <translation>Ja</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="598"/>
        <location filename="mainwindow.cpp" line="1644"/>
        <location filename="mainwindow.cpp" line="1685"/>
        <location filename="mainwindow.cpp" line="1724"/>
        <location filename="mainwindow.cpp" line="1766"/>
        <location filename="mainwindow.cpp" line="1808"/>
        <location filename="mainwindow.cpp" line="1849"/>
        <location filename="mainwindow.cpp" line="2080"/>
        <source>No</source>
        <translation>Nein</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="606"/>
        <source>Your personal settings are deleted and
 </source>
        <translation>Ihre persönlichen Einstellungen werden gelöscht und
 </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="606"/>
        <source> will exit</source>
        <translation> wird beendet</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="612"/>
        <source>Your personal settings cannot be found.
New settings will be saved then you restart the program.</source>
        <translation>Ihre persönlichen Einstellungen können nicht gefunden werden.
Neue Einstellungen werden gespeichert und dann starten Sie das Programm neu.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="643"/>
        <source>File</source>
        <translation>Datei</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="663"/>
        <source>Create folders (yyyy</source>
        <translation>Ordner erstellen (jjjj</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="663"/>
        <source>mm</source>
        <translation>mm</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="663"/>
        <source>dd)</source>
        <translation>tt)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="682"/>
        <location filename="mainwindow.cpp" line="2385"/>
        <source>Open File</source>
        <translation>Datei öffnen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="725"/>
        <location filename="mainwindow.cpp" line="2412"/>
        <source>Image files</source>
        <translation>Bilddateien</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="752"/>
        <location filename="mainwindow.cpp" line="2440"/>
        <source>You do not have enough permissions to read
</source>
        <translation>Sie haben nicht genügend Berechtigungen zum Lesen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="775"/>
        <location filename="mainwindow.cpp" line="776"/>
        <source> File(s) selected</source>
        <translation>Datei(en) ausgewählt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="785"/>
        <location filename="mainwindow.cpp" line="1933"/>
        <location filename="mainwindow.cpp" line="1978"/>
        <source>The original file will be renamed</source>
        <translation>Die Originaldatei wird umbenannt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="931"/>
        <source>Copy pictures to folder</source>
        <translation>Bilder in Ordner kopieren</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="953"/>
        <source>You have not enough permissions
to write to </source>
        <translation>Sie haben nicht genügend Berechtigungen
zum Schreiben in </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="959"/>
        <location filename="mainwindow.cpp" line="1084"/>
        <location filename="mainwindow.cpp" line="1945"/>
        <location filename="mainwindow.cpp" line="1947"/>
        <location filename="mainwindow.cpp" line="1968"/>
        <location filename="mainwindow.cpp" line="1970"/>
        <source>Copy to: </source>
        <translation>Kopieren nach: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1046"/>
        <source>rename</source>
        <translation>umbenennen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1048"/>
        <source>copy</source>
        <translation>kopieren</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1054"/>
        <source>You have not selected any file(s).
Please select file(s) to </source>
        <translation>Sie haben keine Datei(en) ausgewählt.
Bitte Datei(en) auswählen zum </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1080"/>
        <source>Error in destination path
Please select another output directory!</source>
        <translation>Fehler im Zielpfad
Bitte wählen Sie ein anderes Ausgabeverzeichnis aus!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1168"/>
        <location filename="mainwindow.cpp" line="1231"/>
        <location filename="mainwindow.cpp" line="1282"/>
        <location filename="mainwindow.cpp" line="1338"/>
        <location filename="mainwindow.cpp" line="1397"/>
        <location filename="mainwindow.cpp" line="1456"/>
        <source>Unable to create directory 
</source>
        <translation>Verzeichnis konnte nicht erstellt werden 
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1168"/>
        <location filename="mainwindow.cpp" line="1231"/>
        <location filename="mainwindow.cpp" line="1282"/>
        <location filename="mainwindow.cpp" line="1338"/>
        <location filename="mainwindow.cpp" line="1397"/>
        <location filename="mainwindow.cpp" line="1456"/>
        <source>
check your permisions</source>
        <translation>
überprüfen Sie Ihre Berechtigungen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1190"/>
        <location filename="mainwindow.cpp" line="1304"/>
        <location filename="mainwindow.cpp" line="1418"/>
        <source>Unable to delete:
</source>
        <translation>Nicht löschbar:
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1366"/>
        <source>Indicate a new file name, please!</source>
        <translation>Bitte geben Sie einen neuen Dateinamen an!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1481"/>
        <source>Done! </source>
        <translation>Fertig! </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1481"/>
        <source> of </source>
        <translation> von </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1481"/>
        <source>file(s) )</source>
        <translation>Datei(en) )</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1509"/>
        <source>/exif_rename_log.txt</source>
        <translation>/exif_rename_log.txt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1512"/>
        <source>Save log as...</source>
        <translation>Protokoll speichern als...</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1519"/>
        <source>Text files (*.txt)</source>
        <translation>Textdateien (*.txt)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1519"/>
        <source>Any files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1538"/>
        <source>Error writing to
</source>
        <translation>Fehler beim Schreiben in
</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1538"/>
        <source>
check your permissions</source>
        <translation>
überprüfen Sie Ihre Berechtigungen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1543"/>
        <source>*** Log saved: </source>
        <translation>*** Protokoll gespeichert: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1549"/>
        <source>Log created by </source>
        <translation>Protokoll erstellt von </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1551"/>
        <source>Copyright Ingemar Ceicer 2007 - </source>
        <translation>Urheberrecht Ingemar Ceicer 2007 - </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1609"/>
        <location filename="mainwindow.cpp" line="2075"/>
        <source>Licensierat under GNU General Public License This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version </source>
        <translatorcomment>&apos;Licensierat&apos; source string is no English word.</translatorcomment>
        <translation>Lizensiert unter der GNU General Public License. Dieses Programm ist freie Software; Sie können es unter den Bedingungen der GNU General Public License, wie von der Free Software Foundation veröffentlicht, weiterverbreiten und/oder modifizieren; entweder Version </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1609"/>
        <location filename="mainwindow.cpp" line="2075"/>
        <source> of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</source>
        <translation> der Lizenz oder (nach Ihrer Wahl) eine spätere Version. Dieses Programm wird in der Hoffnung, dass es nützlich sein wird, aber OHNE JEGLICHE GEWÄHRLEISTUNG verteilt; ohne auch nur die implizierte Gewährleistung der MARKTGÄNGIGKEIT oder BRAUCHBARKEIT FÜR EINEN BESTIMMTEN ZWECK. Weitere Informationen finden Sie in der GNU General Public License. Sie sollten eine Kopie der GNU General Public License zusammen mit diesem Programm erhalten haben; Wenn nicht, schreiben Sie an die Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1612"/>
        <location filename="mainwindow.cpp" line="2074"/>
        <source>&lt;br&gt;&lt;font coor=&quot;green&quot; size=&quot;3&quot;&gt;&lt;b&gt;Copyright &amp;copy; 2007 - </source>
        <translation>&lt;br&gt;&lt;font coor=&quot;green&quot; size=&quot;3&quot;&gt;&lt;b&gt;Urheberrecht &amp;copy; 2007 - </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1612"/>
        <location filename="mainwindow.cpp" line="2074"/>
        <source>homepage</source>
        <translation>Homepage</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1613"/>
        <source>About </source>
        <translation>Über </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1613"/>
        <location filename="mainwindow.cpp" line="2078"/>
        <source>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</source>
        <translation>&lt;h1&gt;&lt;font color=&quot;#009900&quot;&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1614"/>
        <source>A small application for renaming pictures. EXIF ReName uses exif data from the picture file.&lt;br&gt;&lt;br&gt;</source>
        <translation>Eine kleine Anwendung zum Umbenennen von Bildern. EXIF ReName verwendet exif-Daten aus der Bilddatei.&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1614"/>
        <source>Thank you Ivan Dolgov for the translation into Russian.</source>
        <translation>Vielen Dank Ivan Dolgov für die Übersetzung ins Russische.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1614"/>
        <source>Thank you Roberto Nerozzi for the translation into Italien.</source>
        <translation>Vielen Dank Roberto Nerozzi für die Übersetzung ins Italienische.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1614"/>
        <source>Thank you Cristian Sosa Noe for the translation into Spanish.</source>
        <translation>Vielen Dank Cristian Sosa Noe für die Übersetzung ins Spanische.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1614"/>
        <source>Thank you Ben Weis for the translation into German.</source>
        <translation>Vielen Dank Ben Weis für die Übersetzung ins Deutsch.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1642"/>
        <location filename="mainwindow.cpp" line="1683"/>
        <location filename="mainwindow.cpp" line="1722"/>
        <location filename="mainwindow.cpp" line="1764"/>
        <location filename="mainwindow.cpp" line="1806"/>
        <location filename="mainwindow.cpp" line="1847"/>
        <source>The application needs to restart in order
to initialise the now language settings.

Do you want to close
the application now?</source>
        <translatorcomment>Typo! It must be &apos;new language settings&apos;.</translatorcomment>
        <translation>Die Anwendung muss neu gestartet werden,
um die neuen Spracheinstellungen zu initialisieren.

Möchten Sie die Anwendung
jetzt schließen?</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1644"/>
        <location filename="mainwindow.cpp" line="1685"/>
        <location filename="mainwindow.cpp" line="1724"/>
        <location filename="mainwindow.cpp" line="1766"/>
        <location filename="mainwindow.cpp" line="1808"/>
        <location filename="mainwindow.cpp" line="1849"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="1914"/>
        <source>Ready for action!</source>
        <translation>Einsatzbereit!</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2078"/>
        <source>&lt;br&gt;A small application for renaming pictures. EXIF ReName uses exif data from the picture file.&lt;br&gt;&lt;br&gt;</source>
        <translation>&lt;br&gt;Eine kleine Anwendung zum Umbenennen von Bildern. EXIF ReName verwendet exif-Daten aus der Bilddatei.&lt;br&gt;&lt;br&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2078"/>
        <source>&lt;br&gt;&lt;br&gt;&lt;font color=&quot;#FF0000&quot; size=&quot;4&quot;&gt;Do you accept the license?&lt;/font&gt;</source>
        <translation>&lt;br&gt;&lt;br&gt;&lt;font color=&quot;#FF0000&quot; size=&quot;4&quot;&gt;Akzeptieren Sie die Lizenz?&lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2116"/>
        <location filename="mainwindow.cpp" line="2124"/>
        <source> is updatet to </source>
        <translatorcomment>Typo!
&apos;updated&apos; is correct.</translatorcomment>
        <translation> ist aktualisiert zu </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2160"/>
        <source>Copied to new folder in: &lt;b&gt;</source>
        <translation>In neuen Ordner kopiert nach: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2160"/>
        <location filename="mainwindow.cpp" line="2164"/>
        <source>&lt;/b&gt; and renamed: </source>
        <translation>&lt;/b&gt; und umbenannt: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2164"/>
        <source>Copied to: &lt;b&gt;</source>
        <translation>Kopiert nach: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2175"/>
        <source>&lt;br /&gt;Selected file(s) in: &lt;b&gt;</source>
        <translation>&lt;br /&gt;Ausgewählte Datei(en) in: &lt;b&gt;</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2307"/>
        <source>Italiano</source>
        <translation>Italienisch</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2312"/>
        <source>Choose the language that will be used by EXIF Rename</source>
        <translation>Wählen Sie die Sprache aus, die von EXIF Rename verwendet wird</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2326"/>
        <source>You must restart the application to the Russian language settings to take effect.</source>
        <translation>Sie müssen die Anwendung neu starten, damit die russischen Spracheinstellungen wirksam werden.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2334"/>
        <source>You must restart the application to the Italien language settings to take effect.</source>
        <translation>Sie müssen die Anwendung neu starten, damit die italienischen Spracheinstellungen wirksam werden.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2342"/>
        <source>You must restart the application to the Swedish language settings to take effect.</source>
        <translation>Sie müssen die Anwendung neu starten, damit die schwedischen Spracheinstellungen wirksam werden.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2349"/>
        <location filename="mainwindow.cpp" line="2356"/>
        <source>You must restart the application to the Spanish language settings to take effect.</source>
        <translation>Sie müssen die Anwendung neu starten, damit die spanischen Spracheinstellungen wirksam werden.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2416"/>
        <source>All files</source>
        <translation>Alle Dateien</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2452"/>
        <source>Changes the EXIF information for these files</source>
        <translation>Ändert die EXIF-Informationen für diese Dateien</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2461"/>
        <source> ...selected</source>
        <translation> ...ausgewählt</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2506"/>
        <source>MS-DOS-based version of Windows.</source>
        <translation>MS-DOS-basierte Version von Windows.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2509"/>
        <source>NT-based version of Windows.</source>
        <translation>NT-basierte Version von Windows.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2512"/>
        <source>CE-based version of Windows.</source>
        <translation>CE-basierte Version von Windows.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2515"/>
        <source>Windows 3.1 with Win 32s operating system.</source>
        <translation>Windows 3.1 mit Win-32-Betriebssystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2518"/>
        <source>Windows 95 operating system.</source>
        <translation>Windows 95 Betriebssystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2521"/>
        <source>Windows 98 operating system.</source>
        <translation>Windows 98 Betriebssystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2524"/>
        <source>Windows Me operating system.</source>
        <translation>Windows Me Betriebssystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2527"/>
        <source>Windows operating system version 4.0, corresponds to Windows NT.</source>
        <translation>Windows-Betriebssystemversion 4.0, entspricht Windows NT.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2530"/>
        <source>Windows operating system version 5.0, corresponds to Windows 2000.</source>
        <translation>Windows-Betriebssystemversion 5.0, entspricht Windows 2000.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2533"/>
        <source>Windows operating system version 5.1, corresponds to Windows XP.</source>
        <translation>Windows-Betriebssystemversion 5.1, entspricht Windows XP.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2536"/>
        <source>Windows operating system version 5.2, corresponds to Windows Server 2003, Windows Server 2003 R2, Windows Home Server, and Windows XP Professional x64 Edition.</source>
        <translation>Windows-Betriebssystemversion 5.2, entspricht Windows Server 2003, Windows Server 2003 R2, Windows Home Server und Windows XP Professional x64 Edition.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2539"/>
        <source>Windows operating system version 6.0, corresponds to Windows Vista and Windows Server 2008.</source>
        <translation>Windows-Betriebssystemversion 6.0, entspricht Windows Vista und Windows Server 2008.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2542"/>
        <source>Windows operating system version 6.1, corresponds to Windows 7 and Windows Server 2008 R2.</source>
        <translation>Windows-Betriebssystemversion 6.1, entspricht Windows 7 und Windows Server 2008 R2.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2545"/>
        <source>Windows operating system version 6.2, corresponds to Windows 8</source>
        <translation>Windows-Betriebssystemversion 6.2, entspricht Windows 8</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2548"/>
        <source>Operating system version 6.3, corresponds to Windows 8.1</source>
        <translation>Windows-Betriebssystemversion 6.3, entspricht Windows 8.1</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2551"/>
        <source>Operating system version 10.0, corresponds to Windows 10</source>
        <translation>Windows-Betriebssystemversion 10.0, entspricht Windows 10</translation>
    </message>
    <message>
        <source>Windows operating system version 6.2, corresponds to Windows 8.</source>
        <translation type="obsolete">Windows-Betriebssystemversion 6.2, entspricht Windows 8.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2554"/>
        <source>Windows CE operating system.</source>
        <translation>Windows CE Betriebssystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2557"/>
        <source>Windows CE .NET operating system.</source>
        <translation>Windows CE .NET Betriebssystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2560"/>
        <source>Windows CE 5.x operating system.</source>
        <translation>Windows CE 5.x Betriebssystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2563"/>
        <source>Windows CE 6.x operating system.</source>
        <translation>Windows CE 6.x Betriebssystem.</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2566"/>
        <source>Unknown Windows operating system</source>
        <translation>Unbekanntes Windows-Betriebssystem</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2575"/>
        <source>Architecture instruction set: </source>
        <translation>Architektur-Befehlssatz: </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2586"/>
        <source> was created </source>
        <translation> wurde erstellt </translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2596"/>
        <location filename="mainwindow.cpp" line="2598"/>
        <location filename="mainwindow.cpp" line="2633"/>
        <source>Compiled by</source>
        <translation>Kompiliert von</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2629"/>
        <source>Unknown version</source>
        <translation>Unbekannte Version</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="2638"/>
        <source>Unknown compiler.</source>
        <translation>Unbekannter Kompiler.</translation>
    </message>
</context>
<context>
    <name>Update</name>
    <message>
        <location filename="checkupdate.cpp" line="46"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="50"/>
        <source>Check for updates on startup</source>
        <translation>Beim Start nach Aktualisierungen suchen</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="62"/>
        <source>Contacting server...</source>
        <translation>Server wird kontaktiert...</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="134"/>
        <source>Error message: Host found but
the requested version information was not found.
Please try again later</source>
        <translation>Fehlermeldung: Host gefunden, aber
die angeforderte Versionsinformation wurde nicht gefunden.
Bitte versuchen Sie es später erneut</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="135"/>
        <location filename="checkupdate.cpp" line="149"/>
        <location filename="checkupdate.cpp" line="190"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="148"/>
        <source>Error message: </source>
        <translation>Fehlermeldung: </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="148"/>
        <source>
Please try again later</source>
        <translation>
Bitte versuchen Sie es später erneut</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source>New version available: </source>
        <translation>Neue Version verfügbar: </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source>Download it from</source>
        <translation>Herunterladen von</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="170"/>
        <source> homepage</source>
        <translation> Homepage</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="181"/>
        <source>Your current version is the latest available: </source>
        <translation>Ihre aktuelle Version ist die neueste verfügbare: </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="181"/>
        <source>Thank you for using</source>
        <translation>Danke für das Benutzen</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>New version available!</source>
        <translation>Neue Version verfügbar!</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>Version </source>
        <translation>Version </translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>Please visit</source>
        <translation>Bitte besuchen</translation>
    </message>
    <message>
        <location filename="checkupdate.cpp" line="317"/>
        <source>homepage</source>
        <translation>Homepage</translation>
    </message>
</context>
</TS>
