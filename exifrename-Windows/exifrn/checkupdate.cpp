/*
    EXIF ReName
    Copyright (C) 2011-2013 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/


#include "checkupdate.h"
#include "mainwindow.h"

Update::~Update()
{

}


void Update::startUpdate(Config *kk)
{
k=kk;
http = new QHttp(this);





this->setFixedSize(550,200);
this->setContentsMargins(5,5,5,5);

  setWindowTitle(TITLE_STRING);


      topTopLayout = new QVBoxLayout(this);


     pu_terminate = new QPushButton(tr("Cancel"));
     pu_terminate->setPalette(QColor(255, 255, 255));
     pu_terminate->setFixedSize(100,30);

    ch_automatiskt = new QCheckBox(tr("Check for updates on startup"));

    std::string conf = k->getConf("check_onstart");


    if (conf == "1")
        ch_automatiskt->setCheckState(Qt::Checked);
    if (conf == "0")
        ch_automatiskt->setCheckState(Qt::Unchecked);



    ted_info = new QLabel(tr("Contacting server..."),this);

    ted_animo = new QLabel(tr(""),this);

    timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(setValue()));
    timer->start(300);


    QFont font;
    font.setPixelSize(12);
    ted_info->setFont(font);
    ted_info->setTextInteractionFlags( Qt::LinksAccessibleByMouse );
    ted_info->setOpenExternalLinks(true);


    topTopLayout->addWidget(ted_info);
    topTopLayout->addWidget(ch_automatiskt);
    topTopLayout->addWidget(ted_animo);
    topTopLayout->addWidget(pu_terminate);



    connect( pu_terminate, SIGNAL( pressed() ) , this, SLOT( close_me() ) );
    connect( ch_automatiskt, SIGNAL(stateChanged ( int )) , this, SLOT( auto_check(int) ) );
    connect(http, SIGNAL(done(bool)), this, SLOT(handleResponse(bool)));





do_update();
}




void Update::close_me()
{
delete this;

}


void Update::do_update()
{




    http->setHost("bin.ceicer.com");

      QHttpRequestHeader header("GET", "/exifrename/version.txt");
   // QHttpRequestHeader header("GET", "/exifrename/version2.txt");
    header.setValue("Host", "bin.ceicer.com:80");

    http->request(header);



}

void Update::handleResponse(bool error)
{
    bool upd;
    QString ny_version;

    ny_version = http->readAll();
    ny_version = ny_version.simplified();

    if (ny_version.size() > 10 )
    {
        ted_info->setText(tr("Error message: Host found but\nthe requested version information was not found.\nPlease try again later"));
        pu_terminate->setText(tr("OK"));
        fortsatta = false;
        timer->stop();
        ted_animo->setText("");

    }
if (ny_version.size() <= 10 )
    fortsatta = true;

if (fortsatta)
{
    if (error==true)
    {
        ted_info->setText(tr("Error message: ")+http->errorString()+tr("\nPlease try again later"));
    pu_terminate->setText(tr("OK"));
    timer->stop();
    ted_animo->setText("");
    return;
    }
    if (error==false)
    {

        std::string language = k->getConf("language");

        QString applicationHomepage;
        if (language == "sv")
            applicationHomepage = APPLICATION_HOMEPAGE;
        else
            applicationHomepage = APPLICATION_HOMEPAGE_ENG;



    upd = jfr(ny_version);
        if (upd)
            {
            const QString text ="<html><p>"+tr("New version available: ")+"<b>"+ny_version+"</b><br>"+tr("Download it from")+"<br><a href="+applicationHomepage+"> EXIF ReName"+tr(" homepage")+"</a></p></html>";
            ted_info->setText(text);
            timer->stop();
            ted_animo->setText("");



            }
        if (! upd)
            {

                const QString text ="<html><p>"+tr("Your current version is the latest available: ")+"<b>"+VERSION+"</b><br>"+tr("Thank you for using")+" EXIF ReName</p></html>";
                ted_info->setText(text);
                timer->stop();
                ted_animo->setText("");



            }

    pu_terminate->setText(tr("OK"));
    }
}
}
bool Update::jfr(QString ny_version)
{
    int ny, nuvarande, possition_nuvarande, possition_ny;
    QString tmp_nuvarande, tmp_ny;
    QString nuvarande_version = VERSION;
    ny_version = ny_version.simplified();





    possition_nuvarande = nuvarande_version.indexOf(".");
    tmp_nuvarande = nuvarande_version.left(possition_nuvarande);
    nuvarande = tmp_nuvarande.toInt();

    possition_ny = ny_version.indexOf(".");
    tmp_ny = ny_version.left(possition_ny);
    ny = tmp_ny.toInt();


    if (ny > nuvarande)
        return true;

    nuvarande_version = nuvarande_version.remove(0,possition_nuvarande+1);
    ny_version = ny_version.remove(0,possition_ny+1);


    possition_nuvarande = nuvarande_version.indexOf(".");
    tmp_nuvarande = nuvarande_version.left(possition_nuvarande);
    nuvarande = tmp_nuvarande.toInt();

    possition_ny = ny_version.indexOf(".");
    tmp_ny = ny_version.left(possition_ny);
    ny = tmp_ny.toInt();

    if (ny > nuvarande)
        return true;

    nuvarande_version = nuvarande_version.remove(0,possition_nuvarande+1);
    ny_version = ny_version.remove(0,possition_ny+1);


    possition_nuvarande = nuvarande_version.indexOf(".");
    tmp_nuvarande = nuvarande_version.left(possition_nuvarande);
    nuvarande = tmp_nuvarande.toInt();

    possition_ny = ny_version.indexOf(".");
    tmp_ny = ny_version.left(possition_ny);
    ny = tmp_ny.toInt();

    if (ny > nuvarande)
        return true;


return false;


}



void Update::auto_check(const int i)
{
    switch (i)
    {
        case 0:
        k->setConf("check_onstart","0");
        break;
        case 2:
        k->setConf("check_onstart","1");
        break;
    }

}

void Update::startUppCheck(Config *kk)
{
k=kk;

http = new QHttp(this);


do_update();
connect(http, SIGNAL(done(bool)), this, SLOT(checkTystUpdate(bool)));

}


void Update::checkTystUpdate(bool fel)
{

if (fel)
{
    return;
}

    bool upd;
    QString ny_version;

    ny_version = http->readAll();
    ny_version = ny_version.simplified();

    if (ny_version.size() > 10 )
    {
        return;

    }
if (ny_version.size() <= 10 )
{

    upd = jfr(ny_version);
        if (upd)
            {

            std::string language = k->getConf("language");


            QString applicationHomepage;
            if (language == "sv")
                applicationHomepage = APPLICATION_HOMEPAGE;
            else
                applicationHomepage = APPLICATION_HOMEPAGE_ENG;

                const QString s = "<html><p><font color=\"red\"><b>"+tr("New version available!")+"</b></font><br>"+tr("Version ")+ny_version+"<br>"+tr("Please visit")+"</p><p><br><a href="+applicationHomepage+">EXIF ReName "+tr("homepage")+"</a><br></p></html>";

                QMessageBox::information(this, TITLE_STRING,s);
                return;

            }

}


}

void Update::setValue()
{
QString tmp;
int i;
const QString s1 = " ";
const QString s2 = "*";

    if (ted_animo->text().length() > 30)
        {
            ted_animo->setText("");

        }
    i = ted_animo->text().length();
    for (int j=0; j<i; j++)
        tmp.append(s1);


    QFont font;
    font.setPixelSize(52);

    ted_animo->setFont(font);

   ted_animo->setText(tmp+s2);

}

void Update::closeEvent(QCloseEvent *event)
{
delete this;
//event->accept();

}
