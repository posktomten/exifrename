/*
    EXIF ReName
    Copyright (C) 2011-2013 Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/


#include "mainwindow.h"
#include "exif.h"
#include "exif_w.h"
#include "exifw.h"
#include "config.h"
#include "help.h"
#include "checkupdate.h"
#include <string>
#include <sstream>
#include <fstream>
#include <QString>
#include <QStringList>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QButtonGroup>
#include <QRadioButton>
#include <QFileDialog>
#include <QLabel>
#include <QFileInfo>
#include <QDir>
#include <QMessageBox>
#include <QGridLayout>
#include <QFrame>
#include <QFile>
#include <QTextStream>
#include <QDateTime>
#include <QToolBar>
#include <QAction>
#include <QProgressBar>









MainWindow::MainWindow()
{

QString conf_file_name = QDir::toNativeSeparators("/exif_rename.conf");
k = new Config(conf_file_name,1);


  std::string initiera = k->getConf("vidd");
  int i_vidd,i_hojd,x_lage,y_lage;

    std::stringstream ss;

    ss << initiera;
    ss >> i_vidd;

    initiera = k->getConf("hojd");
    ss.clear();
   ss << initiera;
    ss >> i_hojd;

    initiera = k->getConf("x_lage");
    ss.clear();
   ss << initiera;
    ss >> x_lage;

    initiera = k->getConf("y_lage");
    ss.clear();
   ss << initiera;
    ss >> y_lage;

  setGeometry(y_lage,x_lage,i_vidd,i_hojd);

  setWindowTitle(tr(TITLE_STRING));
  w = new QWidget;

  setCentralWidget(w);
  QFont font;
  font.setPointSize(8);
  w->setFont(font);


  if (check_first_time()==false)
    exit(0);

createActions();
createMenus();
createToolBars();
createStatusBar();

topTopLayout = new QVBoxLayout(w);
topLayout = new QGridLayout();
layout = new QVBoxLayout;



copyLayout = new QGridLayout();




copyLayout->QLayout::setAlignment(Qt::AlignLeft);

ch_copy = new QCheckBox(tr("Copy (do not rename)"));
ch_copy->setFixedHeight(30);
pu_copy = new QPushButton(tr("Copy to..."));
pu_copy->setEnabled(false);
pu_copy->setFixedSize(100, 30);

copyLayout->addWidget(ch_copy,0,0);
copyLayout->addWidget(pu_copy,0,1);


la_copy_path = new QLabel;



topTopLayout->addLayout(copyLayout);
topTopLayout->addWidget(la_copy_path);



preserveLayout = new QGridLayout();
preserveLayout->setSpacing(0);
preserveLayout->setColumnMinimumWidth(0,230);
preserveLayout->setColumnStretch(1,1);

preserve_raLayout = new QVBoxLayout;
preserve_raLayout->setMargin(0);
preserve_raLayout->setSpacing(0);

ch_preserve = new QCheckBox(tr("Preserve original file name"));
preserve = new QButtonGroup;
ra_before_preserve = new QRadioButton(tr("Before date/time"));
ra_after_preserve = new QRadioButton(tr("After date/time"));
ra_after_preserve->setChecked(true);
preserve->addButton(ra_before_preserve);
preserve->addButton(ra_after_preserve);

preserve_raLayout->addWidget(ra_before_preserve);
preserve_raLayout->addWidget(ra_after_preserve);
preserveLayout->addWidget(ch_preserve,0,0);
preserveLayout->addLayout(preserve_raLayout,0,1);

// layout
layout->addLayout(preserveLayout);



insert_allLayout = new QVBoxLayout();
insert_allLayout->setSpacing(0);

insertLayout = new QGridLayout();
insertLayout->setColumnMinimumWidth(0,230);
insertLayout->setColumnStretch(1,1);

insertLayout->setMargin(0);
insertLayout->setSpacing(0);

insert_raLayout = new QVBoxLayout;
insert_raLayout->setMargin(0);
insert_raLayout->setSpacing(0);


ch_insert = new QCheckBox(tr("Insert new file name"));
insert = new QButtonGroup;
ra_before_insert = new QRadioButton(tr("Before date/time"));
ra_after_insert = new QRadioButton(tr("After date/time"));
ra_after_insert->setChecked(true);
insert->addButton(ra_before_insert);
insert->addButton(ra_after_insert);

insert_raLayout->addWidget(ra_before_insert);
insert_raLayout->addWidget(ra_after_insert);
insertLayout->addWidget(ch_insert,0,0);
insertLayout->addLayout(insert_raLayout,0,1);
li_insert = new QLineEdit();
li_insert->setFixedSize(400, 30);
li_insert->setMaxLength(32);


progress = new QProgressBar;

insert_allLayout->addLayout(insertLayout);
insert_allLayout->addWidget(li_insert);

// layout
layout->addLayout(insert_allLayout);
pu_go = new QPushButton(tr("Go!"));
pu_go->setFixedSize(100, 30);



ok_layout = new QVBoxLayout;
ok_layout->setSpacing(10);
ok_layout->addWidget(pu_go);
layout->addLayout(ok_layout);

topLayout->addLayout(layout,0,0);

// DISPLAY
displayLayout = new QVBoxLayout;
displayLayout->setSpacing(5);
te_disp = new QTextEdit;
te_disp->setReadOnly(true);
displayLayout->addWidget(te_disp);

pu_clear_log = new QPushButton(tr("Clear log"));
pu_clear_log ->setFixedSize(160, 30);
pu_save_log = new QPushButton(tr("Save log"));
pu_save_log ->setFixedSize(160, 30);
log_buttomLayout = new QHBoxLayout;
log_buttomLayout->addWidget(pu_clear_log);
log_buttomLayout->addWidget(pu_save_log);
displayLayout->addLayout(log_buttomLayout);

topLayout->addLayout(displayLayout,0,1);

topTopLayout->addLayout(topLayout);
topTopLayout->addWidget(progress);

layout->QLayout::setAlignment(Qt::AlignTop);
topLayout->setColumnStretch(1,1);




    connect( ch_insert, SIGNAL( pressed() ), this, SLOT( insertText() ) );
    connect( ra_before_insert, SIGNAL( pressed() ), this, SLOT( m_insert_beforeText() ) );
    connect( ra_after_insert, SIGNAL( pressed() ), this, SLOT( m_insert_afterText() ) );
    connect( ch_preserve, SIGNAL( pressed() ), this, SLOT( preserveText() ) );
    connect( ra_before_preserve, SIGNAL( pressed() ), this, SLOT( m_preserve_beforeText() ) );
    connect( ra_after_preserve, SIGNAL( pressed() ), this, SLOT( m_preserve_afterText() ) );

connect( ch_copy, SIGNAL( pressed() ), this, SLOT( copy() ) );
connect( pu_copy, SIGNAL( pressed() ), this, SLOT( copy_to() ) );
connect( li_insert, SIGNAL( textChanged(const QString&)  ) , this, SLOT( li_insert_change(const QString&) ) );
connect( pu_go, SIGNAL( pressed()  ) , this, SLOT( go() ) );
connect( pu_clear_log, SIGNAL( pressed()  ) , this, SLOT( clear_log() ) );
connect( pu_save_log, SIGNAL( pressed()  ) , this, SLOT( save_log() ) );

// Edit menu
   connect( editMenu, SIGNAL( aboutToShow()  ) , this, SLOT( edit_menu_aboutToShow() ) );

if (k->getConf("bugg_config") != "1")
{


    Createuser *cb = new Createuser;
        if ( cb->remove_buggConfig() )
           // QMessageBox::critical(this, TITLE_STRING,tr("You hav"));
            k->newConf("bugg_config","1");
    delete cb;

}


setConfig();
std::string str = k->getConf("check_onstart");
if (str=="1")
{
   // QMessageBox::critical(this, TITLE_STRING,tr("You hav"));
    startup_check();
}







}


MainWindow::~MainWindow()
{



QRect rect;
rect=this->geometry();



int wi = rect.width();
int he = rect.height();
int y_lage=rect.x();
int x_lage=rect.y();

std::stringstream ss;
std::string str;
ss << wi;
ss >> str;

k->setConf("vidd",str);
ss.clear();

ss << he;
ss >> str;

k->setConf("hojd",str);


ss.clear();

ss << x_lage;
ss >> str;
k->setConf("x_lage",str);

ss.clear();
ss << y_lage;
ss >> str;
k->setConf("y_lage",str);



//delete this;
delete k;
}

// ACTIONS
void MainWindow::createActions()
{
    QFont font;
    font.setPointSize(8);

    openAct = new QAction(QIcon(":/images/open.xpm"), tr("&Open..."), this);
    openAct->setShortcut(tr("Ctrl+O"));
    openAct->setStatusTip(tr("Open jpeg image(s)"));
     openAct->setFont(font);
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    renameAct = new QAction(tr("&Rename"), this);
    renameAct->setShortcut(tr("Ctrl+R"));
    renameAct->setStatusTip(tr("Rename original file(s)"));
    renameAct->setFont(font);
    connect(renameAct, SIGNAL(triggered()), this, SLOT(rename()));


    copy_toAct = new QAction(QIcon(":/images/save_as.xpm"),tr("&Copy to..."), this);
    copy_toAct->setShortcut(tr("Ctrl+C"));
    copy_toAct->setStatusTip(tr("Copy original file(s) with new name to folder..."));
    copy_toAct->setFont(font);
    connect(copy_toAct, SIGNAL(triggered()), this, SLOT(copy_to()));




    exitAct = new QAction(QIcon(":/images/exit.xpm"),tr("E&xit"), this);
    exitAct->setShortcut(tr("Ctrl+Q"));
    exitAct->setStatusTip(tr("Exit application"));
    exitAct->setFont(font);
    connect(exitAct, SIGNAL(triggered()), this, SLOT(close()));




        //Edit


        createFolderAct = new QAction(tr("Create folders (yyyy_mm_dd)"), this);
        createFolderAct->setCheckable(true);
        createFolderAct->setStatusTip(tr("Create folders according to file creation date and copy the files into these folders"));
        createFolderAct->setFont(font);
        connect(createFolderAct, SIGNAL(triggered()), this, SLOT(createFolder()));


        dontAddFileNameAct = new QAction(tr("No name exept date/time"), this);
        dontAddFileNameAct->setCheckable(true);
        dontAddFileNameAct->setStatusTip(tr("File name is only date/time"));
        dontAddFileNameAct->setFont(font);
        connect(dontAddFileNameAct, SIGNAL(triggered()), this, SLOT(dontAddFileName()));



        preserve_beforeAct = new QAction(tr("Before date/time"), this);
        preserve_beforeAct->setCheckable(true);
        preserve_beforeAct->setStatusTip(tr("Preserve original file name before date/time"));
        preserve_beforeAct->setFont(font);
        connect(preserve_beforeAct, SIGNAL(triggered()), this, SLOT(m_preserve_beforeText()));

        preserve_afterAct = new QAction(tr("After date/time"), this);
        preserve_afterAct->setCheckable(true);
        preserve_afterAct->setStatusTip(tr("Preserve original file name after date/time"));
        preserve_afterAct->setFont(font);
        connect(preserve_afterAct, SIGNAL(triggered()), this, SLOT(m_preserve_afterText()));


        insert_beforeAct = new QAction(tr("Before date/time"), this);
        insert_beforeAct->setCheckable(true);
        insert_beforeAct->setStatusTip(tr("Insert new file name before date/time"));
        insert_beforeAct->setFont(font);
        connect(insert_beforeAct, SIGNAL(triggered()), this, SLOT(m_insert_beforeText()));

        insert_afterAct = new QAction(tr("After date/time"), this);
        insert_afterAct->setCheckable(true);
        insert_afterAct->setStatusTip(tr("Insert new file name after date/time"));
        insert_afterAct->setFont(font);
        connect(insert_afterAct, SIGNAL(triggered()), this, SLOT(m_insert_afterText()));


        edit_exifAct = new QAction(tr("Edit date and time..."), this);
        edit_exifAct->setStatusTip(tr("Edit date and time..."));
        edit_exifAct->setFont(font);
        connect(edit_exifAct, SIGNAL(triggered()), this, SLOT(edit_exif()));


        //HELP
        helpAct = new QAction(QIcon(":/images/help.xpm"),tr("Help..."), this);
        helpAct->setStatusTip(tr("Help for application"));
        helpAct->setFont(font);
        connect(helpAct, SIGNAL(triggered()), this, SLOT(help()));

        aboutAct = new QAction(tr("About..."), this);
        aboutAct->setStatusTip(tr("About application"));
        aboutAct->setFont(font);
        connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

        aboutQtAct = new QAction(QIcon(":/images/qt.xpm"),tr("About Qt..."), this);
       aboutQtAct->setStatusTip(tr("About Qt C++ Application Development Framework"));
       aboutQtAct->setFont(font);
       connect(aboutQtAct, SIGNAL(triggered()), this, SLOT(aboutQt()));

        check_updateAct = new QAction(tr("Check for updates..."), this);
        check_updateAct->setStatusTip(tr("Check for new versions of EXIF ReName"));
        check_updateAct->setFont(font);
        connect(check_updateAct, SIGNAL(triggered()), this, SLOT(check_update()));


        //LANGUAGE
        engAct = new QAction(QIcon(":/images/eng.xpm"),tr("English"), this);
        engAct->setStatusTip(tr("English language"));
        engAct->setFont(font);
        connect(engAct, SIGNAL(triggered()), this, SLOT(engelska()));

        // italienska
        itAct = new QAction(QIcon(":/images/it.xpm"),tr("Italian"), this);
        itAct->setStatusTip(tr("Italian language"));
        itAct->setFont(font);
        connect(itAct, SIGNAL(triggered()), this, SLOT(italienska()));

        // tyska
        itAct = new QAction(QIcon(":/images/de.xpm"),tr("German"), this);
        itAct->setStatusTip(tr("German language"));
        itAct->setFont(font);
        connect(itAct, SIGNAL(triggered()), this, SLOT(tyska()));

        // ryska
        ruAct = new QAction(QIcon(":/images/russian.xpm"),tr("Russian"), this);
        ruAct->setStatusTip(tr("Russian language"));
        ruAct->setFont(font);
        connect(ruAct, SIGNAL(triggered()), this, SLOT(ryska()));

        // svenska
        svAct = new QAction(QIcon(":/images/sv.xpm"),tr("Swedish"), this);
        svAct->setStatusTip(tr("Swedish language"));
        svAct->setFont(font);
        connect(svAct, SIGNAL(triggered()), this, SLOT(svenska()));

        // spanska
        spAct = new QAction(QIcon(":/images/spain.xpm"),tr("Spanish"), this);
        spAct->setStatusTip(tr("Spanish language"));
        spAct->setFont(font);
        connect(spAct, SIGNAL(triggered()), this, SLOT(spanska()));


        //TOOLS
        delete_personal_dataAct = new QAction(tr("Delete personal settings"), this);
        delete_personal_dataAct->setStatusTip(tr("Remove your personal settings"));
        delete_personal_dataAct->setFont(font);
        connect(delete_personal_dataAct, SIGNAL(triggered()), this, SLOT(delete_personal_data()));

        fileNamePatternAct = new QAction(tr("Specify file name pattern..."), this);
        fileNamePatternAct->setStatusTip(tr("Specify file name pattern..."));
        fileNamePatternAct->setFont(font);
        connect(fileNamePatternAct, SIGNAL(triggered()), this, SLOT(fileNamePattern()));

        //TOOLBAR
  toolbarAct = new QAction(this);
  toolbarAct->setFont(font);

  iconsOnlyAct = new QAction(tr("Icons only"), this);
  iconsOnlyAct->setStatusTip(tr("Display only icons in the toolbar"));
  connect(iconsOnlyAct, SIGNAL(triggered()), this, SLOT(icons_only()));

  textOnlyAct = new QAction(tr("Text only"), this);
  textOnlyAct->setStatusTip(tr("Display only text in the toolbar"));
  connect(textOnlyAct, SIGNAL(triggered()), this, SLOT(text_only()));

  textAlongsideIconsAct = new QAction(tr("Text alongside icons"), this);
  textAlongsideIconsAct->setStatusTip(tr("Display text alongside icons in the toolbar"));
  connect(textAlongsideIconsAct, SIGNAL(triggered()), this, SLOT(icons_text_alongside()));


  textUnderIconsAct = new QAction(tr("Text under icons"), this);
  textUnderIconsAct->setStatusTip(tr("Display text under icons in the toolbar"));
  connect(textUnderIconsAct, SIGNAL(triggered()), this, SLOT(icons_text_under()));

  noToolbarAct = new QAction(tr("No toolbar"), this);
  noToolbarAct->setStatusTip(tr("No toolbar will be displayed"));
  connect(noToolbarAct, SIGNAL(triggered()), this, SLOT(no_toolbar()));

}

// MENYER
void MainWindow::createMenus()
{
    QFont font;
    font.setPointSize(8);
 menuBar()->setFont(font);

  fileMenu = menuBar()->addMenu(tr("&File"));
  fileMenu->addAction(openAct);
  fileMenu->addAction(renameAct);
  fileMenu->addAction(copy_toAct);
  fileMenu->addSeparator();
  fileMenu->addAction(exitAct);


  editMenu = menuBar()->addMenu(tr("&Edit"));
  editMenu->addAction(createFolderAct);
  editMenu->addAction(dontAddFileNameAct);
  editMenu->setFont(font);


        preserveMenu = editMenu->addMenu(tr("&Preserve original file name"));
            preserveMenu->addAction(preserve_beforeAct);
            preserveMenu->addAction(preserve_afterAct);

        insertMenu = editMenu->addMenu(tr("&Insert new file name"));
            insertMenu->addAction(insert_beforeAct);
            insertMenu->addAction(insert_afterAct);
editMenu->addSeparator();
editMenu->addAction(edit_exifAct);


  wievMenu = menuBar()->addMenu(tr("&View"));
  wievMenu->setFont(font);
       toggleToolbarMenu = wievMenu->addMenu(tr("Toolbar"));
        toggleToolbarMenu->setFont(font);
  toggleToolbarMenu->addAction(iconsOnlyAct);
  toggleToolbarMenu->addAction(textOnlyAct);
  toggleToolbarMenu->addAction(textAlongsideIconsAct);
  toggleToolbarMenu->addAction(textUnderIconsAct);
  toggleToolbarMenu->addSeparator();
  toggleToolbarMenu->addAction(noToolbarAct);


  sprakMenu = menuBar()->addMenu(tr("&Language"));
          sprakMenu->addAction(engAct);
          sprakMenu->addAction(spAct);
          sprakMenu->addAction(itAct);
          sprakMenu->addAction(ruAct);
          sprakMenu->addAction(svAct);





    toolsMenu = menuBar()->addMenu(tr("&Tools"));
    toolsMenu->addAction(delete_personal_dataAct);
    toolsMenu->addAction(fileNamePatternAct);


    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(helpAct);
    helpMenu->addAction(aboutAct);
  helpMenu->addAction(aboutQtAct); //About Qt
    helpMenu->addAction(check_updateAct);


}
// TOOLBAR


void MainWindow::delete_personal_data()
{

        switch(QMessageBox::question(this, TITLE_STRING,
                                    tr("Do you want to remove your personal settings?\nThis is an appropriate action if you like to uninstall this application.\n")+TITLE_STRING+tr(" will exit after your personal data is deleted."),
                                    tr("Yes"),
                                    tr("No"), 0,2))
    {
        case 0:
    {
    Createuser *cr = new Createuser;

    if (cr->removeDir())
        {
        QMessageBox::information(this, TITLE_STRING,tr("Your personal settings are deleted and\n ")+TITLE_STRING+tr(" will exit"));
        delete cr;
        exit(0);

        }
    else
        QMessageBox::information(this, TITLE_STRING,tr("Your personal settings cannot be found.\nNew settings will be saved then you restart the program."));

    delete cr;
        break;
    }
    default:

        break;
        }




}

void MainWindow::fileNamePattern()
{
    QFont font;
    font.setPointSize(8);
    FilePattern *FP = new FilePattern;
    FP->setFont(font);
    FP->show();
    FP->setWindowTitle(TITLE_STRING);
}


void MainWindow::createToolBars()
{



    fileToolBar = addToolBar(tr("File"));
     fileToolBar->setMovable(false);
     fileToolBar->setWindowTitle(tr("Toolbar"));


    fileToolBar->addAction(openAct);
    fileToolBar->addAction(copy_toAct);
    fileToolBar->addAction(exitAct);
     fileToolBar->addAction(helpAct);


}

// SLOTTS

void MainWindow::edit_menu_aboutToShow()
{
        QString fs = displayFolderSeparators();

       // createFolderAct = new QAction(tr("Create folders (yyyy")+fs+tr("mm")+fs+tr("dd)"), this);
        createFolderAct->setText(tr("Create folders (yyyy")+fs+tr("mm")+fs+tr("dd)"));

}


void MainWindow::open()
{
    progress->reset();
     QString sokvag;
     std::string s = k->getConf("open_path");
    QString qs = QString::fromStdString(s);
     QFileInfo info(qs);
      if (info.isReadable())
            sokvag = qs;
      else
      {
          sokvag = QDir::homePath();
      }

  inputFileDialog = new QFileDialog(this, tr("Open File"),sokvag);
  inputFileDialog->setFileMode(QFileDialog::ExistingFiles);
  inputFileDialog->setAcceptMode(QFileDialog::AcceptOpen);

  QString conf_file_name = QDir::toNativeSeparators("/exif_rename3.conf");
  Config *k4 = new Config(conf_file_name,2);
  std::string monster_s = k4->getConf("extension_name");




  QString monster = QString::fromStdString(monster_s);
  //QMessageBox::critical(this, TITLE_STRING,monster);
  if (monster=="nothing")
  {
      monster=STANDARD_EXTENSION;


  }

  if (k4->getConf("current_extension") == "nothing")
        k4->newConf("current_extension","");

  if (k4->getConf("current_filter") == "nothing")
        k4->newConf("current_filter","");
QString m;
  QStringList filters;
  if (monster[monster.size()] != ';')
  {

      m="*.";
      m = m + monster;
  m.replace(";"," *.");
      monster = monster+';';
  }
  int sep = monster.indexOf(';');
  QString del_monster;

  while (sep > 0)
  {
      //QMessageBox::critical(this, TITLE_STRING,monster);
    del_monster = monster.mid(0,sep);

    filters << tr("Image files")+" (*."+del_monster+")";

    monster.remove(0,sep+1);
    sep = monster.indexOf(';');
}

  //  filters << tr("Image files ")+"("+m+")";
   //   filters << tr("All files")+" (*.*)";

  inputFileDialog->setNameFilters(filters);
  std::string default_filter = k4->getConf("current_filter");

      inputFileDialog->selectFilter(QString::fromStdString(default_filter));
  inputFileDialog->setViewMode(QFileDialog::Detail);

  if (inputFileDialog->exec())
  {

            te_disp->clear();
            QDir dir;
            QString str;
            dir = inputFileDialog->directory();
            QString directoryName = dir.path();
            QFileInfo info2(directoryName);

            if (! info2.isReadable() )
            {
                QMessageBox::critical(this, TITLE_STRING,tr("You do not have enough permissions to read\n")+directoryName);
                delete k4;
                return;
            }


            fileNames = inputFileDialog->selectedFiles();
            QString qs_qurrent_extension = inputFileDialog->selectedNameFilter();
            k4->setConf("current_filter",qs_qurrent_extension.toStdString());
            int start = qs_qurrent_extension.indexOf("*.");
            qs_qurrent_extension.chop(1);
            qs_qurrent_extension = qs_qurrent_extension.mid(start+2);
            k->setConf("open_path",directoryName.toStdString());

           //QMessageBox::critical(this, "qs_qurrent_extension",qs_qurrent_extension);


           k4->setConf("current_extension",qs_qurrent_extension.toStdString());


           delete k4;
            selectedFiles(&directoryName);
                        int antal =fileNames.size();
                        statusBar()->showMessage(str.setNum(antal)+tr(" File(s) selected"));
                        te_disp->append(QDir::toNativeSeparators(str.setNum(antal)+tr(" File(s) selected")));
  }

}

void MainWindow::rename()
{
    ch_copy->setCheckState(Qt::Unchecked);
    pu_copy->setEnabled(false);
     la_copy_path->setText(tr("The original file will be renamed"));
     k->setConf("copy","0");
}

void MainWindow::copy()
{
check_copy();
}

void MainWindow::dontAddFileName()
{
    ch_preserve->setCheckState(Qt::Unchecked);
    ch_insert->setCheckState(Qt::Unchecked);
    dontAddFileNameAct->setChecked(true);
    preserve_beforeAct->setChecked(false);
    preserve_afterAct->setChecked(false);
    insert_beforeAct->setChecked(false);
    insert_afterAct->setChecked(false);
    k->setConf("preserve","0");
    k->setConf("insert","0");
}

void MainWindow::createFolder()
{
    if (!createFolderAct->isChecked() )
        k->setConf("create_folder","0");
    if (createFolderAct->isChecked())
        k->setConf("create_folder","1");
}


void MainWindow::insertText()
{
    if ( !ch_insert->isChecked()  )
    {
        ch_preserve->setCheckState(Qt::Unchecked);
        k->setConf("insert","1");
        k->setConf("preserve","0");
    }
   if ( ch_insert->isChecked()  )
    {
        k->setConf("insert","0");
    }


    synkronisera(true,false);

}

void MainWindow::m_insert_beforeText()
{
    ch_preserve->setCheckState(Qt::Unchecked);
    ch_insert->setCheckState(Qt::Checked);
    ra_before_insert->setChecked(true);
    dontAddFileNameAct->setChecked(false);
    preserve_beforeAct->setChecked(false);
    preserve_afterAct->setChecked(false);
    insert_beforeAct->setChecked(true);
    insert_afterAct->setChecked(false);

    k->setConf("preserve","0");
    k->setConf("insert","1");
    k->setConf("insert_before","1");

}

void MainWindow::m_insert_afterText()
{
    ch_preserve->setCheckState(Qt::Unchecked);
    ch_insert->setCheckState(Qt::Checked);
    ra_after_insert->setChecked(true);

    dontAddFileNameAct->setChecked(false);
    preserve_beforeAct->setChecked(false);
    preserve_afterAct->setChecked(false);
    insert_beforeAct->setChecked(false);
    insert_afterAct->setChecked(true);

    k->setConf("preserve","0");
    k->setConf("insert","1");
    k->setConf("insert_before","0");
}

void MainWindow::preserveText()
{
    if ( !ch_preserve->isChecked()  )
    {
        ch_insert->setCheckState(Qt::Unchecked);
        k->setConf("insert","0");
        k->setConf("preserve","1");
    }
   if ( ch_preserve->isChecked()  )
    {
        k->setConf("preserve","0");
    }

    synkronisera(false,true);


}

void MainWindow::m_preserve_beforeText()
{
    ch_preserve->setCheckState(Qt::Checked);
    ch_insert->setCheckState(Qt::Unchecked);
    ra_before_preserve->setChecked(true);
    dontAddFileNameAct->setChecked(false);
    preserve_beforeAct->setChecked(true);
    preserve_afterAct->setChecked(false);
    insert_beforeAct->setChecked(false);
    insert_afterAct->setChecked(false);

    k->setConf("preserve","1");
    k->setConf("insert","0");
    k->setConf("preserve_before","1");
}

void MainWindow::m_preserve_afterText()
{
    ch_preserve->setCheckState(Qt::Checked);
    ch_insert->setCheckState(Qt::Unchecked);
    ra_after_preserve->setChecked(true);
    dontAddFileNameAct->setChecked(false);
    preserve_beforeAct->setChecked(false);
    preserve_afterAct->setChecked(true);
    insert_beforeAct->setChecked(false);
    insert_afterAct->setChecked(false);

    k->setConf("preserve","1");
    k->setConf("insert","0");
    k->setConf("preserve_before","0");
}

void MainWindow::copy_to()
{
     QString sokvag;
     std::string s = k->getConf("copy_path");
    QString qs = QString::fromStdString(s);
     QFileInfo filInfo(qs);
      if (filInfo.isReadable())
          sokvag = qs;
        else
         sokvag = QDir::homePath();



  outputFileDialog = new QFileDialog(this, tr("Copy pictures to folder"),sokvag);
  outputFileDialog->setFileMode(QFileDialog::DirectoryOnly);
  outputFileDialog->setAcceptMode(QFileDialog::AcceptSave);

  outputFileDialog->setViewMode(QFileDialog::Detail);

  if (outputFileDialog->exec())
  {


     ch_copy->setCheckState(Qt::Checked);
     pu_copy->setEnabled(true);

      QStringList files = outputFileDialog->selectedFiles();
      if (!files.isEmpty())
        {
            QString directoryName = files[0];

            //ipac
            QFileInfo filInfo(directoryName);
            if (! filInfo.isWritable() )
            {
                QMessageBox::critical(this, TITLE_STRING,tr("You have not enough permissions\nto write to ")+QDir::toNativeSeparators(directoryName));
                return;
            }
                        else
            {
                k->setConf("copy_path",directoryName.toStdString());
                                directoryName = QDir::toNativeSeparators(tr("Copy to: ")+directoryName);
                la_copy_path->setText(directoryName);
            }

        }
  }


}

void MainWindow::li_insert_change(const QString&)
{
    QString s = li_insert->text();
    s=s.trimmed();
    std::string str = s.toStdString();
        k->setConf("file_name",str);


}

void MainWindow::go()
{
    QString conf_file_name = QDir::toNativeSeparators("/exif_rename3.conf");
    Config *k3 = new Config(conf_file_name,2);
    std::string folder_separator_s = k3->getConf("folder_separator");
    QString folder_separator = QString::fromStdString(folder_separator_s);
    std::string extension_s = k3->getConf("extension");
    QString extension;
    std::string onlytime_s = k3->getConf("onlytime");



    std::string current_extension_s = k3->getConf("current_extension");
     delete k3;

    QString current_extension = QString::fromStdString(current_extension_s);
//QMessageBox::critical(this, "GO",current_extension);


    bool onlytime = false;
    if (onlytime_s  == "1")
        onlytime = true;


    if (folder_separator=="none")
        folder_separator="";
    if (folder_separator=="space")
        folder_separator=" ";

    if (current_extension=="nothing")
        current_extension=STANDARD_EXTENSION;

    if (extension_s=="lowercase")
        extension="."+current_extension.toLower();
    if (extension_s=="uppercase")
        extension="."+current_extension.toUpper();

//QMessageBox::critical(this, TITLE_STRING,onlytime);
/*
    if ((k->getConf("create_folder") == "1") && (k->getConf("copy") == "0"))
    {




        switch(QMessageBox::question(this, TITLE_STRING,
                                    tr("You have chosen to create folders named after creation date and copy the pictures into this folders. This is not possible at the same time as you rename your pictures.\n\nIf you continue no folders will be created and your pictures will be renamed.\n\nDo you want to continue?"),
                                    tr("Yes"),
                                    tr("No"),0,1))
        {
                case 0: // The user clicked the "Retry again button" or pressed Enter
                break;
                case 1:
                return;

             }


    }

*/



    std::string copy = k->getConf("copy");
    QString type_of;
    if (copy == "0")
        type_of = tr("rename");
    if (copy == "1")
        type_of = tr("copy");

        if (fileNames.empty())
        {

                 QMessageBox::information(this, TITLE_STRING,
                   tr("You have not selected any file(s).\n"
                      "Please select file(s) to ")+type_of);

                 return;

        }



    Exif *wert;
    wert = new Exif;
    int misslyckade=0;

    char *ch = new char[20];
    char *filnamn = new char[300];

    char *info = new char[50];
    QString s, new_filnamn,new_filnamn_folder,namnet,path,str,tmp,folder_datum_tmp,folder_datum;
    int ind=0,lika=0,langd=0;
    std::string open_path = k->getConf("open_path");
    std::string copy_path = k->getConf("copy_path");

        if (!test_copy_path(copy_path) && copy == "1")
        {

             QMessageBox::critical(this, TITLE_STRING,
                   tr("Error in destination path\n"
                      "Please select another output directory!"));

                 k->setConf("copy_path","");
                 la_copy_path->setText(tr("Copy to: "));
                 return;

        }

    std::string preserve = k->getConf("preserve");
    std::string preserve_before = k->getConf("preserve_before");
    std::string insert = k->getConf("insert");
    std::string insert_before = k->getConf("insert_before");
    std::string file_name = k->getConf("file_name");

        if (copy == "0")
           path = QString::fromStdString(open_path);
        if (copy == "1")
           path = QString::fromStdString(copy_path);

        QStringList::const_iterator constIterator;

          int antal_filer =  fileNames.size();
          progress->setMinimum(0);
          progress->setMaximum(antal_filer);
          int klara=0;




   for (constIterator = fileNames.constBegin(); constIterator != fileNames.constEnd();++constIterator)
    {

    QByteArray ba = (*constIterator).toLatin1();
    strcpy(filnamn,ba.data());

        lika=0;

            if (! wert->getExif(filnamn,ch,true,info) )
                {

                        s=s.mid(0,19);

                    QString s = QString::fromLatin1(info);


                    te_disp->append(QDir::toNativeSeparators("<font color=red>"+s+stripNames(constIterator)+"</font>"));
                    progress->setValue(++klara);
                    misslyckade++;
                    continue;


                }


folder_datum_tmp = ch;
folder_datum = folder_datum_tmp.left(4)+folder_separator+folder_datum_tmp.mid(5,2)+folder_separator+folder_datum_tmp.mid(8,2);
QDir ny_folder;


      s = QString::fromAscii(ch);
     // s.remove(QChar('x'));
      // ipac Workaround for konstigt fel
        s.replace(QRegExp("[a-zA-Z]"), "");

if (onlytime==false)
        s=s.mid(0,19);
else
        s=s.mid(11,8);



        if (preserve == "0" && insert == "0")
         {
                new_filnamn = path+"/"+s+extension;
                new_filnamn_folder = path+"/"+folder_datum+"/"+s+extension;

                    if (copy == "0")
                    {

                        if (k->getConf("create_folder") == "1")
                            {
                                ind = new_filnamn_folder.lastIndexOf(extension,-1);

                                if (! ny_folder.exists(path+"/"+folder_datum))
                                {
                                    if (! ny_folder.mkdir(path+"/"+folder_datum ))
                                    {
                                      QMessageBox::critical(this, TITLE_STRING,tr("Unable to create directory \n")+path+"/"+folder_datum+tr("\ncheck your permisions"));
                                        exit(0);
                                    }


                                }


                                 while ( !QFile::copy( *constIterator, new_filnamn_folder ) )
                                    {
                                        langd = new_filnamn_folder.size();
                                        new_filnamn_folder.remove(ind,langd);
                                        new_filnamn_folder+="_"+str.setNum(++lika)+extension;


                                    }
                                            disp_log(constIterator,new_filnamn_folder,path);
                                             progress->setValue(++klara);

                                             //QMessageBox::critical(this, TITLE_STRING,*constIterator);

                                             if ( ! QFile::remove(QDir::toNativeSeparators(*constIterator)))
                                                QMessageBox::critical(this, TITLE_STRING,tr("Unable to delete:\n")+QDir::toNativeSeparators(*constIterator));
                            }


                        if (k->getConf("create_folder") == "0")
                        {
                        ind = new_filnamn.lastIndexOf(extension,-1);
                                                while ( !QFile::rename( *constIterator, new_filnamn ) )
                                {
                                    langd = new_filnamn.size();
                                    new_filnamn.remove(ind,langd);
                                    new_filnamn+="_"+str.setNum(++lika)+extension;
                                }
                            disp_log(constIterator,new_filnamn);
                             progress->setValue(++klara);
                         }

                    }
                    if (copy == "1")
                    {
                        if (k->getConf("create_folder") == "0")
                            {
                                 ind = new_filnamn.lastIndexOf(extension,-1);
                                     while ( !QFile::copy( *constIterator, new_filnamn ) )
                                        {
                                            langd = new_filnamn.size();
                                            new_filnamn.remove(ind,langd);
                                            new_filnamn+="_"+str.setNum(++lika)+extension;
                                        }
                                    disp_log(constIterator,new_filnamn,path);
                                    progress->setValue(++klara);
                            }

                            if (k->getConf("create_folder") == "1")
                                {
                                    ind = new_filnamn_folder.lastIndexOf(extension,-1);

                                    if (! ny_folder.exists(path+"/"+folder_datum))
                                    {
                                        if (! ny_folder.mkdir(path+"/"+folder_datum ))
                                        {
                                          QMessageBox::critical(this, TITLE_STRING,tr("Unable to create directory \n")+path+"/"+folder_datum+tr("\ncheck your permisions"));
                                            exit(0);
                                        }


                                    }


                                     while ( !QFile::copy( *constIterator, new_filnamn_folder ) )
                                        {
                                            langd = new_filnamn_folder.size();
                                            new_filnamn_folder.remove(ind,langd);
                                            new_filnamn_folder+="_"+str.setNum(++lika)+extension;
                                        }
                                                disp_log(constIterator,new_filnamn_folder,path);
                                                 progress->setValue(++klara);
                                }
                        }

         }
         if (preserve == "1")
         {

                        ind = ((*constIterator).trimmed()).lastIndexOf('/',-1);
                        namnet = (*constIterator).mid(ind+1);
                        ind = namnet.lastIndexOf(extension,-1,Qt::CaseInsensitive);
                        namnet = namnet.mid(0,ind);

                if (preserve_before == "0")
                {
                                    new_filnamn = path+"/"+s+"_"+namnet+extension;
                    new_filnamn_folder = path+"/"+folder_datum+"/"+s+namnet+extension;
                }
                if (preserve_before == "1")
                {
                                    new_filnamn = path+"/"+namnet+"_"+s+extension;
                    new_filnamn_folder = path+"/"+folder_datum+"/"+namnet+s+extension;
                }

                if (copy == "0")
                {


                    if (k->getConf("create_folder") == "1")
                        {
                            ind = new_filnamn_folder.lastIndexOf(extension,-1);

                            if (! ny_folder.exists(path+"/"+folder_datum))
                            {
                                if (! ny_folder.mkdir(path+"/"+folder_datum ))
                                {
                                  QMessageBox::critical(this, TITLE_STRING,tr("Unable to create directory \n")+path+"/"+folder_datum+tr("\ncheck your permisions"));
                                    exit(0);
                                }


                            }


                             while ( !QFile::copy( *constIterator, new_filnamn_folder ) )
                                {
                                    langd = new_filnamn_folder.size();
                                    new_filnamn_folder.remove(ind,langd);
                                    new_filnamn_folder+="_"+str.setNum(++lika)+extension;


                                }
                                        disp_log(constIterator,new_filnamn_folder,path);
                                         progress->setValue(++klara);

                                         //QMessageBox::critical(this, TITLE_STRING,*constIterator);

                                         if ( ! QFile::remove(QDir::toNativeSeparators(*constIterator)))
                                            QMessageBox::critical(this, TITLE_STRING,tr("Unable to delete:\n")+QDir::toNativeSeparators(*constIterator));
                        }
                    if (k->getConf("create_folder") == "0")
                    {
                    QFile::rename( *constIterator, new_filnamn );
                    disp_log(constIterator,new_filnamn);
                    progress->setValue(++klara);
                    }
                }
                if (copy == "1")
                {
                        if (k->getConf("create_folder") == "0")
                            {
                                 ind = new_filnamn.lastIndexOf(extension,-1);


                                 while ( !QFile::copy( *constIterator, new_filnamn ) )
                                        {
                                            langd = new_filnamn.size();
                                            new_filnamn.remove(ind,langd);
                                            new_filnamn+="_"+str.setNum(++lika)+extension;
                                        }
                                    disp_log(constIterator,new_filnamn,path);
                                    progress->setValue(++klara);
                            }

                            if (k->getConf("create_folder") == "1")
                                {
                                    ind = new_filnamn_folder.lastIndexOf(extension,-1);

                                    if (! ny_folder.exists(path+"/"+folder_datum))
                                    {
                                        if (! ny_folder.mkdir(path+"/"+folder_datum ))
                                        {
                                          QMessageBox::critical(this, TITLE_STRING,tr("Unable to create directory \n")+path+"/"+folder_datum+tr("\ncheck your permisions"));
                                            exit(0);
                                        }


                                    }


                                     while ( !QFile::copy( *constIterator, new_filnamn_folder ) )
                                        {
                                            langd = new_filnamn_folder.size();
                                            new_filnamn_folder.remove(ind,langd);
                                            new_filnamn_folder+="_"+str.setNum(++lika)+extension;
                                        }
                                                disp_log(constIterator,new_filnamn_folder,path);
                                                 progress->setValue(++klara);
                                }
                }


         }
         if (insert == "1")
         {


               namnet = QString::fromStdString(file_name);
            if (namnet == "")
                {
                    QMessageBox::warning(this, TITLE_STRING,tr("Indicate a new file name, please!"));
                    return;
                }




                if (insert_before == "0")
                {
                    new_filnamn = path+"/"+s+namnet+extension;
                    new_filnamn_folder = path+"/"+folder_datum+"/"+s+namnet+extension;

                }
                if (insert_before == "1")
                {
                    new_filnamn = path+"/"+namnet+s+extension;
                    new_filnamn_folder = path+"/"+folder_datum+"/"+namnet+s+extension;
                }

                if (copy == "0")
                {

                    if (k->getConf("create_folder") == "1")
                        {

                            ind = new_filnamn_folder.lastIndexOf(extension,-1);

                            if (! ny_folder.exists(path+"/"+folder_datum))
                            {
                                if (! ny_folder.mkdir(path+"/"+folder_datum ))
                                {
                                  QMessageBox::critical(this, TITLE_STRING,tr("Unable to create directory \n")+path+"/"+folder_datum+tr("\ncheck your permisions"));
                                    exit(0);
                                }


                            }

                            while ( !QFile::copy( *constIterator, new_filnamn_folder ) )
                                {
                                    langd = new_filnamn_folder.size();
                                    new_filnamn_folder.remove(ind,langd);
                                    new_filnamn_folder+="_"+str.setNum(++lika)+extension;


                                }
                                        disp_log(constIterator,new_filnamn_folder,path);
                                         progress->setValue(++klara);

                                        // QMessageBox::critical(this, TITLE_STRING,QDir::toNativeSeparators(*constIterator));
                                        // QFile::remove(QDir::toNativeSeparators(*constIterator));
                                        if ( ! QFile::remove(QDir::toNativeSeparators(*constIterator)))
                                           QMessageBox::critical(this, TITLE_STRING,tr("Unable to delete:\n")+QDir::toNativeSeparators(*constIterator));
                        }
                    if (k->getConf("create_folder") == "0")
                    {
                    ind = new_filnamn.lastIndexOf(extension,-1);
                                        while ( !QFile::rename( *constIterator, new_filnamn ) )
                        {
                            langd = new_filnamn.size();
                            new_filnamn.remove(ind,langd);
                            new_filnamn+="_"+str.setNum(++lika)+extension;
                        }
                    disp_log(constIterator,new_filnamn);
                    progress->setValue(++klara);
                   }
                }
                if (copy == "1")
                {
                        if (k->getConf("create_folder") == "0")
                            {
                                 ind = new_filnamn.lastIndexOf(extension,-1);
                                     while ( !QFile::copy( *constIterator, new_filnamn ) )
                                        {
                                            langd = new_filnamn.size();
                                            new_filnamn.remove(ind,langd);
                                            new_filnamn+="_"+str.setNum(++lika)+extension;
                                        }
                                    disp_log(constIterator,new_filnamn,path);
                                    progress->setValue(++klara);
                            }

                            if (k->getConf("create_folder") == "1")
                                {
                                    ind = new_filnamn_folder.lastIndexOf(extension,-1);

                                    if (! ny_folder.exists(path+"/"+folder_datum))
                                    {
                                        if (! ny_folder.mkdir(path+"/"+folder_datum))
                                        {
                                          QMessageBox::critical(this, TITLE_STRING,tr("Unable to create directory \n")+path+"/"+folder_datum+tr("\ncheck your permisions"));
                                            exit(0);
                                        }


                                    }


                                     while ( !QFile::copy( *constIterator, new_filnamn_folder ) )
                                        {
                                            langd = new_filnamn_folder.size();
                                            new_filnamn_folder.remove(ind,langd);
                                            new_filnamn_folder+="_"+str.setNum(++lika)+extension;
                                        }
                                                disp_log(constIterator,new_filnamn_folder,path);
                                                 progress->setValue(++klara);
                                }
                }
         }

    }
QString alla;
alla.setNum(klara);
QString godtjanda;
godtjanda.setNum(klara-misslyckade);
QString resultat = tr("Done! ")+"( "+godtjanda+tr(" of ")+alla+" "+tr("file(s) )");
te_disp->append(QDir::toNativeSeparators((resultat)));
statusBar()->showMessage(resultat);
fileNames.clear();

delete wert;

delete [] ch;
delete [] filnamn;
delete [] info;
}

void MainWindow::clear_log()
{
    te_disp->clear();

}
void MainWindow::save_log()
{
     QString sokvag;
     std::string s = k->getConf("log_path");
    QString qs = QString::fromStdString(s);
     QFileInfo filInfo(qs);
      if (filInfo.isReadable())
          sokvag = qs;
        else
          sokvag = QDir::homePath();
          
        QString fn = QDir::toNativeSeparators(tr("/exif_rename_log.txt"));


  outputFileDialog = new QFileDialog(this, tr("Save log as..."),sokvag+fn);
  outputFileDialog->setFileMode(QFileDialog::AnyFile);
  outputFileDialog->setAcceptMode(QFileDialog::AcceptSave);

  outputFileDialog->setViewMode(QFileDialog::Detail);

  QStringList filters;
  filters << tr("Text files (*.txt)") << tr("Any files (*)");
  outputFileDialog->setNameFilters(filters);



  if (outputFileDialog->exec())
  {
            QDir dir;
            dir = outputFileDialog->directory();
            QString logvag = dir.path();

      QStringList files = outputFileDialog->selectedFiles();
      if (!files.isEmpty())
        {
            QString fileName = files[0];

             QFile file(fileName);
                if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
                {
                    QMessageBox::critical(this, TITLE_STRING,tr("Error writing to\n")+fileName+tr("\ncheck your permissions"));
                return;
                }
                       QDateTime qdt;
                       qdt = QDateTime::currentDateTime();
                       QString dt =tr("*** Log saved: ")+qdt.toString("yyyy-MM-dd hh:mm:ss")+" ***\n";

                QString allt = te_disp->document()->toPlainText();
                     QTextStream out(&file);
                  out << dt;
                        out << allt;
                        QString log_created = tr("Log created by ");
                  out << "\n\n" << log_created << TITLE_STRING << endl;
                  out << tr("Copyright Ingemar Ceicer 2007 - ") << CURRENT_YEAR  << endl;
                file.close();




                k->setConf("log_path",logvag.toStdString());


        }
  }

}
void MainWindow::help()
{
    std::string sprak = k->getConf("language");
    sprak = "info_"+sprak+".html";
    Help *wiev_help = new Help;
    wiev_help->startHelp(sprak,k);
    wiev_help->setPalette(QColor(255, 255, 153));
    wiev_help->exec();


}
void MainWindow::check_update()
{

    Update *checkUpdate = new Update;
    checkUpdate->startUpdate(k);
    checkUpdate->show();




}
void MainWindow::startup_check()
{
    Update *checkUpdate2 = new Update;
    checkUpdate2->startUppCheck(k);
}

void MainWindow::about()
{
    //QString conf_file_name = QDir::toNativeSeparators("/exif_rename.conf");
   // k = new Config(conf_file_name,1);
    std::string language = k->getConf("language");
   // delete k;

	QString system = getSystem();

    QString applicationHomepage;
    if (language == "sv")
        applicationHomepage = APPLICATION_HOMEPAGE;
    else
        applicationHomepage = APPLICATION_HOMEPAGE_ENG;



    const QString license = tr("Licensierat under GNU General Public License This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version ")+ LICENCE_VERSION +tr(" of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.");

   // QMessageBox::critical(this, TITLE_STRING,licens);
const QString auther = tr("<br><font coor=\"green\" size=\"3\"><b>Copyright &copy; 2007 - ")+ CURRENT_YEAR + " Ingemar Ceicer<br><a href=" + applicationHomepage + ">EXIF ReName "+tr("homepage")+"</a><br>programmering1@ceicer.org<br>Phone  +46706361747</b></font><br><br>";
QMessageBox::about(this, tr("About ")+TITLE_STRING,tr(
                         "<h1><font color=\"#009900\">")+TITLE_STRING+"</h1>"+auther+tr("A small application for renaming pictures. EXIF ReName uses exif data from the picture file.<br><br>")+tr("Thank you Ivan Dolgov for the translation into Russian.")+"<br>"+tr("Thank you Roberto Nerozzi for the translation into Italien.")+"<br>"+tr("Thank you Cristian Sosa Noe for the translation into Spanish.")+"<br>"+tr("Thank you Ben Weis for the translation into German.")+"<br><br>"+license+"<br><br>"+system);


}

void MainWindow::aboutQt()
{

    QMessageBox::aboutQt(this,TITLE_STRING);


}

void MainWindow::ryska()
{
    std::string conf_value = k->getConf("language");
    if (conf_value != "ru")
    {

        // Windows
           const QString EXECUTE = QDir::toNativeSeparators("\""+QDir::currentPath()+"/"+EXECUTABLE_NAME+"-"+VERSION+".exe\"");
        // Slackware
        // const QString EXECUTE = "/usr/local/bin/"EXECUTABLE_NAME;
       QProcess *CommProcess = new QProcess(this);
       // QMessageBox::information(this,"IPAC",EXECUTE);


        switch(QMessageBox::question(this, TITLE_STRING,
                                    tr("The application needs to restart in order\nto initialise the now language settings.\n\nDo you want to close\nthe application now?"),
                                     tr("Yes"),
                                     tr("No"),tr("Cancel"), 0, 2))
    {
        case 0: // The user clicked the Retry again button or pressed Enter
             k->setConf("language","ru");

             CommProcess->startDetached(EXECUTE);

               close();

        case 1:
    k->setConf("language","ru");
    return;

      case 2: // The user clicked the Quit or pressed Escape
          return;

        }


}

}

// qt-4.1.4
void MainWindow::engelska()
{
    std::string conf_value = k->getConf("language");
    if (conf_value != "eng")
    {
        // Windows
           const QString EXECUTE = QDir::toNativeSeparators("\""+QDir::currentPath()+"/"+EXECUTABLE_NAME+"-"+VERSION+".exe\"");
        // Slackware
        // const QString EXECUTE = "/usr/local/bin/"EXECUTABLE_NAME;
        QProcess *CommProcess = new QProcess(this);
       // QMessageBox::information(this,"IPAC",EXECUTE);



        switch(QMessageBox::question(this, TITLE_STRING,
                                    tr("The application needs to restart in order\nto initialise the now language settings.\n\nDo you want to close\nthe application now?"),
                                    tr("Yes"),
                                    tr("No"),tr("Cancel"), 0, 2))
    {
        case 0: // The user clicked the Retry again button or pressed Enter
             k->setConf("language","eng");

             CommProcess->startDetached(EXECUTE);
               close();
         return;
        case 1:
    k->setConf("language","eng");
    return;

        case 2: // The user clicked the Quit or pressed Escape
            return;

        }


}

}
// qt-4.1.4
void MainWindow::italienska()
{
        std::string conf_value = k->getConf("language");
    if (conf_value != "it")
    {
        // Windows
           const QString EXECUTE = QDir::toNativeSeparators("\""+QDir::currentPath()+"/"+EXECUTABLE_NAME+"-"+VERSION+".exe\"");
        // Slackware
        // const QString EXECUTE = "/usr/local/bin/"EXECUTABLE_NAME;
        QProcess *CommProcess = new QProcess(this);
       // QMessageBox::information(this,"IPAC",EXECUTE);



        switch(QMessageBox::question(this, TITLE_STRING,
                                    tr("The application needs to restart in order\nto initialise the now language settings.\n\nDo you want to close\nthe application now?"),
                                    tr("Yes"),
                                    tr("No"),tr("Cancel"), 0,2))
    {
        case 0: // The user clicked the Retry again button or pressed Enter
             k->setConf("language","it");


             CommProcess->startDetached(EXECUTE);

              close();

         return;
            break;
        case 1:
    k->setConf("language","it");
    return;
            break;
        case 2: // The user clicked the Quit or pressed Escape
            return;
            break;
        }


}
}
// qt-4.1.4
void MainWindow::tyska()
{
        std::string conf_value = k->getConf("language");
    if (conf_value != "de")
    {
        // Windows
           const QString EXECUTE = QDir::toNativeSeparators("\""+QDir::currentPath()+"/"+EXECUTABLE_NAME+"-"+VERSION+".exe\"");
        // Slackware
        // const QString EXECUTE = "/usr/local/bin/"EXECUTABLE_NAME;
        QProcess *CommProcess = new QProcess(this);
       // QMessageBox::information(this,"IPAC",EXECUTE);



        switch(QMessageBox::question(this, TITLE_STRING,
                                    tr("The application needs to restart in order\nto initialise the now language settings.\n\nDo you want to close\nthe application now?"),
                                    tr("Yes"),
                                    tr("No"),tr("Cancel"), 0,2))
    {
        case 0: // The user clicked the Retry again button or pressed Enter
             k->setConf("language","de");


             CommProcess->startDetached(EXECUTE);

              close();

         return;
            break;
        case 1:
    k->setConf("language","de");
    return;
            break;
        case 2: // The user clicked the Quit or pressed Escape
            return;
            break;
        }


}
}
// qt-4.1.4
void MainWindow::svenska()
{
        std::string conf_value = k->getConf("language");
    if (conf_value != "sv")
    {
        // Windows
           const QString EXECUTE = QDir::toNativeSeparators("\""+QDir::currentPath()+"/"+EXECUTABLE_NAME+"-"+VERSION+".exe\"");
        // Slackware
        // const QString EXECUTE = "/usr/local/bin/"EXECUTABLE_NAME;
        QProcess *CommProcess = new QProcess(this);
       // QMessageBox::information(this,"IPAC",EXECUTE);



        switch(QMessageBox::question(this, TITLE_STRING,
                                    tr("The application needs to restart in order\nto initialise the now language settings.\n\nDo you want to close\nthe application now?"),
                                    tr("Yes"),
                                    tr("No"),tr("Cancel"), 0,2))
    {
        case 0: // The user clicked the Retry again button or pressed Enter
             k->setConf("language","sv");


             CommProcess->startDetached(EXECUTE);

              close();

         return;
            break;
        case 1:
    k->setConf("language","sv");
    return;
            break;
        case 2: // The user clicked the Quit or pressed Escape
            return;
            break;
        }


}
}
void MainWindow::spanska()
{
        std::string conf_value = k->getConf("language");
    if (conf_value != "sp")
    {
        // Windows
           const QString EXECUTE = QDir::toNativeSeparators("\""+QDir::currentPath()+"/"+EXECUTABLE_NAME+"-"+VERSION+".exe\"");
        // Slackware
        // const QString EXECUTE = "/usr/local/bin/"EXECUTABLE_NAME;
        QProcess *CommProcess = new QProcess(this);
       // QMessageBox::information(this,"IPAC",EXECUTE);



        switch(QMessageBox::question(this, TITLE_STRING,
                                    tr("The application needs to restart in order\nto initialise the now language settings.\n\nDo you want to close\nthe application now?"),
                                    tr("Yes"),
                                    tr("No"),tr("Cancel"), 0,2))
    {
        case 0: // The user clicked the Retry again button or pressed Enter
             k->setConf("language","sp");


             CommProcess->startDetached(EXECUTE);

              close();

         return;
            break;
        case 1:
    k->setConf("language","sp");
    return;
            break;
        case 2: // The user clicked the Quit or pressed Escape
            return;
            break;
        }


}
}

void MainWindow::icons_only()
{
       fileToolBar->show();
        fileToolBar->setToolButtonStyle(Qt::ToolButtonIconOnly);
        k->setConf("ToolButtonStyle","0");
}

void MainWindow::text_only()
{
       fileToolBar->show();
        fileToolBar->setToolButtonStyle(Qt::ToolButtonTextOnly);
        k->setConf("ToolButtonStyle","1");
}

void MainWindow::icons_text_alongside()
{
    fileToolBar->show();
    fileToolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    k->setConf("ToolButtonStyle","2");
}

void MainWindow::icons_text_under()
{
    fileToolBar->show();
    fileToolBar->setToolButtonStyle(Qt::ToolButtonTextUnderIcon);
    k->setConf("ToolButtonStyle","3");
}
void MainWindow::no_toolbar()
{

    fileToolBar->hide();
    k->setConf("ToolButtonStyle","-1");
}

// STATUSBAR
void MainWindow::createStatusBar()
{
    QFont font;
    font.setPixelSize(10);
    statusBar()->setFont(font);
    statusBar()->showMessage(tr("Ready for action!"));
}


void MainWindow::closeEvent(QCloseEvent *event)
{

    event->accept();

}


void MainWindow::check_copy()
{

    if ( ch_copy->isChecked() )
    {
        pu_copy->setEnabled(false);
      k->setConf("copy","0");
        la_copy_path->setText(tr("The original file will be renamed"));


    }
    if ( !ch_copy->isChecked() )
    {
        pu_copy->setEnabled(true);
                k->setConf("copy","1");
                std::string s = k->getConf("copy_path");
                QString qs = QString::fromStdString(s);
                     QFileInfo info(qs);
                     if (info.isWritable())
                        la_copy_path->setText(QDir::toNativeSeparators(tr("Copy to: ")+qs));
                    else
                        la_copy_path->setText(QDir::toNativeSeparators(tr("Copy to: ")));

    }

}
void MainWindow::setConfig()
{

  std::string conf_value,conf_value2,conf_value3;
  conf_value = k->getConf("copy");
  std::stringstream ss;
  int i;

  if (conf_value == "1")
  {
    ch_copy->setCheckState(Qt::Checked);
    pu_copy->setEnabled(true);
     std::string s = k->getConf("copy_path");
    QString qs = QString::fromStdString(s);
     QFileInfo info(qs);
      if (info.isWritable())
            la_copy_path->setText(QDir::toNativeSeparators(tr("Copy to: ")+qs));
      else
          la_copy_path->setText(tr("Copy to: "));


  }
  if (conf_value == "0")
  {
    ch_copy->setCheckState(Qt::Unchecked);
    pu_copy->setEnabled(false);
     la_copy_path->setText(tr("The original file will be renamed"));


  }
  conf_value = k->getConf("preserve");
  conf_value2 = conf_value;

  if (conf_value == "1")
  {
    ch_preserve->setCheckState(Qt::Checked);
    ch_insert->setCheckState(Qt::Unchecked);
  }

  conf_value = k->getConf("preserve_before");
  if (conf_value == "1")
  {
    ra_before_preserve->setChecked(true);
     if (conf_value2=="1")
     {
        preserve_beforeAct->setChecked(true);
        preserve_afterAct->setChecked(false);
     }
  }
  if (conf_value == "0" && conf_value2 == "1")
  {
      preserve_afterAct->setChecked(true);
      preserve_beforeAct->setChecked(false);
  }

  conf_value = k->getConf("insert");
  conf_value3 = conf_value;
  if (conf_value == "1")
  {
    ch_preserve->setCheckState(Qt::Unchecked);
    ch_insert->setCheckState(Qt::Checked);
  }

  conf_value = k->getConf("insert_before");
  if (conf_value == "1")
  {
    ra_before_insert->setChecked(true);
     if (conf_value3=="1")
     {
        insert_beforeAct->setChecked(true);
        insert_afterAct->setChecked(false);
     }
  }
  if (conf_value == "0" && conf_value3 == "1")
  {
      insert_afterAct->setChecked(true);
      insert_beforeAct->setChecked(false);
  }

  conf_value = k->getConf("file_name");
  li_insert->setText(QString::fromStdString(conf_value));

    if ((conf_value2 == conf_value3) && (conf_value2 == "0"))
        dontAddFileNameAct->setChecked(true);

    conf_value = k->getConf("ToolButtonStyle");
    ss << conf_value;
    ss >> i;
    if (i > 0 )
    {
        Qt::ToolButtonStyle buttom_style = Qt::ToolButtonStyle(i);
        fileToolBar->setToolButtonStyle(buttom_style);
    }
    if (i < 0)
        fileToolBar->hide();

conf_value = k->getConf("create_folder");
if (conf_value == "0")
    createFolderAct->setChecked(false);
if (conf_value == "1")
    createFolderAct->setChecked(true);


}

// qt-4.1.4
bool MainWindow::check_first_time()
{

     std::string conf_value = k->getConf("first_time");
      if (conf_value == "1")
        {

          std::string language = k->getConf("language");

          QString applicationHomepage;
          if (language == "sv")
              applicationHomepage = APPLICATION_HOMEPAGE;
          else
              applicationHomepage = APPLICATION_HOMEPAGE_ENG;


const QString auther = tr("<br><font coor=\"green\" size=\"3\"><b>Copyright &copy; 2007 - ")+ CURRENT_YEAR + "Ingemar Ceicer<br><a href=" + applicationHomepage + ">EXIF ReName "+tr("homepage")+"</a><br>programmering1@ceicer.org<br>Phone  +46706361747</b></font><br><br>";
const QString license = tr("Licensierat under GNU General Public License This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version ")+ LICENCE_VERSION +tr(" of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program; if not, write to the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.");

        switch(QMessageBox::question(this, TITLE_STRING,
                                    tr("<h1><font color=\"#009900\">")+TITLE_STRING+"</font></h1><br>"+auther+tr("<br>A small application for renaming pictures. EXIF ReName uses exif data from the picture file.<br><br>")+license+tr("<br><br><font color=\"#FF0000\" size=\"4\">Do you accept the license?</font>"),
                                    tr("Yes"),
                                    tr("No"),0,1,2))
    {
        case 0:
          {
            k->setConf("first_time","0");
            sett_language();
            return true;
            break;
          }
        case 1:
          {

        Createuser *cr = new Createuser;
        cr->removeDir();
        delete cr;
        return false;
        break;
          }
        default:
        {
        Createuser *cr = new Createuser;
        cr->removeDir();
        delete cr;
        return false;
        break;
        }
    }


        }
if (conf_value == "0")
{
                std::string version = k->getConf("version");
                    if (version == "nothing")
                    {
                        k->newConf("version",VERSION);
                        QMessageBox::information(this,TITLE_STRING,"EXIF ReName "+tr(" is updatet to ")  + TITLE_STRING );
                    }

                    if (version != VERSION)
                    {

                        new_version_config();
                        k->setConf("version",VERSION);
                        QMessageBox::information(this,TITLE_STRING,"EXIF ReName "+QString::fromStdString(version)+tr(" is updatet to ")  + TITLE_STRING );



                    }

}
return true;
}



bool MainWindow::test_copy_path(std::string fileName)
{
  QString fins = QString::fromStdString(fileName);
  bool test;
 test = QFile::exists(fins);
  return test;



}
void MainWindow::disp_log(QStringList::const_iterator org,QString ny)
{
    te_disp->append(QDir::toNativeSeparators(stripNames(org)+" <font color=green>-></font>"+stripNames(ny)));

}
void MainWindow::disp_log(QStringList::const_iterator org,QString ny,QString path)
{
    if (org == fileNames.begin())
    {


         if (k->getConf("create_folder") == "1")
            {

                te_disp->append(QDir::toNativeSeparators(tr("Copied to new folder in: <b>")+path+tr("</b> and renamed: ")));
            }
         if (k->getConf("create_folder") == "0")
            {
                te_disp->append(QDir::toNativeSeparators(tr("Copied to: <b>")+path+tr("</b> and renamed: ")));


            }
    }
    te_disp->append(QDir::toNativeSeparators(stripNames(org)+" <font color=green>-></font>"+stripNames(ny)));


}
void MainWindow::selectedFiles(QString *directoryName)
{
    te_disp->append(QDir::toNativeSeparators(tr("<br />Selected file(s) in: <b>")+*directoryName+"</b>"));
   QStringList::const_iterator constIterator;
   for (constIterator = fileNames.constBegin(); constIterator != fileNames.constEnd();++constIterator)
    {

        te_disp->append(QDir::toNativeSeparators(stripNames(constIterator)));

    }

}
QString MainWindow::stripNames(QStringList::const_iterator ps)
{

        int ind = ps->lastIndexOf("/",-1);
        return ps->mid(ind+1,-1);

}
QString MainWindow::stripNames(QString s)
{

        int ind = s.lastIndexOf("/",-1);
        return s.mid(ind+1,-1);

}

void MainWindow::synkronisera(bool bool_insert, bool bool_preserve)
{

    if ( (ch_insert->isChecked()==bool_insert) && (ch_preserve->isChecked()==bool_preserve) )
    {
        dontAddFileNameAct->setChecked(true);
        preserve_beforeAct->setChecked(false);
        preserve_afterAct->setChecked(false);
        insert_beforeAct->setChecked(false);
        insert_afterAct->setChecked(false);
    }
    else
    {
        if (ra_before_insert->isChecked())
        {
            dontAddFileNameAct->setChecked(false);
            preserve_beforeAct->setChecked(false);
            preserve_afterAct->setChecked(false);
            insert_beforeAct->setChecked(true);
            insert_afterAct->setChecked(false);
        }
        if (ra_after_insert->isChecked())
        {
            dontAddFileNameAct->setChecked(false);
            preserve_beforeAct->setChecked(false);
            preserve_afterAct->setChecked(false);
            insert_beforeAct->setChecked(false);
            insert_afterAct->setChecked(true);
        }
        if (ra_before_preserve->isChecked())
        {
            dontAddFileNameAct->setChecked(false);
            preserve_beforeAct->setChecked(true);
            preserve_afterAct->setChecked(false);
            insert_beforeAct->setChecked(false);
            insert_afterAct->setChecked(false);
        }
        if (ra_after_preserve->isChecked())
        {
            dontAddFileNameAct->setChecked(false);
            preserve_beforeAct->setChecked(false);
            preserve_afterAct->setChecked(true);
            insert_beforeAct->setChecked(false);
            insert_afterAct->setChecked(false);
        }

    }
}

void MainWindow::new_version_config()
{
    if (k->getConf("first_time") == "nothing")
        k->newConf("first_time","1");
    if (k->getConf("version") == "nothing")
        k->newConf("version",VERSION);
    if (k->getConf("language") == "nothing")
        k->newConf("language","eng");
    if (k->getConf("copy") == "nothing")
        k->newConf("copy","0");
    if (k->getConf("preserve") == "nothing")
        k->newConf("preserve","0");
    if (k->getConf("preserve_before") == "nothing")
        k->newConf("preserve_before","0");
    if (k->getConf("insert") == "nothing")
        k->newConf("insert","0");
    if (k->getConf("insert_before") == "nothing")
        k->newConf("insert_before","0");
    if (k->getConf("file_name") == "nothing")
        k->newConf("file_name","");
    if (k->getConf("copy_path") == "nothing")
        k->newConf("copy_path","");
    if (k->getConf("open_path") == "nothing")
        k->newConf("open_path","");
    if (k->getConf("log_path") == "nothing")
        k->newConf("log_path","");
    if (k->getConf("vidd") == "nothing")
        k->newConf("vidd","749");
    if (k->getConf("hojd") == "nothing")
        k->newConf("hojd","401");
    if (k->getConf("x_lage") == "nothing")
        k->newConf("x_lage","150");
    if (k->getConf("y_lage") == "nothing")
        k->newConf("y_lage","150");
    if (k->getConf("hvidd") == "nothing")
        k->newConf("hvidd","600");
    if (k->getConf("hhojd") == "nothing")
        k->newConf("hhojd","675");
    if (k->getConf("hx_lage") == "nothing")
        k->newConf("hx_lage","150");
    if (k->getConf("hy_lage") == "nothing")
        k->newConf("hy_lage","150");
    if (k->getConf("ToolButtonStyle") == "nothing")
        k->newConf("ToolButtonStyle","3");
    if (k->getConf("create_folder") == "nothing")
        k->newConf("create_folder","0");
    if (k->getConf("check_onstart") == "nothing")
        k->newConf("check_onstart","1");


}

void MainWindow::sett_language()
{
    QMessageBox languagabox(this);
    languagabox.setIcon(QMessageBox::Question);
    languagabox.setWindowTitle(TITLE_STRING);
    QPushButton *btEng = new QPushButton(QIcon(":/images/eng.xpm"),tr("English"));
    QPushButton *btIt = new QPushButton(QIcon(":/images/it.xpm"),tr("Italiano"));
    QPushButton *btRu = new QPushButton(QIcon(":/images/russian.xpm"),tr("Russian"));
    QPushButton *btSv = new QPushButton(QIcon(":/images/sv.xpm"),tr("Swedish"));
    QPushButton *btSp = new QPushButton(QIcon(":/images/spain.xpm"),tr("Spanish"));
    QPushButton *btDe = new QPushButton(QIcon(":/images/de.xpm"),tr("German"));
    languagabox.setText(tr("Choose the language that will be used by EXIF Rename"));
    languagabox.addButton(btEng,QMessageBox::ApplyRole);
    languagabox.addButton(btIt,QMessageBox::ApplyRole);
    languagabox.addButton(btRu,QMessageBox::ApplyRole);
    languagabox.addButton(btSv,QMessageBox::ApplyRole);
    languagabox.addButton(btSp,QMessageBox::ApplyRole);
    languagabox.addButton(btDe,QMessageBox::ApplyRole);

if (languagabox.exec())
{

    if (languagabox.clickedButton()==btRu)
    {
        k->setConf("language","ru");
        QMessageBox::information(this, TITLE_STRING,tr("You must restart the application to the Russian language settings to take effect."));
        delete k;
        exit(0);

    }
    if (languagabox.clickedButton()==btIt)
    {
        k->setConf("language","it");
        QMessageBox::information(this, TITLE_STRING,tr("You must restart the application to the Italien language settings to take effect."));
        delete k;
        exit(0);

    }
    if (languagabox.clickedButton()==btSv)
    {
        k->setConf("language","sv");
        QMessageBox::information(this, TITLE_STRING,tr("You must restart the application to the Swedish language settings to take effect."));
        delete k;
        exit(0);
    }
    if (languagabox.clickedButton()==btSp) // ipac2
    {
        k->setConf("language","sv");
        QMessageBox::information(this, TITLE_STRING,tr("You must restart the application to the Spanish language settings to take effect."));
        delete k;
        exit(0);
    }
    if (languagabox.clickedButton()==btDe) // ipac2
    {
        k->setConf("language","de");
        QMessageBox::information(this, TITLE_STRING,tr("You must restart the application to the Spanish language settings to take effect."));
        delete k;
        exit(0);
    }
    else
    {
        k->setConf("language","eng");


    }


}

}

void MainWindow::edit_exif()
{
     QString sokvag;
     std::string s = k->getConf("open_path");
    QString qs = QString::fromStdString(s);
     QFileInfo info(qs);
      if (info.isReadable())
            sokvag = qs;
      else
      {
          sokvag = QDir::homePath();
      }

  inputFileDialog = new QFileDialog(this, tr("Open File"),sokvag);
  inputFileDialog->setFileMode(QFileDialog::ExistingFiles);
  inputFileDialog->setAcceptMode(QFileDialog::AcceptOpen);

  QString conf_file_name = QDir::toNativeSeparators("/exif_rename3.conf");
  Config *k4 = new Config(conf_file_name,2);
  std::string monster_s = k4->getConf("extension_name");

  QString monster = QString::fromStdString(monster_s);
  if (monster=="nothing")
      monster=STANDARD_EXTENSION;
  //QMessageBox::information(this, TITLE_STRING,monster);

  if (k4->getConf("current_extension") == "nothing")
        k4->newConf("current_extension","");

  if (k4->getConf("current_filter") == "nothing")
        k4->newConf("current_filter","");

  QStringList filters;
  if (monster[monster.size()] != ';')
      monster = monster+';';
  int sep = monster.indexOf(';');
  QString del_monster;
  while (sep > 0)
  {
    del_monster = monster.mid(0,sep);
    filters << tr("Image files")+" (*."+del_monster+")";
    monster.remove(0,sep+1);
    sep = monster.indexOf(';');
}
  filters << tr("All files")+" (*.*)";


  inputFileDialog->setNameFilters(filters);
  std::string default_filter = k4->getConf("current_filter");

  inputFileDialog->selectFilter(QString::fromStdString(default_filter));

  inputFileDialog->setViewMode(QFileDialog::Detail);

  if (inputFileDialog->exec())
  {

            te_disp->clear();
            QDir dir;
            QString str;


            dir = inputFileDialog->directory();
            QString directoryName = dir.path();
            QFileInfo info2(directoryName);

            if (! info2.isReadable() )
            {
                QMessageBox::critical(this, TITLE_STRING,tr("You do not have enough permissions to read\n")+directoryName);
                return;
            }


            QString qs_qurrent_extension = inputFileDialog->selectedNameFilter();
            k4->setConf("current_filter",qs_qurrent_extension.toStdString());


            QStringList fileNames = inputFileDialog->selectedFiles();
            k->setConf("open_path",directoryName.toStdString());

            te_disp->append(tr("Changes the EXIF information for these files"));
            selectedFiles(&directoryName);

           for (int i=0; i < fileNames.size(); i++)
           {
           te_disp->append(stripNames(fileNames[i]));

           }

           statusBar()->showMessage((directoryName)+tr(" ...selected"));



   Exif_w *e_w = new Exif_w;
   e_w->startExif(&fileNames);


  }
  delete k4;

}

QString MainWindow::displayFolderSeparators()
{
  QString conf_file_name = QDir::toNativeSeparators("/exif_rename3.conf");
  Config *k5 = new Config(conf_file_name,2);
  std::string fs_s = k5->getConf("folder_separator");
  delete k5;
  QString fs = QString::fromStdString(fs_s);
  if (fs == "none")
      fs = "";
  if (fs == "space")
      fs = " ";
//QMessageBox::critical(this, TITLE_STRING,tr("You have"));
  return fs;

}

QString MainWindow::getSystem()
{
      QString v="";
    #ifdef Q_OS_LINUX
    v="Linux";
    #endif // Q_OS_LINUX

    #ifdef Q_OS_MAC
    v="Mac";
    #endif // Q_OS_MAC


    #ifdef Q_OS_WIN32
     switch (QSysInfo::WindowsVersion)
    {
    case QSysInfo::WV_DOS_based:
        v=tr("MS-DOS-based version of Windows.");
        break;
    case QSysInfo::WV_NT_based:
        v=tr("NT-based version of Windows.");
        break;
    case QSysInfo::WV_CE_based:
        v=tr("CE-based version of Windows.");
        break;
     case QSysInfo::WV_32s:
        v=tr("Windows 3.1 with Win 32s operating system.");
        break;
    case QSysInfo::WV_95:
        v=tr("Windows 95 operating system.");
        break;
    case QSysInfo::WV_98:
        v=tr("Windows 98 operating system.");
        break;
    case QSysInfo::WV_Me:
        v=tr("Windows Me operating system.");
        break;
    case QSysInfo::WV_4_0:
        v=tr("Windows operating system version 4.0, corresponds to Windows NT.");
        break;
    case QSysInfo::WV_5_0:
        v=tr("Windows operating system version 5.0, corresponds to Windows 2000.");
        break;
    case QSysInfo::WV_5_1:
        v=tr("Windows operating system version 5.1, corresponds to Windows XP.");
        break;
    case QSysInfo::WV_5_2:
        v=tr("Windows operating system version 5.2, corresponds to Windows Server 2003, Windows Server 2003 R2, Windows Home Server, and Windows XP Professional x64 Edition.");
        break;
    case QSysInfo::WV_6_0:
        v=tr("Windows operating system version 6.0, corresponds to Windows Vista and Windows Server 2008.");
        break;
    case QSysInfo::WV_6_1:
        v=tr("Windows operating system version 6.1, corresponds to Windows 7 and Windows Server 2008 R2.");
        break;
     case QSysInfo::WV_6_2:
         v=tr("Windows operating system version 6.2, corresponds to Windows 8");
         break;
     case QSysInfo::WV_6_3:
         v=tr("Operating system version 6.3, corresponds to Windows 8.1");
         break;
     case QSysInfo::WV_10_0:
         v=tr("Operating system version 10.0, corresponds to Windows 10");
         break;
    case QSysInfo::WV_CE:
        v=tr("Windows CE operating system.");
        break;
    case QSysInfo::WV_CENET:
        v=tr("Windows CE .NET operating system.");
        break;
    case QSysInfo::WV_CE_5:
        v=tr("Windows CE 5.x operating system.");
        break;
    case QSysInfo::WV_CE_6:
        v=tr("Windows CE 6.x operating system.");
        break;
    default:
           v=tr("Unknown Windows operating system");

  }

   #endif // Q_OS_WIN32

    int wsize = QSysInfo::WordSize;
    QString qswsize;
    qswsize.setNum(wsize);
    v+="<br>"+tr("Architecture instruction set: ")+qswsize+"-bit.";
/*
    const QLocale loc = this->locale();
    const QString l = loc.QLocale::bcp47Name();
    v+=". "+tr("Your country and language settings, separated by \"-\":")+" "+l;
*/

  



    v+="<br>" TITLE_STRING+tr(" was created ")+BUILD_DATE_TIME+".";

    v+="<br>";
    // write GCC version
   #if defined __GNUC__ && !defined __clang__
		#ifdef __MINGW32__
			#define COMPILER "MinGW GCC"
		#else
			#define COMPILER "GCC"
		#endif
			v+=(QString(tr("Compiled by")+ " %1 %2.%3.%4%5").arg(COMPILER).arg(__GNUC__).arg(__GNUC_MINOR__).arg(__GNUC_PATCHLEVEL__).arg("."));
   #elif defined __clang__
			v+=(tr("Compiled by")+  " %1 %2.%3.%4%5").arg("Clang").arg(__clang_major__).arg(__clang_minor__).arg(__clang_patchlevel__).arg("."));

     // MSVC version
   
   #elif defined _MSC_VER
			#define COMPILER "MSVC"
			QString version;
			     switch (_MSC_FULL_VER)
				 {
					case 12008804:
                    version = "6.0 SP6";
					break;
					case 13009466:
                    version = "7.0";
					break;
					case 13103077:
                    version = "7.1 (2003)";
					break;
					case 140050727:
                    version = "8.0 (2005)";
					break;
					case 150021022:
                    version = "9.0 (2008)";
					break;
					case 160040219:
                    version = "10.0 (2010)";
					break;
					case 170050727:
                    version = "11.0 (2012))";
					break;	
					default:
                    version = tr("Unknown version");
					break;
				 }

          v+=(QString(tr("Compiled by")+  " %1 %2%3").arg(COMPILER).arg(version).arg("."));



   #else
    v+=tr("Unknown compiler.");
   #endif

	
    return v;
}
