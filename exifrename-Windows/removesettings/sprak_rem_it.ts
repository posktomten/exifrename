<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it_IT" sourcelanguage="en">
<context>
    <name>Messagebox</name>
    <message>
        <location filename="messagebox.cpp" line="32"/>
        <source>Do you want to remove your personal settings?
This is an appropriate action if you like to uninstall this application.
Or if you want to reset all settings to default.</source>
        <translation>Vuoi cancellare i settaggi personali ? E&apos; preferibile se disistalli questa applicazione o se vuoi resettarli.</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="33"/>
        <source>Yes</source>
        <translation>Si</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="34"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="42"/>
        <source>Your personal settings are deleted.</source>
        <translation>I tuoi settaggi personali sono cancellati.</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="48"/>
        <location filename="messagebox.cpp" line="61"/>
        <source>Your personal settings cannot be found.
New settings will be saved then you restart the program.</source>
        <translation>I tuoi settaggi personali non sono stati trovati. Ne saranno creati di nuovi dopo aver fatto ripartire il programma.</translation>
    </message>
</context>
</TS>
