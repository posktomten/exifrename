/*
    EXIF ReName
    Copyright (C) 2007 - 2017  Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/



#ifndef MESSAGEBOX_H
#define MESSAGEBOX_H



#include <QMessageBox>

#define TITLE_STRING "EXIF ReName 0.1.15"
#define VERSION "0.1.15"

class Messagebox : public QMessageBox
{
    Q_OBJECT
public:

    Messagebox();




protected:





private slots:


private:
     void stang();

};

#endif

