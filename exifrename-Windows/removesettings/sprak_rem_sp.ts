<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_AR">
<context>
    <name>Messagebox</name>
    <message>
        <location filename="messagebox.cpp" line="32"/>
        <source>Do you want to remove your personal settings?
This is an appropriate action if you like to uninstall this application.
Or if you want to reset all settings to default.</source>
        <translation>Desea eliminar sus preferencias?
Esto es apropiado si quiere desinstalar esta aplicación
O si desea restablecer sus preferencias a las predeterminadas.</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="33"/>
        <source>Yes</source>
        <translation>Si</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="34"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="42"/>
        <source>Your personal settings are deleted.</source>
        <translation>Sus preferencias serán eliminadas.</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="48"/>
        <location filename="messagebox.cpp" line="61"/>
        <source>Your personal settings cannot be found.
New settings will be saved then you restart the program.</source>
        <translation>Sus preferencias personales no pudieron ser encontradas
Se grabaran nuevas preferencias luego de reiniciar el programa.</translation>
    </message>
</context>
</TS>
