
/*
    EXIF ReName
    Copyright (C) 2007 - 2017  Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/


#include <QApplication>



#include "messagebox.h"
#include "config.h"
#include <string>
#include <QTranslator>
#include <QString>
#include <QMessageBox>
//#include<QStringBuilder>

// Windows
  const QString INSTALL_DIR = QDir::toNativeSeparators(QDir::currentPath()+"/");
// Slackware
//   const QString INSTALL_DIR = "/usr/local/exifrename";

const QString CONF_DIR = QDir::toNativeSeparators(QDir::homePath()+"/.exifrename");
const QString CONF_FILE = CONF_DIR + QDir::toNativeSeparators("/exif_rename.conf");

int main(int argc, char *argv[])
{


        QApplication app(argc, argv);



                QTranslator translator;
            Config *kk = new Config;
            std::string sprak = kk->getConf("language");
            delete kk;


             if (sprak == "sv")
                {
                    translator.load(INSTALL_DIR+QDir::toNativeSeparators("/sprak_rem_sv.qm"));
                    app.installTranslator(&translator);

                }

             if (sprak == "ru")
                {
                    translator.load(INSTALL_DIR+QDir::toNativeSeparators("/sprak_rem_ru.qm"));
                    app.installTranslator(&translator);

                }
             if (sprak == "it")
                {
                    translator.load(INSTALL_DIR+QDir::toNativeSeparators("/sprak_rem_it.qm"));
                    app.installTranslator(&translator);

                }
             if (sprak == "sp")
                {
                    translator.load(INSTALL_DIR+QDir::toNativeSeparators("/sprak_rem_sp.qm"));
                    app.installTranslator(&translator);

                }
             if (sprak == "de")
                {
                    translator.load(INSTALL_DIR+QDir::toNativeSeparators("/sprak_rem_de.qm"));
                    app.installTranslator(&translator);

                }


            QFont font;
            font.setPixelSize(12);
            app.QApplication::setFont(font);

            Messagebox mb;

            return app.exec();
}
