/*
    EXIF ReName
    Copyright (C) 2007 - 2017  Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/

#include "messagebox.h"
#include "createuser.h"
//#include<QStringBuilder>

Messagebox::Messagebox()
{
const QString CONF_DIR = QDir::toNativeSeparators(QDir::homePath()+"/.exifrename");
const QString CONF_FILE = CONF_DIR + QDir::toNativeSeparators("/exif_rename.conf");


QFile fil(CONF_FILE);
if (fil.exists() == true)
{
        switch(QMessageBox::question(this, TITLE_STRING,
                                    tr("Do you want to remove your personal settings?\nThis is an appropriate action if you like to uninstall this application.\nOr if you want to reset all settings to default."),
                                    tr("Yes"),
                                    tr("No"), 0,2)) 
	{
        case 0: 
	{
	Createuser *cr = new Createuser;

	if (cr->removeDir())
		{
		QMessageBox::information(this, TITLE_STRING,tr("Your personal settings are deleted."));
		delete cr;
		exit(0);

		}
	else
		QMessageBox::information(this, TITLE_STRING,tr("Your personal settings cannot be found.\nNew settings will be saved then you restart the program."));

	delete cr;
        break;
	}
	default:
	
        break;
        }

stang();
}

QMessageBox::information(this, TITLE_STRING,tr("Your personal settings cannot be found.\nNew settings will be saved then you restart the program."));
stang();

}

void Messagebox::stang()
{
	exit(0);
}
