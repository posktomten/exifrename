/*
    EXIF ReName
    Copyright (C) 2007 - 2017  Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/


#ifndef CONFIGH
#define CONFIGH
#include<string>
#include<vector>
#include<vector>
#include "createuser.h"

using namespace std;


class Config 
{


private:


vector<string> *name_values; 
bool createConffile();
void checkConf();
bool readConf();
bool writeConf();
string conf_path;
string conf;
Createuser *cr;


public:
Config();
~Config();

void newConf(string name,string value);
string getConf(string);
bool setConf(string name,string value);

};

#endif
