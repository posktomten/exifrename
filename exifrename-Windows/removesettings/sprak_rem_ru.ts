<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>Messagebox</name>
    <message>
        <location filename="messagebox.cpp" line="32"/>
        <source>Do you want to remove your personal settings?
This is an appropriate action if you like to uninstall this application.
Or if you want to reset all settings to default.</source>
        <translation>Хотите удалить персональные настройки?
Это рекомендуется делать, если вы собираетесь удалить приложение.
Или сбросить все на настройки по умолчанию.</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="33"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="34"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="42"/>
        <source>Your personal settings are deleted.</source>
        <translation>Персональные настройки удалены.</translation>
    </message>
    <message>
        <location filename="messagebox.cpp" line="48"/>
        <location filename="messagebox.cpp" line="61"/>
        <source>Your personal settings cannot be found.
New settings will be saved then you restart the program.</source>
        <translation>Невозможно найти персональные настройки.
Новые настройки будут сохранены после рестарта программы.</translation>
    </message>
</context>
</TS>
