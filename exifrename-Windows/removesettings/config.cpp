/*
    EXIF ReName
    Copyright (C) 2007 - 2010  Ingemar Ceicer
    http://ceicer.org/ingemar/
    programmering1 (at) ceicer (dot) org

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.
*/


#include "createuser.h"
#include "config.h"
#include "messagebox.h"
#include<fstream>
#include<string>
#include<vector>
#include<iostream>
//#include<QStringBuilder>

const QString configure_path = QDir::toNativeSeparators(QDir::homePath()+"/.exifrename");
const QString configure = configure_path + QDir::toNativeSeparators("/exif_rename.conf");


const std::string my_configure_path = configure_path.toStdString();
const std::string my_configure = configure.toStdString();

using namespace std;

Config::Config()
{
    //L�ser in configurationsfilen i minnet
    //vid skapandet

    cr = new Createuser;
    conf_path = my_configure_path;
    conf = my_configure;


    checkConf();
    readConf();

    delete cr;
}



Config::~Config()
{
    //Skriver configurationsfilen inan d�den
    writeConf();
}

// L�mnar ut "values" grundade p� "name"
string Config::getConf(string name)
{

vector<string>::iterator it;
string s;


for (it=name_values->begin();it<name_values->end();it++)
    {

            if (it->substr(0,1) != "#") // hoppa �ver kommaterade rader direkt
            {
                // S�ker efter samma delstr�ng som skickas hit med samma l�ngd
                // som hitskickade str�ngen. Samt att n�sta tecken �r "="
                // F�r att undvika fel typ: gustav gustavson
                if ((it->substr(0,name.size()) == name) && (it->substr(name.size(),1) == "="))
                {
                    // Returnerar fr�n tecknet efter "=" tecknet och til radens slut
                    s = it->substr(name.size()+1,it->size());
                    return s;

                }
            }
    }
    s="nothing";
    return s;
}

bool Config::setConf(string name,string value)
{
vector<string>::iterator it;
for (it=name_values->begin();it<name_values->end();it++)
    {

            if (it->substr(0,1) != "#") // hoppa �ver kommaterade rader direkt
            {
                // S�ker efter str�ngen "name" som skickas hit med samma l�ngd
                // som hitskickade str�ngen. Samt att n�sta tecken �r "="
                // F�r att undvika fel typ: gustav gustavson
                if ((it->substr(0,name.size()) == name) && (it->substr(name.size(),1) == "="))
                {
                    // Returnerar fr�n tecknet efter "=" tecknet och til radens slut

                    // "name" har hittats, nu skall nya v�rdet l�ggaas in
                    name.append("=");
                    name.append(value);
                    *it = name;
                    return true;

                }
            }
    }

return false;
}



void Config::newConf(string name,string value)
{
    string pair = name+"="+value;
    name_values->push_back(pair);
}



// L�ser configurationsvektorn
bool Config::readConf()
{

    name_values = new vector<string>;
    string conf_values;
    ifstream infil;

    infil.open(conf.c_str());
    if (!(infil.good()))
    {
            return false;
    }

    while (getline(infil,conf_values))
    {
        name_values->push_back(conf_values);
    }

    infil.close();
    return true;


}

// skriver configurationsvektorn till fil
bool Config::writeConf()
{
    ofstream utfil;
    utfil.open (conf.c_str(),ios::out);
    if (!(utfil.is_open()))
    {
        return false;
    }
    vector<string>::iterator it;
    for (it=name_values->begin();it<name_values->end();it++)
    {
        utfil << *it;
        utfil << "\n";
    }
    return true;

}
// kollar om konfigurationsfilen finns
void Config::checkConf()
{
    ifstream infil2;
    infil2.open(conf.c_str());
    infil2.seekg(0,ios::end);
    if (infil2.tellg() < 1)
    {
            infil2.close();
            // ipac BORTA I "exifrn_remsettings"
            //createConffile();
    }

}
// skapar en default konfigurationsfil
bool Config::createConffile()
{
    cr->mkDir();

    ofstream utfil2;
    utfil2.open (conf.c_str(),ios::out);
    if (utfil2.good())
    {
        utfil2 << "first_time=1" << endl;
        utfil2 << "version="VERSION << endl;
        utfil2 << "language=eng" << endl;
        utfil2 << "copy=0" << endl;
        utfil2 << "preserve=0" << endl;
        utfil2 << "preserve_before=0" << endl;
        utfil2 << "insert=0" << endl;
        utfil2 << "insert_before=0" << endl;
        utfil2 << "file_name=" << endl;
        utfil2 << "copy_path=" << endl;
        utfil2 << "open_path=" << endl;
        utfil2 << "log_path=" << endl;
        utfil2 << "vidd=749" << endl;
        utfil2 << "hojd=401" << endl;
        utfil2 << "x_lage=150" << endl;
        utfil2 << "y_lage=150" << endl;
        utfil2 << "hvidd=600" << endl;
        utfil2 << "hhojd=675" << endl;
        utfil2 << "hx_lage=150" << endl;
        utfil2 << "hy_lage=150" << endl;
        utfil2 << "ToolButtonStyle=3" << endl;
        utfil2 << "create_folder=0" << endl;
        utfil2 << "check_onstart=1" << endl;
        utfil2.close();
        return true;
    }
    return false;

}
