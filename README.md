# This program is no longer being developed. Please visit [EXIF ReName](https://gitlab.com/posktomten/exifrename2/-/wikis/home) This program replaces and is remade from scratch.

# EXIF ReName
Copyright 2007-2019<br>
EXIF ReName (Free GPL license)<br>
EXIF ReName Uses Qt 4<br>
A program to name the pictures after the date and time the picture was taken<br>
<br>Please se the Wiki pages<br>
https://gitlab.com/posktomten/exifrename/wikis/home<br>
@posktomten
